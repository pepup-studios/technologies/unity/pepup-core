﻿using UnityEngine;
using PepUp.Events;

namespace PepUp.Collections {
    [CreateAssetMenu(menuName = "PepUp/Collections/Stop Watch Object")]
    public class StopWatchObject : ScriptableObject {

        public float currTimerCount { get; private set; }

        public StopWatch timer { get; private set; }

        public event EventAction OnStart;
        public event EventAction OnStop;

        public void Init() {
            if (timer == null) {
                timer = new StopWatch();
            } else {
                timer.Init();
            }
            OnStart?.Invoke();
        }

        public void ResetTimer() {
            timer.Reset();
        }

        public void Start() {
            OnStart?.Invoke();
        }

        public void Stop() {
            OnStop?.Invoke();
        }

        public float Update() {
            return Update(Time.deltaTime);
        }

        public float Update(float deltaTime) {
            return currTimerCount = timer.Update(deltaTime);
        }
    }
}
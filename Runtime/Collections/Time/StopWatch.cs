﻿namespace PepUp.Collections {
    public class StopWatch {

        public int second { get; private set; }
        public float millisecond { get; private set; }
        public float time { get; private set; }

        public StopWatch() {
            Init();
        }

        public void Init() {
            Reset();
        }

        public void Reset() {
            time = millisecond = second = 0;
        }

        public float Update(float deltaTime) {
            millisecond += deltaTime; //converted to seconds
            if (millisecond >= 1) {
                second++;
                millisecond -= (int) millisecond; //better precision than setting it to 0.0f
            }
            time = second + millisecond;
            return time;
        }
    }
}
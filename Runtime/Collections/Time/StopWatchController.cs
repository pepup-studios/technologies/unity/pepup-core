﻿using PepUp.Events;
using UnityEngine;

namespace PepUp.Collections {
    public class StopWatchController : MonoBehaviour {

        public bool onStart = true;
        public StopWatchObject timer;

        public float currTimerCount { get; private set; }

        public RegisterEvent DoStart;
        public RegisterEvent DoStop;

        public event EventAction OnStart;
        public event EventAction OnStop;
        public event EventAction OnResume;
        public event EventAction OnPause;
        public event EventAction OnReset;

        protected void Awake() {
            DoStart.Register(this, "StartTimer");
            DoStop.Register(this, "StopTimer");

            enabled &= onStart;
        }

        protected void OnDestroy() {
            DoStart.Unregister();
            DoStop.Unregister();
        }

        public void Init() {
            timer?.Init();
        }

        protected void StartTimer(params object[] objs) {
            Start();
        }

        protected void StopTimer(params object[] objs) {
            Stop();
        }

        public void Start() {
            Init();
            currTimerCount = 0;
            enabled = true;
            timer.Start();
            OnStart?.Invoke();
        }

        public void Pause() {
            enabled = false;
            OnPause?.Invoke();
        }

        public void Resume() {
            enabled = true;
            OnResume?.Invoke();
        }

        public void Reset() {
            enabled = false;
            Init();
            timer?.ResetTimer();
            currTimerCount = 0;
            OnReset?.Invoke();
        }

        public void Stop() {
            enabled = false;
            currTimerCount = 0;
            timer?.Stop();
            OnStop?.Invoke();
        }

        public void Update() {
            currTimerCount = timer.Update();
        }

        public int GetSeconds() {
            return timer.timer.second;
        }

        public float GetMilliseconds() {
            return timer.timer.millisecond;
        }
    }
}
﻿using UnityEngine;
using PepUp.Events;

namespace PepUp.Collections {
    [CreateAssetMenu(menuName = "PepUp/Collections/Timer Object")]
    public class TimerObject : ScriptableObject {

        public float timerCount = 3;

        public float currTimerCount { get; private set; }
        public float currTimerValue;

        public RegisterEvent DoStart;
        public RegisterEvent DoStop;

        protected Timer timer;

        public event EventAction OnStart;
        public event EventAction OnStop;

        protected void OnEnable() {
#if UNITY_EDITOR
            if (!Application.isPlaying) return;
#endif
            DoStart.Register(this, "StartTimer");
            DoStop.Register(this, "StopTimer");
        }

        protected void OnDisable() {
#if UNITY_EDITOR
            if (!Application.isPlaying) return;
#endif
            DoStart.Unregister();
            DoStop.Unregister();
        }

        public void Init() {
            if (timer == null) {
                timer = new Timer(timerCount);
            } else {
                timer.Init(timerCount);
            }
        }

        protected void StartTimer(params object[] objs) {
            StartTimer();
        }

        protected void StopTimer(params object[] objs) {
            StopTimer();
        }

        public void StartTimer() {
            Init();
            currTimerCount = timerCount;
            timer.Start();
            OnStart?.Invoke();
        }

        public void StopTimer() {
            OnStop?.Invoke();
        }

        public float Update() {
            return Update(Time.deltaTime);
        }

        public float Update(float deltaTime) {
            return currTimerValue = currTimerCount = timer.Update(deltaTime);
        }

        public int GetSeconds() {
            return timer.second;
        }

        public float GetMilliseconds() {
            return timer.millisecond;
        }
    }
}
﻿using UnityEngine;
using PepUp.Events;

namespace PepUp.Collections {
    public class TimerScaleController : MonoBehaviour {

        public RegisterEvent DoSetTimeScale;

        public void OnEnable() {
            DoSetTimeScale.Register(this, "SetTimeScale");
        }

        public void OnDisable() {
            DoSetTimeScale.Unregister();
        }

        protected void SetTimeScale(params object[] objs) {
            Time.timeScale = (float) objs[0];
        }
    }
}
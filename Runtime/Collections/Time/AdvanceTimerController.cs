﻿using UnityEngine;
using PepUp.Events;

namespace PepUp.Collections {
    [AddComponentMenu("PepUp/Collections/Advance Timer Controller")]
    public class AdvanceTimerController : TimerController {

        public float secondCallbackAt = 1;

        public event EventAction OnSecondUpdate;
        public event EventAction OnSecondEnd;

        protected bool doOnce = true;
        protected int prevSec = -1;

        public override void Start() {
            doOnce = true;
            prevSec = -1;
            base.Start();
        }

        public override void Update() {
            currTimerCount.value = timer.Update(unscaleDeltaTime ? Time.unscaledDeltaTime : Time.deltaTime);

            int currSec = timer.second;
            if (prevSec != currSec) {
                UpdateSecond(currSec);
                prevSec = currSec;
            }

            if (doOnce && currTimerCount.value <= secondCallbackAt) {
                EndSecond();
                doOnce = false;
            }
            if (currTimerCount.value <= 0) {
                Stop();
            }
        }

        protected virtual void UpdateSecond(int currSec) {
            OnSecondUpdate?.Invoke(this, currSec);
        }

        protected virtual void EndSecond()
        {
            OnSecondEnd?.Invoke(this);
        }
    }
}

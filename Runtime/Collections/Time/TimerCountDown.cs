using PepUp.Events;
using UnityEngine;

namespace PepUp.Collections {
    public class TimerCountDown : MonoBehaviour {

        public float timerCount = 5f;

        protected Timer timer;
        protected float currTimerCount;
        protected bool doOnce = true;

        public event EventAction OnTimerStart;
        public event EventAction OnTimerUpdate;
        public event EventAction OnTimerEnd;

        public void StartTimer() {
            if (timer == null) {
                timer = new Timer(timerCount);
            } else {
                timer.Init(timerCount);
            }
            timer.Start(currTimerCount = timerCount);

            OnTimerStart(currTimerCount, timerCount);
            enabled = doOnce = true;
        }

        public void StopTimer() {
            TimerEnd(currTimerCount, timerCount);
            enabled = doOnce = false;
        }

        public void PauseTimer() {
            enabled = false;
        }

        public void ResumeTimer() {
            enabled = true;
        }

        public void Update() {
            currTimerCount = timer.Update();
            if (currTimerCount < 0) {
                enabled = false;
                TimerEnd(currTimerCount, timerCount);
                return;
            }
            OnTimerUpdate(currTimerCount, timerCount);
        }

        protected void TimerEnd(params object[] objs) {
            OnTimerEnd(objs);
        }
    }
}
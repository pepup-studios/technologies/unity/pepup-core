﻿using UnityEngine;
using System;

namespace PepUp.Collections {
    public class Timer {

        public float activeSeconds;

        public int second { get; private set; }
        public float millisecond { get; private set; }
        public float time { get; private set; }
        public double networkStartTime { get; private set; }
        public bool hasStopped { get; private set; }

        public Timer(float activeSeconds) {
            Init(activeSeconds);
        }

        public void Init(float activeSeconds) {
            this.activeSeconds = activeSeconds;
            Reset();
        }

        public void Start() {
            Start(activeSeconds);
        }

        public void Start(double netStartTime)
        {
            Start(activeSeconds, netStartTime);
        }

        public void Start(float value) {
            Start(value, GetTime());
        }

        public void Start(float value, double netStartTime)
        {
            networkStartTime = netStartTime;
            time = second = (int)value;
            millisecond = second - value;
            hasStopped = false;
        }

        public void Reset() {
            networkStartTime = 0;
            time = activeSeconds;
            millisecond = 0;
            second = (int)activeSeconds;
            hasStopped = true;
        }

        public void Stop() {
            hasStopped = true;
        }

        public float Update() {
            return Update(Time.deltaTime);
        }

        public float Update(float deltaTime) {
            millisecond += deltaTime; //converted to seconds
            if (millisecond >= 1) {
                second--;
                millisecond -= (int) millisecond; //better precision than setting it to 0.0f
            }
            time = second - millisecond;
            return time;
        }

        public float NetworkUpdate()
        {
            time = activeSeconds - (float)(GetTime() - networkStartTime);
            millisecond = time - (float)Math.Truncate(time);
            second = (int)time; //converted to seconds
            return time;
        }

        protected float GetTime() {
            return Time.time;
        }
    }
}
﻿using UnityEngine;
using PepUp.Events;

namespace PepUp.Collections {
    [AddComponentMenu("PepUp/Collections/Timer Controller")]
    public class TimerController : MonoBehaviour {

        public bool onStart = true;
        public FloatOption timerCount;
        public bool doRepeat;
        public bool unscaleDeltaTime;

        public FloatOption currTimerCount;

        public RegisterEvent DoStart;
        public RegisterEvent DoStop;
        public RegisterEvent DoResume;
        public RegisterEvent DoPause;
        public RegisterEvent DoReset;

        public Timer timer { get; protected set; }

        public event EventAction OnStart;
        public event EventAction OnStop;
        public event EventAction OnResume;
        public event EventAction OnPause;
        public event EventAction OnReset;

        protected void Awake() {
            DoStart.Register(this, "StartTimer");
            DoStop.Register(this, "StopTimer");
            DoResume.Register(this, "Resume");
            DoPause.Register(this, "Pause");
            DoReset.Register(this, "Reset");

            Init();

            enabled &= onStart;
        }

        protected void OnDestroy() {
            DoStart.Unregister();
            DoStop.Unregister();
            DoResume.Unregister();
            DoPause.Unregister();
            DoReset.Unregister();
        }

        public virtual void Init() {
            if (timer == null) {
                timer = new Timer(timerCount.value);
            } else {
                timer.Init(timerCount.value);
            }
        }

        protected virtual void StartTimer(params object[] objs) {
            try {
                timerCount.value = (float) objs[0];
            }
            catch { }
            Start();
        }

        protected void StopTimer(params object[] objs) {
            Stop();
        }

        public virtual void Start() {
            Init();
            currTimerCount.value = timerCount.value;
            StartTimer();
            enabled = true;
            OnStart?.Invoke(this);
        }

        protected virtual void StartTimer() {
            timer.Start();
        }

        protected void Resume(params object[] objs) {
            Resume();
        }

        protected void Pause(params object[] objs) {
            Pause();
        }

        public void Pause() {
            enabled = false;
            OnPause?.Invoke(this);
        }

        public void Resume() {
            enabled = true;
            OnResume?.Invoke(this);
        }

        protected void Reset(params object[] objs) {
            Reset();
        }

        public void Reset() {
            enabled = false;
            Init();
            //timer.Reset();
            currTimerCount.value = timerCount.value;
            OnReset?.Invoke(this);
        }

        public void Stop() {
            if (doRepeat) {
                OnStop?.Invoke(this);
                return;
            }
            enabled = false;
            currTimerCount.value = 0;
            OnStop?.Invoke(this);
        }

        public virtual void Update() {
            currTimerCount.value = timer.Update(unscaleDeltaTime ? Time.unscaledDeltaTime : Time.deltaTime);
            if (currTimerCount.value <= 0) {
                Stop();
            }
        }

        public int GetSeconds() {
            return timer.second;
        }

        public float GetMilliseconds() {
            return timer.millisecond;
        }
    }
}

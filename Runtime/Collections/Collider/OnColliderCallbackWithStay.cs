﻿using UnityEngine;
using PepUp.Events;

public class OnColliderCallbackWithStay : MonoBehaviour {

    public LayerMask colliderMask = -1;

    public EventObject TriggerEnter;
    public EventObject TriggerStay;
    public EventObject TriggerExit;

    public EventObject CollisionEnter;
    public EventObject CollisionStay;
    public EventObject CollisionExit;

    public event EventAction TriggerEnterEvent;
    public event EventAction TriggerStayEvent;
    public event EventAction TriggerExitEvent;

    public event EventAction CollisionEnterEvent;
    public event EventAction CollisionStayEvent;
    public event EventAction CollisionExitEvent;

    public void OnTriggerEnter(Collider other) {
        if (!HasLayerMask(other.gameObject.layer)) return;
        TriggerEnter?.Invoke(this,other);
        TriggerEnterEvent?.Invoke(this,other);
    }

    public void OnTriggerStay(Collider other)
    {
        if (!HasLayerMask(other.gameObject.layer)) return;
        TriggerStay?.Invoke(other);
        TriggerStayEvent?.Invoke(other);
    }

    public void OnTriggerExit(Collider other) {
        if (!HasLayerMask(other.gameObject.layer)) return;
        TriggerExit?.Invoke(other);
        TriggerExitEvent?.Invoke(other);
    }

    public void OnCollisionEnter(Collision other) {
        if (!HasLayerMask(other.gameObject.layer)) return;
        CollisionEnter?.Invoke(this, other.collider);
        CollisionEnterEvent?.Invoke(this, other);
    }

    public void OnCollisionStay(Collision other)
    {
        if (!HasLayerMask(other.gameObject.layer)) return;
        CollisionStay?.Invoke(this, other.collider);
        CollisionStayEvent?.Invoke(this, other);
    }

    public void OnCollisionExit(Collision other) {
        if (!HasLayerMask(other.gameObject.layer)) return;
        CollisionExit?.Invoke(this, other.collider);
        CollisionExitEvent?.Invoke(this, other);
    }

    protected bool HasLayerMask(int layer) {
        return colliderMask == (colliderMask | (1 << layer));
    }
}

﻿using UnityEngine;
using PepUp.Events;

public class OnCollider2DCallback : MonoBehaviour {

    public LayerMask colliderMask = -1;

    public EventObject TriggerEnter;
    public EventObject TriggerExit;

    public EventObject CollisionEnter;
    public EventObject CollisionExit;

    public event EventAction TriggerEnterEvent;
    public event EventAction TriggerExitEvent;

    public event EventAction CollisionEnterEvent;
    public event EventAction CollisionExitEvent;

    public void OnTriggerEnter2D(Collider2D other) {
        if (!HasLayerMask(other.gameObject.layer)) return;
        TriggerEnter?.Invoke(other);
        TriggerEnterEvent?.Invoke(other);
    }

    public void OnTriggerExit2D(Collider2D other) {
        if (!HasLayerMask(other.gameObject.layer)) return;
        TriggerExit?.Invoke(other);
        TriggerExitEvent?.Invoke(other);
    }

    public void OnCollisionEnter2D(Collision2D other) {
        if (!HasLayerMask(other.gameObject.layer)) return;
        CollisionEnter?.Invoke(other.collider);
        CollisionEnterEvent?.Invoke(other);
    }

    public void OnCollisionExit2D(Collision2D other) {
        if (!HasLayerMask(other.gameObject.layer)) return;
        CollisionExit?.Invoke(other.collider);
        CollisionExitEvent?.Invoke(other);
    }

    protected bool HasLayerMask(int layer) {
        return colliderMask == (colliderMask | (1 << layer));
    }
}

﻿using UnityEngine;
using PepUp.Events;

namespace PepUp {
    [RequireComponent(typeof(Collider))]
    public class ColliderController : MonoBehaviour {

        public bool activeOnStart;

        public RegisterEvent DoActive;
        public RegisterEvent DoDeactive;

        [SerializeField, HideInInspector]
        protected Collider colliderObj;

        protected void Start() {
            colliderObj.enabled = activeOnStart;
        }

        protected void OnEnable() {
            DoActive.Register(this, "Active");
            DoDeactive.Register(this, "Deactive");
        }

        protected void OnDisable() {
            DoActive.Unregister();
            DoDeactive.Unregister();
        }

        public void Active(params object[] objs) {
            colliderObj.enabled = true;
        }

        public void Deactive(params object[] objs) {
            colliderObj.enabled = false;
        }

#if UNITY_EDITOR
        protected void OnValidate() {
            if (Application.isPlaying) return; //To check for errors in editor
            colliderObj = GetComponent<Collider>();
        }
#endif
    }
}
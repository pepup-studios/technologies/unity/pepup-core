﻿using PepUp.Events;
using UnityEngine;

public class GameObjectEnableState : MonoBehaviour
{
    public bool activeOnStart;

    public RegisterEvent DoActive;
    public RegisterEvent DoDective;

    public event EventAction OnActiveEvent;
    public event EventAction OnInactiveEvent;

    protected void Awake() {
        DoActive.Register(this, "Active");
        DoDective.Register(this, "Deactive");
    }

    protected void Start() {
        if (!activeOnStart) Deactive(gameObject);
    }

    protected void OnDestroy()
    {
        DoActive.Unregister();
        DoDective.Unregister();
    }

    protected void Active(params object[] objs) {
        try {
            Active(objs[0] as GameObject);
        }
        catch {
            Active(gameObject);
        }
    }

    public void Active(GameObject gameObject) {
        gameObject.SetActive(true);

        OnActiveEvent?.Invoke(this);
    }

    protected void Deactive(params object[] objs) {
        try {
            Deactive(objs[0] as GameObject);
        }
        catch {
            Deactive(gameObject);
        }
    }

    public void Deactive(GameObject gameObject) {
        gameObject.SetActive(false);

        OnInactiveEvent?.Invoke(this);
    }
}

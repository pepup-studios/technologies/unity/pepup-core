﻿using UnityEngine;
using PepUp.Events;

public class ParticleToTarget : MonoBehaviour {

	public Transform target;

	public ParticleSystem ps;

    [Space]
    public RegisterEvent DoActive;
    public RegisterEvent DoDeactive;

    private static ParticleSystem.Particle[] particles = new ParticleSystem.Particle[1000];

    protected void Awake() {
        DoActive.Register(this, "Active");
        DoDeactive.Register(this, "Deactive");
        Deactive();
    }

    protected void OnDestroy() {
        DoActive.Unregister();
        DoDeactive.Unregister();
    }

    protected void Active(params object[] objs) {
        Active();
    }

    public void Active() {
        enabled = true;
    }

    protected void Deactive(params object[] objs) {
        Deactive();
    }

    public void Deactive() {
        enabled = false;
    }

    protected void Update() {
		int count = ps.GetParticles(particles);

		for (int i = 0; i < count; i++) {
			ParticleSystem.Particle particle = particles[i];

			Vector3 v1 = ps.transform.TransformPoint(particle.position);
			Vector3 v2 = target.position;

			Vector3 tarPosi = (v2 - v1) * (particle.remainingLifetime / particle.startLifetime);
			particle.position = ps.transform.InverseTransformPoint(v2 - tarPosi);
			particles[i] = particle;
		}

        ps.SetParticles(particles, count);
	}
}
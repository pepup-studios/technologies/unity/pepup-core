﻿using UnityEngine;
using PepUp.Events;
using System.Collections;
using PepUp.Collections;

namespace PepUp {
    [RequireComponent(typeof(ParticleSystem))]
    public class ParticleController : MonoBehaviour {

        public bool addToPoolOnFinish;
        public string poolTag;
        public float refreshRate = 0.5f;

        [Space]
        public RegisterEvent DoNormalPlay;
        public RegisterEvent DoNormalStop;

        [SerializeField] [HideInInspector]
        private ParticleSystem _ps;
        public ParticleSystem particle { get { return _ps; } }

        public event EventAction OnPlay;
        public event EventAction OnStop;
        public event EventAction OnFinished;

        protected IEnumerator checkIfAlive;
        protected ObjectPool pool;

        protected void Awake() {
            pool = ObjectPool.intance;
        }

        protected void OnEnable() {
            DoNormalPlay.Register(this, "NormalPlay");
            DoNormalStop.Register(this, "NormalStop");
        }

        protected void OnDisable() {
            DoNormalPlay.Unregister();
            DoNormalStop.Unregister();
            Stop();
        }

        public void NormalPlay(params object[] objs) {
            Play();
        }

        public void Play() {
            if (!_ps.isPlaying) {
                _ps.Play();
                OnPlay?.Invoke();
            }
            if (checkIfAlive != null) StopCheckIfAlive();

            checkIfAlive = CheckIfAlive();
            StartCoroutine(checkIfAlive);
        }

        public void NormalStop(params object[] objs) {
            Stop();
        }

        public void Stop() {
            if (_ps.isPlaying) {
                _ps.Stop();
                OnStop?.Invoke();
            }
            StopCheckIfAlive();
        }

        public void PutObjectToPool() {
            pool.PutObject(poolTag, gameObject.name, this);
            gameObject.SetActive(false);
        }

        protected void StopCheckIfAlive() {
            if (checkIfAlive != null) StopCoroutine(checkIfAlive);
        }

        protected IEnumerator CheckIfAlive() {
            while (_ps != null && _ps.IsAlive(true)) {
                yield return new WaitForSeconds(refreshRate);
            }
            if (addToPoolOnFinish) PutObjectToPool();
            OnFinished?.Invoke();
        }

#if UNITY_EDITOR
        protected void OnValidate() {
            _ps = GetComponent<ParticleSystem>();
        }
#endif
    }
}
﻿using UnityEngine;

namespace PepUp.Collections {
    public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour {

        public static T instance { get; private set; }

        protected abstract void Awake();

        protected virtual bool InitSingleton(T newInstance) {
            if (instance != null) {
                Destroy(gameObject);
                return false;
            }
            instance = newInstance;
            return true;
        }
    }
}
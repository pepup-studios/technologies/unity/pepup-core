﻿using UnityEngine;

namespace PepUp.Collections {
    [AddComponentMenu("PepUp/Collections/Don't Destroy On Load")]
    public class DontDestroyOnLoad : MonoBehaviour {

        void Awake() {
            DontDestroyOnLoad(gameObject);
        }
    }
}
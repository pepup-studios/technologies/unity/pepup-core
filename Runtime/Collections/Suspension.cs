﻿using UnityEngine;
using PepUp.Events;

namespace PepUp.Collections {
    [AddComponentMenu("PepUp/Collections/Suspension")]
    public class Suspension : MonoBehaviour {

        public float suspensionOffset;
        public float suspensionSpeed;
        public bool autoSpring = true;

        [Space]
        public RegisterEvent DoActive;
        public RegisterEvent DoRelease;
        public RegisterEvent DoDeactive;

        Vector3 initialPosition;
        Vector3 targetPosition;

        private int _counter;

        private void Start() {
            initialPosition = transform.localPosition;
            targetPosition = transform.localPosition;
            targetPosition.y = transform.localPosition.y + suspensionOffset;
        }

        protected void Awake() {
            DoActive.Register(this, "Activate");
            DoRelease.Register(this, "Release");
            DoDeactive.Register(this, "Deactivate");
        }

        protected void OnDestroy() {
            DoActive.Unregister();
            DoRelease.Unregister();
            DoDeactive.Unregister();
        }

        void Update() {
            if (_counter == 1) {
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, initialPosition, suspensionSpeed * Time.deltaTime);
                if (transform.localPosition.y >= initialPosition.y) {
                    transform.localPosition = initialPosition;
                    _counter++;
                }
            } else {
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, targetPosition, suspensionSpeed * Time.deltaTime);
                if (transform.localPosition.y <= targetPosition.y) {
                    transform.localPosition = targetPosition;
                    if (autoSpring) _counter++;
                    else Deactivate();
                }
            }

            if (_counter >= 2) Deactivate();
        }

        public void Activate(params object[] objs) {
            _counter = 0;
            enabled = true;
        }

        public void Release(params object[] objs) {
            _counter = 1;
            enabled = true;
        }

        public void Deactivate(params object[] objs) {
            enabled = false;
        }
    }
}
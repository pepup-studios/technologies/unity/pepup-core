﻿using UnityEngine;

namespace PepUp {
    public class CameraResolutionController : MonoBehaviour {

        public Camera targetCamera;

        [Space]
        public Resolution mobile = new Resolution(0.6f, 900);
        public Resolution tablet = new Resolution(0.9f, 1080);

        void Awake() {
            float currentAspect = targetCamera.aspect;
            float targetRes = (currentAspect > mobile.maxTargetAspect) ? tablet.horizontalResolution : mobile.horizontalResolution;
            targetCamera.orthographicSize = targetRes / currentAspect / 200;
        }

        [System.Serializable]
        public class Resolution {
            public readonly float maxTargetAspect;
            public float horizontalResolution;

            public Resolution(float maxTargetAspect, float horizontalResolution) {
                this.maxTargetAspect = maxTargetAspect;
                this.horizontalResolution = horizontalResolution;
            }
        }
    }
}
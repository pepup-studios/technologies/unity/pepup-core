﻿using UnityEngine;
using System.Collections;
using PepUp.Events;

namespace PepUp {
    public class CameraShake : MonoBehaviour {

        public float shakeAmount;//The amount to shake this frame.
        public float shakeDuration;//The duration this frame.

        public bool smooth;//Smooth rotation?
        public float smoothAmount = 5f;//Amount to smooth

        [Space]
        public RegisterEvent DoShake;

        protected IEnumerator shake;

        private readonly Quaternion IDENTITY = Quaternion.identity;

        private float _amount;
        private float _duration;

        protected void OnEnable() {
            DoShake.Register(this, "OnShakeCamera");
        }

        protected void OnDisable() {
            DoShake.Unregister();

            StopShake();
        }

        protected void OnShakeCamera(params object[] objs) {
            try {
                float amount = (float) objs[0];
                float duration = (float) objs[1];
                ShakeCamera(amount, duration);
            }
            catch {
                ShakeCamera();
            }
        }

        public void ShakeCamera() {
            _amount = shakeAmount;
            _duration = shakeDuration;

            if (shake == null) StartShake();
        }

        public void ShakeCamera(float amount, float duration) {
            _amount = amount;
            _duration = duration;

            if (shake == null) StartShake();
        }

        protected void StartShake() {
            shake = Shake();
            StartCoroutine(shake);
        }

        protected void StopShake() {
            if (shake != null) StopCoroutine(shake);
        }

        protected IEnumerator Shake() {
            while (shakeDuration > 0.01f) {
                Vector3 rotationAmount = Random.insideUnitSphere * shakeAmount;//A Vector3 to add to the Local Rotation
                 //rotationAmount.z = 0;//Don't change the Z; it looks funny.

                float shakePercentage = shakeDuration / _duration;//Used to set the amount of shake (% * startAmount).

                shakeAmount = _amount * shakePercentage;//Set the amount of shake (% * startAmount).
                shakeDuration = Mathf.Lerp(shakeDuration, 0, Time.deltaTime);//Lerp the time, so it is less and tapers off towards the end.

                if (smooth)
                    transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(rotationAmount), Time.deltaTime * smoothAmount);
                else
                    transform.localRotation = Quaternion.Euler(rotationAmount);//Set the local rotation the be the rotation amount.

                yield return null;
            }
            transform.localRotation = IDENTITY;//Set the local rotation to 0 when done, just to get rid of any fudging stuff.

        }

    }
}
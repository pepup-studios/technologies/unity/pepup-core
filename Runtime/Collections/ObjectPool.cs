﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PepUp.Collections {
    public class ObjectPool {

        protected static ObjectPool _instance;

        protected Dictionary<string, Dictionary<string, Stack>> reservoir;

        public static ObjectPool intance {
            get {
                if (_instance == null) {
                    _instance = new ObjectPool();
                    _instance.reservoir = new Dictionary<string, Dictionary<string, Stack>>();
                }
                return _instance;
            }
        }

        public GameObject GetGameObject(string type, string name, string source = "N/A") {
            if (reservoir.ContainsKey(type)) {
                if (reservoir[type].ContainsKey(name)) {
                    while (reservoir[type][name].Count > 0) {
                        GameObject gameObject = (GameObject) reservoir[type][name].Pop();
                        if (gameObject != null && !gameObject.activeInHierarchy) {
                            //Debug.Log("Found Object in Pool: " + name + " for " + source);
                            //gameObject.SetActive(true);
                            //Debug.Log("Poped: " + name);
                            return gameObject;
                        }
                        //Debug.Log("Poped for Free; found Active: " + name);
                    }
                }
            }
            //Debug.Log("Object not found: " + name + " Type: " + type);
            return null;
        }

        public void PutGameObject(string type, string name, GameObject gameObject) {
            if (!reservoir.ContainsKey(type)) {
                reservoir.Add(type, new Dictionary<string, Stack>());
            }
            if (!reservoir[type].ContainsKey(name)) {
                Stack poolStack = new Stack();
                reservoir[type].Add(name, poolStack);
            }
            //Debug.Log("Pushing to Pool: " + name);
            gameObject.transform.parent = null;
            gameObject.SetActive(false);
            reservoir[type][name].Push(gameObject);
        }

        public T GetObjectByPop<T>(string type, string name, string source = "N/A") {
            if (reservoir.ContainsKey(type)) {
                if (reservoir[type].ContainsKey(name)) {
                    if (reservoir[type][name].Count > 0) {
                        T obj = (T) reservoir[type][name].Pop();
                        //Debug.Log("Poped for: " + obj + " for " + source);
                        return obj;
                    }
                }
            }
            //Debug.Log("Object not found: " + name);
            return default(T);
        }

        public T GetObjectByPeek<T>(string type, string name, string source = "N/A") {
            if (reservoir.ContainsKey(type)) {
                if (reservoir[type].ContainsKey(name)) {
                    if (reservoir[type][name].Count > 0) {
                        T obj = (T) reservoir[type][name].Peek();
                        //Debug.Log("Peeked for: " + obj + " for " + source);
                        return obj;
                    }
                }
            }
            //Debug.Log("Object not found: " + name);
            return default(T);
        }

        public void PutObject<T>(string type, string name, T obj) {
            if (!reservoir.ContainsKey(type)) {
                reservoir.Add(type, new Dictionary<string, Stack>());
            }
            if (!reservoir[type].ContainsKey(name)) {
                Stack poolStack = new Stack();
                reservoir[type].Add(name, poolStack);
            }
            //Debug.Log("Pushing to Pool: " + name);
            reservoir[type][name].Push(obj);
        }

        public void RemoveAllObject(string type, string name) {
            if (!reservoir.ContainsKey(type) || !reservoir[type].ContainsKey(name)) {
                return;
            }
            //Debug.Log("Removed from Pool: " + name);
            reservoir[type][name].Clear();
        }

        public void ClearPool() {
            reservoir.Clear();
        }

        public void OverrideLastObject<T>(string type, string name, T obj) {
            if (!reservoir.ContainsKey(type)) {
                reservoir.Add(type, new Dictionary<string, Stack>());
            }
            if (!reservoir[type].ContainsKey(name)) {
                Stack poolStack = new Stack();
                reservoir[type].Add(name, poolStack);
            } else {
                reservoir[type][name].Pop();
            }
            //Debug.Log("Pushing to Pool: " + name);
            reservoir[type][name].Push(obj);
        }
    }
}
﻿using UnityEngine;
using PepUp.Events;

public class URL : MonoBehaviour {

    public RegisterEvent DoOpenURL;

    protected void Awake() {
        DoOpenURL.Register(this, "OpenURL");
    }

    protected void OnDestroy() {
        DoOpenURL.Unregister();
    }

    protected void OpenURL(params object[] objs) {
        try {
            OpenURL((string) objs[1]);
        }
        catch {
            OpenURL((string) objs[0]);
        }
    }

    public void OpenURL(string url) {
        Application.OpenURL(url);
    }
}

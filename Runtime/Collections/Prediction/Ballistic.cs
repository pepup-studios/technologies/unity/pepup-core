﻿using UnityEngine;

namespace PepUp.Collections {
    public static class Ballistic {
    
        public static Vector3 GetInitialVelocity(Vector3 source, Vector3 target, float force, Vector3 opposingForce, float arch = 0) {
            arch = Mathf.Clamp(arch, 0, 1);
            Vector3 displacement = target - source;
            float gSquared = opposingForce.sqrMagnitude;
            float b = force * force + Vector3.Dot(displacement, opposingForce);
            float discriminant = b * b - gSquared * displacement.sqrMagnitude;
            if (discriminant < 0) {
                return -Vector3.forward;
            }

            float discRoot = Mathf.Sqrt(discriminant);
            float tMax = Mathf.Sqrt((b + discRoot) * 2 / gSquared);
            float tMin = Mathf.Sqrt((b - discRoot) * 2 / gSquared);
            //float tLowEnergy = Mathf.Sqrt(Mathf.Sqrt(displacement.sqrMagnitude * 4 / gSquared));
            float time = (tMax - tMin) * arch + tMin;
            return displacement / time - opposingForce * time / 2;
        }
    }
}

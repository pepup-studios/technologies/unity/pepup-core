﻿using System.Collections;
using System.Collections.Generic;

namespace PepUp.Collections {
    public class EnumeratableHandler {

        public List<IEnumerator> enumerators { get; } = new List<IEnumerator>();

        private UnityEngine.MonoBehaviour _monoBehaviour;

        public EnumeratableHandler(UnityEngine.MonoBehaviour monoBehaviour) {
            _monoBehaviour = monoBehaviour;
        }

        public void StartCoroutine(IEnumerable enumerable) {
            IEnumerator newEnumerator = GetNewEnumerator(enumerable);
            _monoBehaviour.StartCoroutine(newEnumerator);
        }

        public IEnumerator GetNewEnumerator(IEnumerable enumerable) {
            IEnumerator newEnumerator = enumerable.GetEnumerator();
            enumerators.Add(newEnumerator);
            return newEnumerator;
        }

        public void ClearEnumerators() {
            foreach (var enumerator in enumerators) {
                if (enumerator != null) _monoBehaviour.StopCoroutine(enumerator);
            }
            enumerators.Clear();
        }
    }
}
﻿using UnityEngine;
using PepUp.Events;

namespace PepUp.Collections {
    public class Randomizer : MonoBehaviour {

        public bool onStart;
        public RangedFloatOption randomRange;

        public RegisterEvent DoRandomize;

        public event EventAction OnRandom;

        protected void Start() {
            if (onStart) Randomize();
        }

        protected void OnEnable() {
            DoRandomize.Register(this, "Randomize");
        }

        protected void OnDisable() {
            DoRandomize.Unregister();
        }

        public void Randomize(params object[] objs) {
            OnRandom?.Invoke(GetRandomValue());
        }

        public float GetRandomValue() {
            return Random.Range(randomRange.value.minValue, randomRange.value.maxValue);
        }
    }
}
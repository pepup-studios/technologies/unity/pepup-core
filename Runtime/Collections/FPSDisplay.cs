﻿using UnityEngine;

public class FPSDisplay : MonoBehaviour {

    float frameCount = 0;
    float dt = 0.0f;
    float fps = 0.0f;
    float updateRate = 4.0f;  // 4 updates per sec.

    protected Color color = new Color(1.0f, 1.0f, 0.0f, 1.0f);

    void Update() {
        frameCount++;
        dt += Time.deltaTime;
        if (dt > 1.0f / updateRate) {
            fps = frameCount / dt;
            frameCount = 0;
            dt -= 1.0f / updateRate;
        }
    }

    void OnGUI() {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(8, h - 120, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 2 / 100;
        style.normal.textColor = color;
        string text = Mathf.Round(fps) + " fps";
        GUI.Label(rect, text, style);
    }
}
﻿using UnityEngine;

namespace PepUp.Collections {
    [System.Serializable]
    public class ColorGradient {
        public Color top = Color.white;
        public Color middle = Color.gray;
        public Color bottom = Color.black;
    }
}
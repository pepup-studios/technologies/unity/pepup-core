﻿using UnityEngine;

namespace PepUp.Collections {
    [CreateAssetMenu(menuName = "PepUp/Collections/Color Gradient")]
    public class ColorGradientObject : ScriptableObject {

        public Color top = Color.white;
        public Color middle = Color.gray;
        public Color bottom = Color.black;
    }
}
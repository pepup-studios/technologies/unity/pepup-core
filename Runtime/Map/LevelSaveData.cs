﻿namespace PepUp.Map {
    [System.Serializable]
    public class LevelSaveData {

        public int levelId;
        public int stageId;

        public int levelIndex; //deprecated on v0.1.1
        public int stageIndex; //deprecated on v0.1.1

        public LevelSaveData(int levelId, int stageId) {
            this.levelId = levelId;
            this.stageId = stageId;
        }

        //public LevelSaveData(int levelIndex, int stageIndex) {
        //    this.levelIndex = levelIndex;
        //    this.stageIndex = stageIndex;
        //}
    }
}
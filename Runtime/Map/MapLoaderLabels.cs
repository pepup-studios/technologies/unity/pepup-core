﻿using UnityEngine;

namespace PepUp.Map {
    [CreateAssetMenu(menuName = "PepUp/Map/Map Label")]
    public class MapLoaderLabels : ScriptableObject {

        public MapLoader map;

        public string gameMode {
            get {
                try {
                    return map.currentMap.name;
                }
                catch {
                    return "null";
                }
            }
        }

        public string levelStageID {
            get {
                return levelID + "$" + stageID;
            }
        }

        public int levelID {
            get {
                try {
                    return map.currentMap.currentLevel.id;
                }
                catch {
                    return -1;
                }
            }
        }

        public int stageID {
            get {
                try {
                    return map.currentMap.currentLevel.currentStage.id;
                }
                catch {
                    return -1;
                }
            }
        }

        public int levelIndex {
            get {
                try {
                    return map.currentMap.currentLevel.index;
                }
                catch {
                    return -1;
                }
            }
        }

        public int stageIndex {
            get {
                try {
                    return map.currentMap.currentLevel.currentStage.index;
                }
                catch {
                    return -1;
                }
            }
        }

        public int stageLocalIndex {
            get {
                try {
                    return map.currentMap.currentLevel.currentStage.localIndex;
                }
                catch {
                    return -1;
                }
            }
        }

        public int levelNumber {
            get {
                int index = levelIndex;
                if (index > -1) {
                    return index + 1;
                }
                return index;
            }
        }

        public int stageNumber {
            get {
                int index = stageIndex;
                if (index > -1) {
                    return index + 1;
                }
                return index;
            }
        }

        public int stageLocalNumber {
            get {
                int index = stageLocalIndex;
                if (index > -1) {
                    return index + 1;
                }
                return index;
            }
        }
    }
}
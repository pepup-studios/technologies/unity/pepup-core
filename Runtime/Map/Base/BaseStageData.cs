﻿using UnityEngine;

namespace PepUp.Map
{
    [System.Serializable]
    public class BaseStageData
    {
        [HideInInspector]
        public int localIndex;
        public int id;
        public string name;
        public bool isStart;
        public SpawnStrategy spawnStrategy;
        //[HideCondition("spawnStrategy", "order")]
        public string nextStage;

        [HideInInspector]
        public int nextStageIndex = -1;
        [HideInInspector]
        public int index;
    }

    public enum SpawnStrategy { none, random, order, smartRandom };
}

﻿using PepUp.Utility;
using System.Collections.Generic;
using UnityEngine;

namespace PepUp.Map { 
    public class BaseLevelData<T> : ScriptableObject where T : BaseStageData
    {
        [HideInInspector]
        public int index;
        public int id;
        public bool isStart;
        //[Hide("hasNextStage", "true")]
        public SpawnStrategy spawnStrategy;
        public string nextLevel;
        public int smartRandomLimit;
        public bool loadFromRuntimeSave;
        public bool hasStageSpawnLimit;
        public int stageSpawnLimit;
        public T[] stages;

        [HideInInspector]
        public int nextLevelIndex = -1;
        public T currentStage { get; private set; }

        public MapIDSettings remapSettings { get; set; }
        public LevelSaveData saveData { get; set; }

        protected Dictionary<int, int> stageMap = new Dictionary<int, int>();

        protected List<int> startRandomStages = new List<int>();
        protected List<int> smartRandomStages = new List<int>();

        protected int spawnCounter;

        [SerializeField, HideInInspector]
        protected int currentStageIndex;

        public void Initialize()
        {
            int len = stages.Length;
            for (int i = 0; i < len; i++)
            {
                T stage = stages[i];
                if (stageMap.ContainsKey(stage.id)) continue;
                startRandomStages.Add(i);
            }
            Reset();
            LoadStartStage();
            //currentStage = (currentStageIndex != -1) ? GetStage(currentStageIndex) : GetRandomStage();
        }

        public void Reset()
        {
            ResetSmartRandomStage();
            spawnCounter = 0;
        }

        public void Save(string fileName)
        {
            LevelSaveData newSaveData;
            if (loadFromRuntimeSave)
            {
                newSaveData = saveData = new LevelSaveData(id, currentStage.id);
            }
            else
            {
                newSaveData = new LevelSaveData(id, currentStage.id);
                saveData = null;
            }
            SaveUtility.SaveAsBinaryFormatter(newSaveData, fileName);
        }

        public void LoadStartStage()
        {
            if (!CanSpawnStage())
            {
                currentStage = null;
                return;
            }

            currentStage = GetStartStage();
            spawnCounter++;
        }

        public virtual T UpdateStage()
        {
            if (!CanSpawnStage()) return currentStage = null;

            T stage = null;
            if (currentStage.spawnStrategy == SpawnStrategy.order)
            {
                stage = GetStage(currentStage.nextStageIndex);
            }
            else if (currentStage.spawnStrategy == SpawnStrategy.random)
            {
                stage = GetRandomStage();
            }
            else if (currentStage.spawnStrategy == SpawnStrategy.smartRandom)
            {
                stage = GetSmartRandomStage();
            }

            spawnCounter++;
            return currentStage = stage;
        }

        public T UpdateStage(int index)
        {
            if (!CanSpawnStage()) return currentStage = null;

            T stage = GetStage(index);
            spawnCounter++;
            return currentStage = stage;
        }

        protected virtual bool CanSpawnStage()
        {
            return !(hasStageSpawnLimit && spawnCounter > stageSpawnLimit);
        }

        protected T GetRandomStage()
        {
            return stages[Random.Range(0, stages.Length)];
        }

        protected void ResetSmartRandomStage()
        {
            smartRandomStages = new List<int>(startRandomStages);
        }

        protected T GetSmartRandomStage()
        {
            int smartCount = smartRandomStages.Count;
            int diff = startRandomStages.Count - smartCount;
            if (diff >= smartRandomLimit)
            {
                ResetSmartRandomStage();
            }
            int index = Random.Range(0, smartCount);
            T newStage = stages[smartRandomStages[index]];
            smartRandomStages.RemoveAt(index);
            return newStage;
        }

        public T GetStartStage()
        {
            T startStage = null;
            if (currentStageIndex != -1)
            {
                startStage = GetStage(
                    (saveData == null) ?
                    currentStageIndex :
                    GetStageIndexByID(saveData)
                    );
            }
            //if (startStrategy == StartStrategy.smartRandom) {
            //    startStage = GetSmartRandomStage();
            //} else if (startStage == null || startStrategy == StartStrategy.random) {
            //    startStage = GetRandomStage();
            //}
            if (startStage == null)
            {
                startStage = GetRandomStage();
            }
            return startStage;
        }

        public T GetStage(int index)
        {
            return stages[index];
        }

        public int GetStageIndexByID(LevelSaveData level)
        {
            //backwards compatibility
            if (level.stageId == 0 && stageMap.TryGetValue(level.stageIndex + 1, out int index))
            {
                return index;
            }
            if (remapSettings != null && remapSettings.GetStageID(level.levelId, level.stageId, out index))
            {
                index = GetStageIndexByID(index);
                return index;
            }
            return GetStageIndexByID(level.stageId);
        }

        public int GetStageIndexByID(int id)
        {
            if (stageMap.TryGetValue(id, out int index))
            {
                return index;
            }
            return 0;
        }

    #if UNITY_EDITOR

        //[HideInInspector]
        //public BaseMapManager<BaseLevelData<BaseStageData>,BaseStageData> mapManager;

        private readonly Dictionary<string, int> stageIndexMap = new Dictionary<string, int>();

        public void OnValidate()
        {
            if (Application.isPlaying || stages == null) return;
            currentStageIndex = -1;
            saveData = null;
            int counter = 0;
            //if (mapManager != null && mapManager.levels != null)
            //{
            //    bool isAdded = false;
            //    foreach (var level in mapManager.levels)
            //    {
            //        if (level == this)
            //        {
            //            isAdded = true;
            //            break;
            //        }
            //        counter += level.stages.Length;
            //    }
            //    if (!isAdded) mapManager = null;
            //    else mapManager.OnValidate();
            //}

            int len = stages.Length;
            for (int i = 0; i < len; i++)
            {
                stages[i].localIndex = i;
                stages[i].index = counter + i;
                string stageName = stages[i].name;
                if (stageName != "" && !stageIndexMap.ContainsKey(stageName))
                {
                    stageIndexMap.Add(stageName, i);
                }
            }

            for (int i = 0; i < len; i++)
            {
                if (stages[i].spawnStrategy == SpawnStrategy.order)
                {
                    stages[i].nextStageIndex = stageIndexMap[stages[i].nextStage];
                }
                else
                {
                    stages[i].nextStageIndex = -1;
                }
                if (stages[i].isStart)
                {
                    currentStageIndex = i;
                }
            }

            if (smartRandomLimit > len)
            {
                smartRandomLimit = len;
            }
            startRandomStages.Clear();
            smartRandomStages.Clear();
        }
    #endif
    }

}

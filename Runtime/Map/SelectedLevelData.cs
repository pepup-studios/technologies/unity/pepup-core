﻿using UnityEngine;
using PepUp;

[CreateAssetMenu(menuName = "Game/Map/Selected LevelData")]
public class SelectedLevelData : ScriptableObject {

    public string gameMode;

    [SerializeField]
    private int _localLevelIndex;
    public int localLevelIndex { get { return _localLevelIndex; } set { _localLevelIndex = value; levelNumber = _localLevelIndex + 1; } }

    [SerializeField]
    private int _stageIndex;
    public int stageIndex { get { return _stageIndex; } set { _stageIndex = value; stageNumber = _stageIndex + 1; } }

    [SerializeField]
    private int _localStageIndex;
    public int localStageIndex { get { return _localStageIndex; } set { _localStageIndex = value; localStageNumber = _localStageIndex + 1; } }

    public int levelNumber { get; private set; }
    public int stageNumber { get; private set; }
    public int localStageNumber { get; private set; }

    public int test { get { return _stageIndex; } set { _stageIndex = value; } }

    public void Set(string gameMode, int localLevelIndex, int stageIndex, int localStageIndex) {
        this.gameMode = gameMode;
        this.localLevelIndex = localLevelIndex;
        this.stageIndex = stageIndex;
        this.localStageIndex = localStageIndex;
    }
}
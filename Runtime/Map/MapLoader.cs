﻿using UnityEngine;
using PepUp.Events;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using PSM = PepUp.SceneManagement;
using PepUp.Utility;

namespace PepUp.Map {
    [CreateAssetMenu(menuName = "PepUp/Map/Map Loader")]
    public class MapLoader : ScriptableObject {

        public string saveFile;
        public MapConfig[] mapConfigs;

        [Header("Map Events")]
        public EventObject OnMapLoad;
        public EventObject OnMapLoaded;

        [Header("Scene Event")]
        public EventObject OnLoadScene;

        public MapManager currentMap { get; private set; } = null;

        protected int counter;

        protected Dictionary<string, MapConfig> mapConfigMap;

        public void Init() {
            Load();
            mapConfigMap = new Dictionary<string, MapConfig>();

            int len = mapConfigs.Length;
            for (int i = 0; i < len; i++) {
                MapConfig mapConfig = mapConfigs[i];
                mapConfigMap.Add(mapConfig.map.name, mapConfig);
                mapConfig.map.Init();
                if (mapConfig.isDefault) {
                    currentMap = mapConfig.map;
                }
            }
        }

        public void Load() {
            if (!SaveUtility.SaveAsBinaryFormatter(counter, saveFile)) {
                counter = 0;
            }
        }

        public void Save() {
            SaveUtility.SaveAsBinaryFormatter(counter, saveFile);
        }

        public LevelData StartMap() {
#if UNITY_EDITOR
            if (currentMap == null) {
                Debug.LogError("Initialize map before loading it");
                return null;
            }
#endif
            return StartMap(currentMap.currentLevel);
        }

        public LevelData StartMap(LevelData level) {
            return StartMap(level.index);
        }

        public LevelData StartMap(int levelIndex) {
            return currentMap.StartMap(levelIndex);
        }

        public LevelData Update() {
#if UNITY_EDITOR
            if (currentMap == null) {
                Debug.LogError("Initialize map before loading it");
                return null;
            }
#endif
            LevelData level = currentMap.UpdateMap();
            if (GetMapConfig(currentMap.name).saveMap) currentMap.SaveMap();
            return level;
        }

        public LevelData Update(int levelIndex, int stageIndex) {
#if UNITY_EDITOR
            if (currentMap == null) {
                return null;
            }
#endif
            LevelData level = currentMap.UpdateMap(levelIndex, stageIndex);
            if (GetMapConfig(currentMap.name).saveMap) currentMap.SaveMap();
            return level;
        }

        public LevelData Update(int levelIndex) {
#if UNITY_EDITOR
            if (currentMap == null) {
                return null;
            }
#endif
            LevelData level = currentMap.UpdateMap(levelIndex);
            if (GetMapConfig(currentMap.name).saveMap) currentMap.SaveMap();
            return level;
        }

        public LevelData Update(string mapName, int levelIndex, int stageIndex) {
            currentMap = GetMapConfig(mapName).map;
            return Update(levelIndex, stageIndex);
        }

        public MapConfig GetMapConfig(string name) {
            return mapConfigMap[name];
        }

        public void SetMapConfig(string name) {
            currentMap = mapConfigMap[name].map;
        }

        public LevelData UpdateWithSceneLoad() {
            LevelData level = Update();
            LoadLevel(level);
            return level;
        }

        public void UpdateWithoutReturn() {
            Update();
        }

        protected void OnEnable() {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        protected void OnDisable() {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        public LevelData LoadLevel() {
            return LoadLevel(currentMap.currentLevel);
        }

        public LevelData LoadLevel(LevelData level) {
            MapConfig mapConfig = GetMapConfig(currentMap.name);
            string sceneName = mapConfig.mapPrefix + level.name + "/" + level.currentStage.name;
            OnLoadScene?.Invoke(sceneName, LoadSceneMode.Additive, mapConfig.parentScene, mapConfig.sceneType, mapConfig.sceneLimit);
            counter++;
            Save();
            OnMapLoad?.Invoke(currentMap, counter);
            return currentMap.currentLevel;
        }

        public string GetScenePath() {
            if (currentMap == null)
            {
                return null;
            }
            return GetScenePath(currentMap.name);
        }

        public string GetScenePath(string name) {
            return GetMapConfig(name).mapPrefix;
        }

        public string GetParentScene() {
            if (currentMap == null)
            {
                return null;
            }
            return GetParentScene(currentMap.name);
        }

        public string GetParentScene(string name) {
            return GetMapConfig(name).parentScene;
        }

        protected void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
            if (currentMap != null && scene.name == currentMap.currentLevel.currentStage.name) {
                SceneManager.SetActiveScene(scene);
                OnMapLoaded?.Invoke(currentMap, counter);
            }
        }
    }

    [System.Serializable]
    public class MapConfig {
        public MapManager map;
        public bool isDefault;
        public LoadSceneMode mode;
        public PSM.SceneType sceneType;
        public int sceneLimit = -1;
        public bool saveMap = true;
        public string parentScene = "Gameplay";
        public string mapPrefix = "Game/";
    }
}
﻿using System.Collections.Generic;
using UnityEngine;

namespace PepUp.Map {
    [CreateAssetMenu(menuName = "PepUp/Map/Map ID Settings")]
    public class MapIDSettings : ScriptableObject {

#if UNITY_EDITOR
        public string description;
#endif

        public Remap[] remaps;

        protected Dictionary<string, Remap> remap = new Dictionary<string, Remap>();

        public void Init() {
            int len = remaps.Length;
            for (int i = 0; i < len; i++) {
                Remap myRemap = remaps[i];
                remap.Add(myRemap.from, myRemap);
            }
        }

        public bool GetLevelID(int levelId, int stageId, out int remapLevelId) {
            return GetID(levelId, stageId, out remapLevelId, out int temp);
        }

        public bool GetStageID(int levelId, int stageId, out int remapStageId) {
            return GetID(levelId, stageId, out int temp, out remapStageId);
        }

        public bool GetID(int levelId, int stageId, out int remapLevelId, out int remapStageId) {
            if (remap.TryGetValue(MapManager.GetStringID(levelId, stageId), out Remap remapObj)) {
                remapLevelId = remapObj.levelId;
                remapStageId = remapObj.stageId;
                return true;
            }
            remapLevelId = -1;
            remapStageId = -1;
            return false;
        }
    }

    [System.Serializable]
    public class Remap {
        public string from;
        public int levelId;
        public int stageId;
    }
}
﻿using UnityEngine;
using PepUp.Events;
using PepUp.Utility;
using System.Collections.Generic;

namespace PepUp.Map {
    [CreateAssetMenu(menuName = "PepUp/Map/Map Manager")]
    public class MapManager : ScriptableObject {

        public MapIDSettings remapSettings;
        public string _mapFile = "mapInfo";
        public bool dontLoadFromSave;
        public int smartRandomLimit;

        public LevelData[] levels;

#if UNITY_EDITOR
        [Header("Editor"), SerializeField]
        private bool _dontLoadFromSave;
#endif

        public LevelData currentLevel { get; private set; }

        [SerializeField, HideInInspector]
        protected int currentLevelIndex;

        public event EventAction OnLevelLoad;

        protected Dictionary<int, int> levelMap = new Dictionary<int, int>();

        protected List<int> startRandomStages = new List<int>();
        protected List<int> smartRandomStages = new List<int>();

        public void Init() {
            int len = levels.Length;
            for (int i = 0; i < len; i++) {
                LevelData level = levels[i];
                if (levelMap.ContainsKey(level.id)) continue;
                levelMap.Add(level.id, level.index);
                startRandomStages.Add(i);
            }
            ResetSmartRandomStage();
            remapSettings?.Init();

            LevelSaveData levelSave = null;
#if UNITY_EDITOR
            if (!_dontLoadFromSave) {
                SaveUtility.LoadFromBinaryFormatter(_mapFile, out levelSave);
            }
#else
            if (!dontLoadFromSave) {
                SaveUtility.Load(_mapFile, out levelSave);
            }
#endif
            //Debug.Log("LOADED: " + levelSave.levelIndex);
            currentLevel = GetLevel(
                (levelSave == null) ?
                currentLevelIndex :
                GetLevelIndexByID(levelSave)
                );
            if (currentLevel == null) currentLevel = GetRandomLevel();
            currentLevel.remapSettings = remapSettings;
            currentLevel.saveData = levelSave;

            currentLevel.Initialize();
            OnLevelLoad?.Invoke(currentLevel);
        }

        public void SaveMap() {
            currentLevel.Save(_mapFile);
        }

        public LevelData StartMap(int levelIndex) {
            return StartMap(levels[levelIndex]);
        }

        public LevelData StartMap(LevelData level) {
            level.Reset();
            return UpdateMap(level);
        }

        public LevelData UpdateMap() {
            return UpdateMap(currentLevel);
        }

        public LevelData UpdateMap(int levelIndex) {
            return UpdateMap(levels[levelIndex]);
        }

        public LevelData UpdateMap(int levelIndex, int stageIndex) {
            LevelData level = levels[levelIndex];
            return UpdateMap(level, level.UpdateStage(stageIndex));
        }

        public LevelData UpdateMap(LevelData level) {
            return UpdateMap(level, level.UpdateStage());
        }

        public LevelData UpdateMap(LevelData level, Stage stage) {
            if (stage == null) {
                if (level.spawnStrategy == SpawnStrategy.order) {
                    level = GetLevel(level.nextLevelIndex);
                } else if (level.spawnStrategy == SpawnStrategy.random) {
                    level = GetRandomLevel();
                } else if (level.spawnStrategy == SpawnStrategy.smartRandom) {
                    level = GetSmartRandomLevel();
                }
                level.Initialize();
            }
            currentLevel = level;
            OnLevelLoad?.Invoke(currentLevel);
            return level;
        }

        protected LevelData GetRandomLevel() {
            return levels[Random.Range(0, levels.Length)];
        }

        protected void ResetSmartRandomStage() {
            smartRandomStages = startRandomStages;
        }

        protected LevelData GetSmartRandomLevel() {
            int smartCount = smartRandomStages.Count;
            int diff = startRandomStages.Count - smartCount;
            if (diff > smartRandomLimit) {
                ResetSmartRandomStage();
            }
            int index = Random.Range(0, smartCount);
            smartRandomStages.Remove(index);
            return levels[index];
        }

        public LevelData GetLevel(int index) {
            return levels[index];
        }

        public int GetLevelIndexByID(LevelSaveData level) {
            //backwards compatibility
            // > v0.1.1a1
            if (level.levelId == 0 && levelMap.TryGetValue(level.levelIndex + 1, out int index)) {
                return index;
            }
            // > v0.1
            if (remapSettings != null && remapSettings.GetLevelID(level.levelId, level.stageId, out index)) {
                index = GetLevelIndexByID(index);
                return index;
            }
            return GetLevelIndexByID(level.levelId);
        }

        public int GetLevelIndexByID(int id) {
            if (levelMap.TryGetValue(id, out int index)) {
                return index;
            }
            return 0;
        }

        public bool IsLevelLast() {
            return IsLevelLast(currentLevel);
        }

        public bool IsLevelLast(LevelData level) {
            return level.index == levels.Length - 1;
        }

        public bool IsStageLastInLevel() {
            return IsStageLastInLevel(currentLevel);
        }

        public bool IsStageLastInLevel(LevelData level) {
            return level.currentStage.localIndex == level.stages.Length - 1;
        }

        public bool IsStageLast() {
            return IsStageLast(currentLevel);
        }

        public bool IsStageLast(LevelData level) {
            return IsLevelLast(level) && IsStageLastInLevel(level);
        }

        public static string GetStringID(int levelId, int stageId) {
            return levelId + "$" + stageId;
        }

        public static int GetLevelID(string levelStageID) {
            return int.Parse(levelStageID.Split('$')[0]);
        }

        public static int GetStageID(string levelStageID) {
            return int.Parse(levelStageID.Split('$')[1]);
        }

#if UNITY_EDITOR

        private readonly Dictionary<string, int> levelIndexMap = new Dictionary<string, int>();

        public void OnValidate() {
            if (Application.isPlaying || levels == null) return;
            int len = levels.Length;
            for (int i = 0; i < len; i++) {
                if (levels[i] == null) return;
                levels[i].mapManager = this;
                levels[i].index = i;
                string levelName = levels[i].name;
                if (levelName != "" && !levelIndexMap.ContainsKey(levelName)) {
                    levelIndexMap.Add(levelName, i);
                }
            }

            for (int i = 0; i < len; i++) {
                if (levels[i].spawnStrategy == SpawnStrategy.order) {
                    levels[i].nextLevelIndex = levelIndexMap[levels[i].nextLevel];
                } else {
                    levels[i].nextLevelIndex = -1;
                }
                if (levels[i].isStart) {
                    currentLevelIndex = i;
                }
            }

            if (smartRandomLimit > len) {
                smartRandomLimit = len;
            }
            startRandomStages.Clear();
            smartRandomStages.Clear();
        }
#endif
    }
}
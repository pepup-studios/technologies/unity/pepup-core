﻿using PepUp.SceneManagement;
using UnityEngine.SceneManagement;

namespace PepUp.Map {
    [UnityEngine.AddComponentMenu("PepUp/Map/Map Scene Manager")]
    public class MapSceneController : SceneController {

        protected override void LoadScene(params object[] objs) {
            LoadSceneMode mode = (LoadSceneMode) objs[1];
            string parent = (string) objs[2];
            SceneType sceneType = (SceneType) objs[3];
            int sceneLimit;
            try {
                sceneLimit = (int) objs[4];
            }
            catch {
                sceneLimit = -1;
            }

            try {
                LevelData level = (LevelData) objs[0];
                LoadScene(level.currentStage.name, mode, parent, sceneType, sceneLimit);
            }
            catch {
                string sceneName = (string) objs[0];
                LoadScene(sceneName, mode, parent, sceneType, sceneLimit);
            }
        }

        protected override void LoadSceneAsync(params object[] objs) {
            LoadSceneMode mode = (LoadSceneMode) objs[1];
            string parent = (string) objs[2];
            SceneType sceneType = (SceneType) objs[3];
            int sceneLimit;
            try {
                sceneLimit = (int) objs[4];
            }
            catch {
                sceneLimit = -1;
            }

            try {
                LevelData level = (LevelData) objs[0];
                LoadSceneAsync(level.currentStage.name, mode, parent, sceneType, sceneLimit);
            }
            catch {
                string sceneName = (string) objs[0];
                LoadSceneAsync(sceneName, mode, parent, sceneType, sceneLimit);
            }
        }

        protected override void UnloadSceneAsync(params object[] objs) {
            try {
                LevelData level = (LevelData) objs[0];
                UnloadSceneAsync(level.currentStage.name);
            }
            catch {
                string sceneName = (string) objs[0];
                UnloadSceneAsync(sceneName);
            }
        }
    }
}

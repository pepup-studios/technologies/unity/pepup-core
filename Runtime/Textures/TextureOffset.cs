﻿using UnityEngine;

namespace PepUp {
    public class TextureOffset : MonoBehaviour {

        public MeshRenderer meshRenderer = null;

        [Space]
        public Texture texture = null;
        public Vector2 tile = Vector2.one;
        public Vector2 offset = Vector2.zero;

        [Space]
        public Transform target = null;
        public float offsetFactor = 0.05f;

        protected Vector3 lastTargetPos;

        public void Start() {
            meshRenderer.material.SetTexture("_MainTex", texture);
            meshRenderer.material.SetTextureScale("_MainTex", tile);
            meshRenderer.material.SetTextureOffset("_MainTex", offset);
        }

        void LateUpdate() {
            Vector3 shift = target.position - lastTargetPos;
            lastTargetPos = target.position;

            Vector2 offset = meshRenderer.material.mainTextureOffset;
            offset.x += -shift.x * offsetFactor;
            offset.y += -shift.y * offsetFactor;

            meshRenderer.material.SetTextureOffset("_MainTex", offset);
        }
    }
}
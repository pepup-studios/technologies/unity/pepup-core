﻿using UnityEngine;

namespace PepUp {
    public class TextureClamp : MonoBehaviour {

        public MeshRenderer meshRenderer = null;

        [Space]
        public string texPropName = "_MainTex";
        public Texture texture = null;
        public Vector2 tile = Vector2.one;
        public Vector2 offset = Vector2.zero;

        [Space]
        public Vector2 offsetStart = Vector2.zero;
        public Vector2 offsetEnd;
        public Vector2 offsetSpeed;

        protected Vector2 currOffset;
        protected Vector3 lastTargetPos;

        public void Start() {
            meshRenderer.material.SetTexture(texPropName, texture);
            meshRenderer.material.SetTextureScale(texPropName, tile);
            //meshRenderer.material.SetTextureOffset(texPropName, offset);
            meshRenderer.material.mainTextureOffset = currOffset = offset;// new Vector2(offset.x,offset.y);
        }

        void LateUpdate() {
            //Vector2 offset = meshRenderer.material.mainTextureOffset;
            currOffset += Time.unscaledDeltaTime * offsetSpeed;
            if (Vector2.Distance(currOffset, offsetEnd) < 0.1f) {
                meshRenderer.material.mainTextureOffset = currOffset = offsetStart;
                return;
            }
            meshRenderer.material.SetTextureOffset(texPropName, currOffset);
        }
    }
}
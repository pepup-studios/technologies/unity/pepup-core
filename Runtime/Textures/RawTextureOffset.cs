﻿using UnityEngine;
using UnityEngine.UI;
using PepUp.Events;

namespace PepUp {
    public class RawTextureOffset : MonoBehaviour {

        public RawImage rawImage = null;
        public bool activeOnStart;

        public RegisterEvent DoActive;
        public RegisterEvent DoDeactive;

        [Space]
        public Vector2 direction;
        public float speed = 0.05f;

        protected void Awake() {
            DoActive.Register(this, "Active");
            DoDeactive.Register(this, "Deactive");

            enabled = activeOnStart;
        }

        protected void OnDestroy() {
            DoActive.Unregister();
            DoDeactive.Unregister();
        }

        protected void Active(params object[] objs) {
            Active();
        }

        public void Active() {
            enabled = true;
        }

        protected void Deactive(params object[] objs) {
            Deactive();
        }

        public void Deactive() {
            enabled = false;
        }

        void LateUpdate() {
            Rect newRect = rawImage.uvRect;
            newRect.position = Vector2.Lerp(rawImage.uvRect.position, rawImage.uvRect.position + direction, speed * Time.deltaTime);

            rawImage.uvRect = newRect;
        }
    }
}
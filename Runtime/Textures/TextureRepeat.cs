﻿using UnityEngine;

namespace PepUp {
    public class TextureRepeat : MonoBehaviour {

        public MeshRenderer meshRenderer = null;

        [Space]
        public string texPropName = "_MainTex";
        public Texture texture = null;
        public Vector2 tile = Vector2.one;
        public Vector2 offset = Vector2.zero;
        public Color baseColor = Color.white;

        [Space]
        public Vector2 offsetSpeed;

        protected Vector3 lastTargetPos;

        public void Start() {
            meshRenderer.material.SetColor("_BaseColor", baseColor);
            meshRenderer.material.SetTexture(texPropName, texture);
            meshRenderer.material.SetTextureScale(texPropName, tile);
            meshRenderer.material.SetTextureOffset(texPropName, offset);
        }

        void LateUpdate() {
            Vector2 offset = meshRenderer.material.mainTextureOffset;
            offset += Time.time * offsetSpeed;
            meshRenderer.material.SetTextureOffset(texPropName, offset);
        }
    }
}
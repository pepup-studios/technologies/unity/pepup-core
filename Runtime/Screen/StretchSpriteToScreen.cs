using UnityEngine;

namespace PepUp {
    [RequireComponent(typeof(SpriteRenderer))]
    public class StretchSpriteToScreen : MonoBehaviour {

        protected SpriteRenderer spriteRenderer;

        public void Start() {
            spriteRenderer = this.GetComponent<SpriteRenderer>();
            Vector2 worldScreenSize = new Vector2(Screen.width, Screen.height);
            worldScreenSize = Camera.main.ScreenToWorldPoint(worldScreenSize) * 2;
            worldScreenSize.y = spriteRenderer.size.y;
            spriteRenderer.size = worldScreenSize;
        }
    }
}
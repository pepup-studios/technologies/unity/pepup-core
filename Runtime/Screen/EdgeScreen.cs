using UnityEngine;

namespace PepUp {
    public class EdgeScreen : MonoBehaviour {

        public BoundType boundType = BoundType.BoxCollider2D;
        public BoundDirection direction;
        public BoundSide side;
        public bool localOriginPoint;
        public Vector2 offset;

        public void Start() {
            Vector2 destination = transform.position;
            Bounds target = (boundType == BoundType.Renderer) ? GetComponent<Renderer>().bounds : GetComponent<BoxCollider2D>().bounds;
            Vector2 originPoint = localOriginPoint ? (Vector2) transform.position : Vector2.zero;
            Vector2 targetSize = Vector2.zero;
            switch (side) {
                case BoundSide.LeftOrTop:
                    targetSize = target.size / 2;
                    break;
                case BoundSide.RightOrBottom:
                    targetSize = target.size / 2 * -1;
                    break;
            }
            Camera cam = Camera.main;
            Vector2 screen = cam.ScreenToWorldPoint(new Vector2(cam.pixelWidth, cam.pixelHeight));
            switch (direction) {
                case BoundDirection.left:
                    destination.x = originPoint.x - ((targetSize.x * -1) + screen.x);
                    break;
                case BoundDirection.right:
                    destination.x = originPoint.x + (targetSize.x + screen.x);
                    break;
                case BoundDirection.up:
                    destination.y = originPoint.y + ((targetSize.y * -1) + screen.y);
                    break;
                case BoundDirection.down:
                    destination.y = originPoint.y - (targetSize.y + screen.y);
                    break;
            }
            transform.position = destination + offset;
        }

        public enum BoundType { BoxCollider2D, Renderer }
        public enum BoundSide { LeftOrTop, Middle, RightOrBottom }
        public enum BoundDirection { none, up, down, left, right, forward, back }
    }
}
﻿using UnityEngine;

namespace PepUp {
    public class ScaleToScreen : MonoBehaviour {

        public Camera mainCamera;
        public MeshRenderer meshRenderer;

        void Start() {
            float screenHeight = mainCamera.orthographicSize * 2;
            float height = meshRenderer.bounds.size.y;
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, screenHeight * transform.localScale.z / height);
        }
    }
}
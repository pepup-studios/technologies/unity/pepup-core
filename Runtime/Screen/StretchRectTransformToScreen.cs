﻿using UnityEngine;
using PepUp.Utility;

namespace PepUp {
    [RequireComponent(typeof(RectTransform))]
    public class StretchRectTransformToScreen : MonoBehaviour {

        protected void Awake() {
            Camera camera = Camera.main;
            float distance = Mathf.Abs(camera.transform.position.z - transform.position.z);
            float frustumWidth = CameraUtility.GetFrustumWidth(camera, distance);

            RectTransform rect = transform as RectTransform;
            rect.sizeDelta = new Vector2(frustumWidth, rect.sizeDelta.y);
        }
    }
}
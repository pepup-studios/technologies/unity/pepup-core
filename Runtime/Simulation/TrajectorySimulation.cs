﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using PepUp.Collections;
using PepUp.Utility;

namespace PepUp.Simulation {
    public abstract class TrajectorySimulation<T, J> : MonoBehaviour where T : MonoBehaviour, ISimulatable where J : Marker {

        public PhysicsSimulation physicsSimulation;
        public string physicSceneName = "trajectory";
        [Tooltip("Length of the simulation")]
        [Header("Properties")]
        public uint steps = 20;
        [Tooltip("Number of fixed frames for simulation")]
        [Range(1, 500)]
        public uint fixedFrames = 1;
        public float minThreshold = 0.5f;

        protected RegisteredObject<T> registeredObject;

        private int _currReferenceID;
        private EnumeratableHandler _simulateHandler;
        private IEnumerable _simulatable;

        public delegate void SimulateAction(T reference, J[] markers, object caller);
        public event SimulateAction OnCompleted;

        protected virtual void Awake() {
            physicsSimulation.StartMainSimulation();
            physicsSimulation.CreateScene(physicSceneName);

            _currReferenceID = 0;
            _simulateHandler = new EnumeratableHandler(this);
            registeredObject.reference = new Dictionary<int, T>();
            _simulatable = null;
        }

        protected virtual void OnDisable() {
            _simulateHandler.ClearEnumerators();
        }

        protected abstract void PrepareScene();

        public virtual void Register(T obj) {
            if (string.Compare(obj.gameObject.scene.name, physicSceneName) == 0) {
                _currReferenceID = obj.GetInstanceID();
                registeredObject.reference.Add(_currReferenceID, obj);
            } else {
                registeredObject.instance = obj;
            }
        }

        public virtual void Unregister(T obj) {
            if (string.Compare(obj.gameObject.scene.name, physicSceneName) == 0) {
                registeredObject.reference.Remove(obj.GetInstanceID());
            } else {
                registeredObject.instance = null;
            }
        }

        protected abstract J GetNewMarker();
        protected abstract J GetNewMarker(J marker);

        public virtual void ShowExecuteTrajectory() {
            PrepareScene();

            int id = _currReferenceID;
            ShowExecuteTrajectory(id);
        }

        public virtual void ShowExecuteTrajectory(int referenceID) {
            ClearTrajectory();

            SyncObject(referenceID);
            registeredObject.reference[referenceID].Execute();
            Simulate(referenceID);
        }

        public virtual void AsyncShowExecuteTrajectory() {
            PrepareScene();

            int id = _currReferenceID;
            SyncObject(id);
            registeredObject.reference[id].Execute();
            AsyncSimulate(id);
        }

        public virtual void ShowTrajectory() {
            PrepareScene();

            int id = _currReferenceID;
            ShowTrajectory(id);
        }

        public virtual void ShowTrajectory(int referenceID) {
            ClearTrajectory();

            SyncObject(referenceID);
            Simulate(referenceID);
        }

        protected virtual void AsyncSimulate(int referenceID) {
            fixedFrames = (uint)Mathf.Clamp(fixedFrames, 1, 500);
            IEnumerable markerSimulatable = SimulateMarkers(referenceID, fixedFrames);
            _simulateHandler.StartCoroutine(markerSimulatable);

            if (_simulatable == null) { //auto enter simulation steps
                _simulatable = SimulateSteps(fixedFrames);
                _simulateHandler.StartCoroutine(_simulatable);
            }
        }

        protected virtual void Simulate(int referenceID) {
            fixedFrames = (uint) Mathf.Clamp(fixedFrames, 1, 500);
            IEnumerable markerSimulatable = SimulateAll(referenceID, fixedFrames);
            _simulateHandler.StartCoroutine(markerSimulatable);
        }

        protected IEnumerable SimulateMarkers(int referenceID, uint frames) {
            List<J> tempMarkers = new List<J>();
            for (int i = 1; i <= steps; i++) {
                J newMaker = CreateMarker(referenceID, tempMarkers);
                if (newMaker == null) break;

                uint stepRate = steps / frames;
                if (i % stepRate == 0) {
                    yield return new WaitForFixedUpdate();
                }
            }
            SimulationCompleted(registeredObject.reference[referenceID], tempMarkers, this);
        }

        protected IEnumerable SimulateSteps(uint frames) {
            while (enabled) {
                for (int i = 1; i <= steps; i++) {
                    physicsSimulation.SimulatePhysics(physicSceneName, Time.fixedDeltaTime);

                    uint stepRate = steps / frames;
                    if (i % stepRate == 0) {
                        yield return new WaitForFixedUpdate();
                    }
                }
                if (_simulateHandler.enumerators.Count <= 1) { //auto exit simulation steps
                    ClearTrajectory();
                }
            }
        }

        protected IEnumerable SimulateAll(int referenceID, uint frames) {
            List<J> tempMarkers = new List<J>();
            for (int i = 1; i <= steps; i++) {
                physicsSimulation.SimulatePhysics(physicSceneName, Time.fixedDeltaTime);

                J newMaker = CreateMarker(referenceID, tempMarkers);
                if (newMaker == null) break;

                uint stepRate = steps / frames;
                if (i % stepRate == 0) {
                    yield return new WaitForFixedUpdate();
                }
            }
            SimulationCompleted(registeredObject.reference[referenceID], tempMarkers, this);
        }

        protected virtual J CreateMarker(int referenceID, List<J> markers) {
            T reference = registeredObject.reference[referenceID];
            J newMarker = GetNewMarker();
            int markerCount = markers.Count;

            //Index
            newMarker.index = markerCount;

            //Point
            Vector3 currPoint = reference.transform.position;
            newMarker.point = currPoint;

            if (markerCount > 0) {
                J prevMarker = markers[markerCount - 1];
                Vector3 relative = currPoint - prevMarker.point;

                //Direction
                prevMarker.direction = relative.normalized;

                //Displacement
                float distance = relative.magnitude;
                if (distance <= minThreshold) return null;

                float displacementValue = prevMarker.displacement;
                displacementValue += distance;
                newMarker.displacement = displacementValue;
            }

            //Time
            newMarker.time = markerCount * Time.fixedDeltaTime;

            markers.Add(newMarker);
            return newMarker;
        }

        protected virtual void SimulationCompleted(T reference, List<J> markers, object caller) {
#if UNITY_EDITOR
            this.markers = markers;
#endif
            OnCompleted?.Invoke(reference, markers.ToArray(), caller);
        }

        protected virtual void SyncObject(int referenceID) {
            T instance = registeredObject.instance;
            T reference = registeredObject.reference[referenceID];

            reference.transform.position = instance.transform.position;
            reference.transform.rotation = instance.transform.rotation;
        }

        public virtual void ClearTrajectory() {
            _simulateHandler.ClearEnumerators();
#if UNITY_EDITOR
            markers.Clear();
#endif
        }

        public J GetMarkerByDistance(J[] markers, Vector3 targetPoint) {
            return GetMarkerByDistance(markers, targetPoint, Vector3.one);
        }

        public J GetMarkerByDistance(J[] markers, Vector3 targetPoint, Vector3 distanceDir) {
            J nearestMarker = GetNearestFacingMarkerByDistance(markers, targetPoint, distanceDir);
            J newMarker = GetNewMarker(nearestMarker);

            //Point
            float pointDistance = Vector3Utility.Distance(nearestMarker.point, targetPoint, nearestMarker.direction);
            newMarker.point = nearestMarker.point + (nearestMarker.direction * pointDistance);

            //Displacement
            newMarker.displacement = nearestMarker.displacement + pointDistance;

            //Time
            newMarker.time = newMarker.displacement * nearestMarker.time / nearestMarker.displacement;

            return newMarker;
        }

        public static J GetNearestFacingMarkerByDistance(J[] markers, Vector3 targetPoint) {
            return GetNearestFacingMarkerByDistance(markers, targetPoint, Vector3.one);
        }

        public static J GetNearestFacingMarkerByDistance(J[] markers, Vector3 targetPoint, Vector3 distanceDir) {
            J nearestMarker = GetNearestMarkerByDistance(markers, targetPoint, distanceDir);

            Vector3 relativePos = targetPoint - nearestMarker.point;
            float dot = Vector3.Dot(relativePos, nearestMarker.direction);

            if (dot < 0) { //Marker is in front of the target
                nearestMarker = markers[nearestMarker.index - 1];
            }
            return nearestMarker;
        }

        public static J GetNearestMarkerByDistance(J[] markers, Vector3 targetPoint) {
            return GetNearestMarkerByDistance(markers, targetPoint, Vector3.one);
        }

        public static J GetNearestMarkerByDistance(J[] markers, Vector3 targetPoint, Vector3 distanceDir) {
            J nearestMarker = default;

            int min = 0;
            int max = markers.Length - 1;

            while (min <= max) {
                int mid = (min + max) / 2;
                int prev = mid - 1;
                int next = mid + 1;

                if (prev < min) {
                    prev = min;
                }
                if (next > max) {
                    next = max;
                }

                int minDistanceIndex = -1;
                float minDistance = -1;

                for (int i = prev; i <= next; i++) {
                    float distance = Vector3Utility.Distance(markers[i].point, targetPoint, distanceDir);
                    if (distance < minDistance || minDistance < 0) {
                        minDistance = distance;
                        minDistanceIndex = i - prev;
                    }
                }

                if (minDistanceIndex == 1 && min != mid) { //1 equals mid Distance
                    nearestMarker = markers[mid];
                    break;
                }
                if (max - min < 3) {
                    nearestMarker = (minDistanceIndex == 0) ? markers[prev] : markers[next];
                    break;
                }
                if (minDistanceIndex == 0) { //0 equals prev Distance
                    max = mid - 1;
                } else { //2 equals next Distance
                    min = mid + 1;
                }
            }
            return nearestMarker;
        }

        public static int[] GetIndexes(J[] markers) {
            return markers.Select((x) => x.index).ToArray();
        }

        public static Vector3[] GetPoints(J[] markers) {
            return markers.Select((x) => x.point).ToArray();
        }

        public static Vector3[] GetDirections(J[] markers) {
            return markers.Select((x) => x.direction).ToArray();
        }

        public static float[] GetDisplacements(J[] markers) {
            return markers.Select((x) => x.displacement).ToArray();
        }

        public static float[] GetTime(J[] markers) {
            return markers.Select((x) => x.time).ToArray();
        }

        //Causes build errors when put inside #if UNITY_EDITOR
        [Header("Editor Gizmos")]
        public bool drawLine;
        public Color lineColor = Color.red;
        public bool drawPoints;
        public Color pointColor = Color.yellow;
        public float pointSize = 0.05f;
        public bool drawLabel;
        public Color labelColor = Color.black;

        protected List<J> markers = new List<J>();

#if UNITY_EDITOR

        protected void OnDrawGizmos() {
            int markerCount = markers.Count;
            for (int i = 0; i < markerCount; i++) {
                J marker = markers[i];
                if (drawLine && i > 0) {
                    Gizmos.color = lineColor;
                    Gizmos.DrawLine(markers[i - 1].point, marker.point);
                }
                if (drawPoints) {
                    Gizmos.color = pointColor;
                    Gizmos.DrawSphere(marker.point, pointSize);
                }
                if (drawLabel) {
                    UnityEditor.Handles.color = labelColor;
                    UnityEditor.Handles.Label(marker.point, "Point " + i);
                }
            }
        }
#endif
    }
}

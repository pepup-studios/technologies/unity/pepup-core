﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PepUp.Simulation {
    [AddComponentMenu("PepUp/Simulation/Physics Simulation")]
    public class PhysicsSimulation : MonoBehaviour {

        private Scene _mainScene;
        private Dictionary<string, Scene> _physicsScenes = new Dictionary<string, Scene>();

        private bool _isPaused { get { return !enabled; } }

        void Awake() {
            Application.targetFrameRate = 60;
            _mainScene = SceneManager.GetActiveScene();
        }

        protected void OnEnable() {
            Physics.autoSimulation = false;
        }

        protected void OnDisable() {
            Physics.autoSimulation = true;
        }

        private void FixedUpdate() {
            Simulate(_mainScene, Time.fixedDeltaTime);
        }

        public Scene CreateScene(string name) {
            if (_physicsScenes.ContainsKey(name)) return GetScene(name);

            Scene newScene = SceneManager.CreateScene(name, new CreateSceneParameters(LocalPhysicsMode.Physics3D));
            _physicsScenes.Add(name, newScene);
            return newScene;
        }

        public void UnloadScene(string name) {
            Scene scene = _physicsScenes[name];
            SceneManager.UnloadSceneAsync(scene);
            _physicsScenes.Remove(name);
        }

        public Scene GetScene(string name) {
            return _physicsScenes[name];
        }

        public Scene GetMainScene() {
            return _mainScene;
        }

        public void SetActiveScene(string name) {
            Scene scene = GetScene(name);
            SceneManager.SetActiveScene(scene);
        }

        public void SetActiveMainScene() {
            SceneManager.SetActiveScene(_mainScene);
        }

        public void SimulatePhysics(string name, float frequency) {
            Scene scene = GetScene(name);
            Simulate(scene, frequency);
        }

        protected void Simulate(Scene scene, float frequency) {
            scene.GetPhysicsScene().Simulate(frequency);
        }

        public void StartMainSimulation() {
            enabled = true;
        }

        public void StopMainSimulation() {
            enabled = false;
        }
    }
}
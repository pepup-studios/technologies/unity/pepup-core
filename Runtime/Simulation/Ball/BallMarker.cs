﻿using UnityEngine;

namespace PepUp.Simulation {
    [System.Serializable]
    public class BallMarker : Marker {
        public Vector3 velocity;

        public BallMarker() {
        }

        public BallMarker(BallMarker marker) {
            index = marker.index;
            point = marker.point;
            direction = marker.direction;
            displacement = marker.displacement;
            time = marker.time;
            velocity = marker.velocity;
        }
    }
}
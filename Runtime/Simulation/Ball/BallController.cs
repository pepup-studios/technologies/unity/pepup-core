﻿using System.Collections;
using UnityEngine;

namespace PepUp.Simulation {
    public class BallController : MonoBehaviour, ISimulatable {

        public Rigidbody rb;

        [Space]
        public ForceData executeForce;
        public ForceData apposingForce;
        public ForceData collisionForce;

        [Space]
        public string collisionTag;

        [Space]
        public bool isReference;
        public BallTrajectory simulator;

        protected IEnumerator destroyBall;

        protected virtual void Awake() {
            if (isReference) {
                simulator.Register(this);
            }
        }

        public virtual void Execute() {
            rb.isKinematic = false;
            executeForce.AddForce(rb);
        }

        protected virtual void FixedUpdate() {
            apposingForce.AddForce(rb);
        }

        protected void OnCollisionEnter(Collision collision) {
            if (collision.gameObject.tag == collisionTag) {
                SpinBall();
            }
        }

        protected void SpinBall() {
            collisionForce.AddForce(rb);
        }
    }

    [System.Serializable]
    public struct ForceData {
        public Vector3 velocity;
        public ForceMode mode;

        public ForceData(Vector3 velocity, ForceMode mode) {
            this.velocity = velocity;
            this.mode = mode;
        }

        public void AddForce(Rigidbody rd) {
            rd.AddForce(velocity, mode);
        }
    }
}
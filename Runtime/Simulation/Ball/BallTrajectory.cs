﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using PepUp.Collections;

namespace PepUp.Simulation {
    [AddComponentMenu("PepUp/Simulation/Ball Trajectory")]
    public class BallTrajectory : TrajectorySimulation<BallController, BallMarker> {

        [Space]
        [Tooltip("Refernce of the ball which needs to be simulated.")]
        public BallController simulateBall;
        public string poolTag = "SimulateBall";

        protected ObjectPool pool;

        protected override void Awake() {
            physicsSimulation = GameObject.Find("BallSimulation").GetComponent<PhysicsSimulation>(); //Temporary fix, will be solved by IoC
            pool = ObjectPool.intance;

            base.Awake();
        }

        protected override void PrepareScene() {
            physicsSimulation.SetActiveScene(physicSceneName);

            BallController ball = pool.GetObjectByPop<BallController>(poolTag, simulateBall.gameObject.name);
            if (ball == null) {
                ball = Instantiate(simulateBall);
                ball.gameObject.name = simulateBall.gameObject.name;
            }
            Register(ball);
            ball.simulator = this;
            ball.gameObject.SetActive(true);

            physicsSimulation.SetActiveMainScene();
        }

        protected override void SimulationCompleted(BallController reference, List<BallMarker> markers, object caller) {
            pool.PutObject(poolTag, reference.gameObject.name, reference);
            reference.gameObject.SetActive(false);
            Unregister(reference);

            base.SimulationCompleted(reference, markers, this);
        }

        protected override void SyncObject(int referenceID) {
            base.SyncObject(referenceID);

            BallController instance = registeredObject.instance;
            BallController reference = registeredObject.reference[referenceID];

            reference.rb.velocity = instance.rb.velocity;
            reference.rb.angularVelocity = instance.rb.angularVelocity;
            reference.executeForce = instance.executeForce;
            reference.apposingForce = instance.apposingForce;
            reference.collisionForce = instance.collisionForce;
        }

        protected override BallMarker CreateMarker(int referenceID, List<BallMarker> markers) {
            BallMarker newMarker = base.CreateMarker(referenceID, markers);
            if (newMarker == null) return null;
            BallController reference = registeredObject.reference[referenceID];

            newMarker.velocity = reference.rb.velocity;
            //newMarker.time = newMarker.displacement / newMarker.velocity.magnitude;
            return newMarker;
        }

        protected override BallMarker GetNewMarker() {
            return new BallMarker();
        }

        protected override BallMarker GetNewMarker(BallMarker marker) {
            return new BallMarker(marker);
        }

        public static Vector3[] GetVelocities(BallMarker[] markers) {
            return markers.Select((x) => x.velocity).ToArray();
        }
    }
}
﻿using System.Collections.Generic;

namespace PepUp.Simulation {
    public struct RegisteredObject<T> where T : ISimulatable {

        public T instance;
        public Dictionary<int, T> reference;

        public RegisteredObject(T instance, Dictionary<int, T> reference) {
            this.instance = instance;
            this.reference = reference;
        }
    }
}
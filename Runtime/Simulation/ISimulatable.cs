﻿namespace PepUp.Simulation {
    public interface ISimulatable {

        void Execute();
    }
}
﻿using UnityEngine;

namespace PepUp.Simulation {
    [System.Serializable]
    public class Marker {

        public int index;
        public Vector3 point;
        public Vector3 direction;
        public float displacement;
        public float time;

        public Marker() {
        }

        public Marker(Marker marker) {
            index = marker.index;
            point = marker.point;
            direction = marker.direction;
            displacement = marker.displacement;
            time = marker.time;
        }
    }
}
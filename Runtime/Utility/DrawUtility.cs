﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace PepUp.Utility {
    public static class DrawUtility {

        public static void GizmoArrow(Vector3 pos, Vector3 direction, int width, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f) {
            DrawRay(pos, direction, width);

            Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * new Vector3(0, 0, 1);
            Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * new Vector3(0, 0, 1);
            DrawRay(pos + direction, right * arrowHeadLength, width);
            DrawRay(pos + direction, left * arrowHeadLength, width);
        }

        public static void GizmoArrow(Vector3 pos, Vector3 direction, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f) {
            Gizmos.DrawRay(pos, direction);

            Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * new Vector3(0, 0, 1);
            Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * new Vector3(0, 0, 1);
            Gizmos.DrawRay(pos + direction, right * arrowHeadLength);
            Gizmos.DrawRay(pos + direction, left * arrowHeadLength);
        }

        public static void DebugArrow(Vector3 pos, Vector3 direction, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f) {
            Debug.DrawRay(pos, direction);

            Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * new Vector3(0, 0, 1);
            Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * new Vector3(0, 0, 1);
            Debug.DrawRay(pos + direction, right * arrowHeadLength);
            Debug.DrawRay(pos + direction, left * arrowHeadLength);
        }

        public static void DebugArrow(Vector3 pos, Vector3 direction, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f) {
            Debug.DrawRay(pos, direction, color);

            Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * new Vector3(0, 0, 1);
            Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * new Vector3(0, 0, 1);
            Debug.DrawRay(pos + direction, right * arrowHeadLength, color);
            Debug.DrawRay(pos + direction, left * arrowHeadLength, color);
        }

        public static void DrawRay(Vector3 from, Vector3 to, int size) {
            int count = size; // how many lines are needed.
            if (count == 1)
                Gizmos.DrawRay(from, to);
            else {
                Camera c = SceneView.currentDrawingSceneView.camera;
                if (c == null) {
                    Debug.LogError("Camera.current is null");
                    return;
                }
                Vector3 v1 = (from - to).normalized; // line direction
                Vector3 v2 = (c.transform.position - from).normalized; // direction to camera
                Vector3 n = Vector3.Cross(v1, v2); // normal vector
                for (int i = 0; i < count; i++) {
                    Vector3 o = n * size * ((float) i / (count - 1) - 0.5f);
                    Gizmos.DrawRay(from + o, to + o);
                }
            }
        }

        public static void DrawLine(Vector3 from, Vector3 to, int size, Vector3 direction) {

        }
    }
}
#endif
﻿using UnityEngine;

namespace PepUp {
    public interface IPointable {

        Vector3 GetPoint();
    }
}
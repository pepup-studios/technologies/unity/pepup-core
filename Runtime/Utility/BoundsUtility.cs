using UnityEngine;

namespace PepUp.Utility {
    public static class BoundsUtility {

        public static BoundDirection2D CheckBounds2D(Collision2D collision) {
            if (collision.contacts.Length == 0) return BoundDirection2D.none;

            Collider2D collider = collision.collider;

            float RectWidth = collider.bounds.size.x;
            float RectHeight = collider.bounds.size.y;

            //Debug.Log("RectWidth " + RectWidth + " RectHeight " + RectHeight);

            Vector3 contactPoint = collision.contacts[0].point;
            Vector3 center = collider.bounds.center;

            if (contactPoint.y > center.y && //checks that circle is on top of rectangle
                contactPoint.x < center.x + (RectWidth / 2 - 0.08f) && contactPoint.x > center.x - (RectWidth / 2 - 0.08f)) {
                //Debug.Log("Up");
                return BoundDirection2D.up;
            }
            if (contactPoint.y < center.y &&
                   contactPoint.x < center.x + (RectWidth / 2 - 0.08f) && contactPoint.x > center.x - (RectWidth / 2 - 0.08f)) {
                //Debug.Log("Down");
                return BoundDirection2D.down;
                //Debug.Log("Contact Distance from center: " + (contactPoint.x - center.x));
            }
            if (contactPoint.x < center.x &&
                   (contactPoint.y < center.y + RectHeight / 2 && contactPoint.y > center.y - RectHeight / 2)) {
                //Debug.Log("Left");
                return BoundDirection2D.left;
            }
            if (contactPoint.x > center.x &&
                  (contactPoint.y < center.y + RectHeight / 2 && contactPoint.y > center.y - RectHeight / 2)) {
                //Debug.Log("Right");
                return BoundDirection2D.right;
            }
            return BoundDirection2D.none;
        }

        public static BoundDirection CheckBounds(Collision collision, float allowAngle = 0.75f) {
            if (collision.contacts.Length == 0) return BoundDirection.none;

            Collider collider = collision.collider;

            Vector3 contactNormal = -collision.contacts[0].normal;
            Transform colliderTransform = collider.transform;

            if (Vector3.Dot(colliderTransform.up, contactNormal) >= allowAngle) {
                return BoundDirection.up;
            }
            if (Vector3.Dot(-colliderTransform.up, contactNormal) >= allowAngle) {
                return BoundDirection.down;
            }
            if (Vector3.Dot(colliderTransform.right, contactNormal) >= allowAngle) {
                return BoundDirection.right;
            }
            if (Vector3.Dot(-colliderTransform.right, contactNormal) >= allowAngle) {
                return BoundDirection.left;
            }
            if (Vector3.Dot(colliderTransform.forward, contactNormal) >= allowAngle) {
                return BoundDirection.forward;
            }
            if (Vector3.Dot(-colliderTransform.forward, contactNormal) >= allowAngle) {
                return BoundDirection.back;
            }

            return BoundDirection.none;
        }
    }

    public enum BoundDirection { none, up, down, left, right, forward, back }
    public enum BoundDirection2D { none, up, down, left, right }
}
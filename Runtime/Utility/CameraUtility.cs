﻿using UnityEngine;

namespace PepUp.Utility {
    public static class CameraUtility {

        public static float GetFrustumHeight(Camera camera, float distance = 0) {
            return 2.0f * distance * Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad);
        }

        public static float GetFrustumWidth(Camera camera, float distance = 0) {
            float frustumHeight = GetFrustumHeight(camera, distance);
            return GetFrustumWidth(frustumHeight, camera.aspect);
        }

        public static float GetFrustumWidth(float height, Camera camera) {
            return GetFrustumWidth(height, camera.aspect);
        }

        public static float GetFrustumWidth(float height, float aspect) {
            return height * aspect;
        }

        public static Vector3 GetSceenEdgePosition(Camera camera, float distance, Vector3 pos, Vector3 padding = default) {
            Vector3 center = camera.transform.position;
            float height = GetFrustumHeight(camera, distance);
            float width = GetFrustumWidth(height, camera);

            Vector3 newPos = pos;

            float maxHeight;
            float maxWidth;

            if (pos.x < center.x && pos.x < (maxWidth = center.x - (width / 2))) {
                newPos.x = maxWidth + padding.x;
            } else if (pos.x >= center.x && pos.x >= (maxWidth = center.x + (width / 2))) {
                newPos.x = maxWidth - padding.x;
            }

            if (pos.y < center.y && pos.y < (maxHeight = center.y - (height / 2))) {
                newPos.y = maxHeight + padding.y;
            } else if (pos.y >= center.y && pos.y >= (maxHeight = center.y + (height / 2))) {
                newPos.y = maxHeight - padding.y;
            }
            return newPos;
        }
    }
}

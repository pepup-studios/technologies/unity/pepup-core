﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using System;
using System.Text;
using System.Security.Cryptography;

namespace PepUp.Utility {
    public static class SaveUtility {

        [Obsolete("Save is deprecated, please use SaveAsBinaryFormatter instead.")]
        public static bool Save<T>(T data, string fileName) {
            return SaveAsBinaryFormatter(data, fileName);
        }

        [Obsolete("Load is deprecated, please use LoadFromBinaryFormatter instead.")]
        public static bool Load<T>(string fileName, out T data) {
            return LoadFromBinaryFormatter(fileName, out data);
        }

        public static bool SaveAsJson(object data, string fileName, bool prettyPrint = false, string ext = ".json") {
            try {
                string jsonData = JsonUtility.ToJson(data, prettyPrint);
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Create(Application.persistentDataPath + "/" + fileName + ext);

                bf.Serialize(file, jsonData);
                file.Close();
                return true;
            }
            catch {
                return false;
            }
        }

        public static bool LoadFromJson(string fileName, object data, string ext = ".json") {
            string directory = Application.persistentDataPath + "/" + fileName + ext;
            if (File.Exists(directory)) {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(directory, FileMode.Open);

                string jsonData = bf.Deserialize(file).ToString();
                JsonUtility.FromJsonOverwrite(jsonData, data);
                file.Close();
                return true;
            }
            return false;
        }

        public static string GetEncryptedHash(string jsonData) {
            //Setup SHA
            SHA256Managed crypt = new SHA256Managed();
            string hash = String.Empty;

            //Compute Hash
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(jsonData), 0, Encoding.UTF8.GetByteCount(jsonData));

            //Convert to Hex
            foreach (byte bit in crypto) {
                hash += bit.ToString("x2");
            }

            return hash;
        }

        public static bool WriteAsJson(string jsonData, string fileName, string ext = ".json") {
            try {
                FileStream file = File.Create(Application.persistentDataPath + "/" + fileName + ext);

                using (StreamWriter writer = new StreamWriter(file)) {
                    writer.Write(jsonData);
                    return true;
                }
            }
            catch {
                return false;
            }
        }

        public static string ReadFromJson(string fileName, string ext = ".json") {
            string directory = Application.persistentDataPath + "/" + fileName + ext;
            if (File.Exists(directory)) {
                using (StreamReader reader = new StreamReader(directory)) {
                    return reader.ReadToEnd();
                }
            }
            return null;
        }

        public static bool SaveAsBinaryFormatter<T>(T data, string fileName, string ext = ".dat") {
            try {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Create(Application.persistentDataPath + "/" + fileName + ext);

                bf.Serialize(file, data);
                file.Close();
                return true;
            }
            catch {
                return false;
            }
        }

        public static bool LoadFromBinaryFormatter<T>(string fileName, out T data, string ext = ".dat") {
            string directory = Application.persistentDataPath + "/" + fileName + ext;
            if (File.Exists(directory)) {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(directory, FileMode.Open);

                T newData = (T) bf.Deserialize(file);
                file.Close();
                data = newData;
                return true;
            }
            data = default;
            return false;
        }

        public static bool SaveTexture2D(Texture2D data, string fileName){
            try {
                File.WriteAllBytes(Application.persistentDataPath + "/" + fileName + ".jpg", data.EncodeToJPG());
                return true;
            }
            catch (Exception e) {
                Debug.LogException(e);
                return false;
            }
        }

        public static bool LoadTexture2D(string fileName, out Texture2D data) {
            string directory = Application.persistentDataPath + "/" + fileName + ".jpg";
            try {
                if (File.Exists(directory)) {
                    byte[] bytes = File.ReadAllBytes(directory);
                    Texture2D newTex = new Texture2D(1, 1);
                    newTex.LoadImage(bytes);
                    data = newTex;
                    return true;
                }
            } catch {}

            data = null;
            return false;
        }
    }
}
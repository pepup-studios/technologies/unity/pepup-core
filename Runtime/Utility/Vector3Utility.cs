﻿using UnityEngine;

namespace PepUp.Utility {
    public static class Vector3Utility {

        public static float Distance(Vector3 a, Vector3 b, Vector3 dir) {
            return Vector3.Scale(a - b, dir.normalized).magnitude;
        }

        public static Vector3 MultiplyByAxis(Vector3 a, Vector3 axis, float value) {
            return Vector3.Scale(a, axis) * value;
        }

        public static Vector3 MultiplyByAxis(Vector3 a, AxisDirection axis, Vector3 value) {
            Vector3 x = Vector3.Scale(a, axis.right) * value.x;
            Vector3 y = Vector3.Scale(a, axis.up) * value.y;
            Vector3 z = Vector3.Scale(a, axis.forward) * value.z;
            return x + y + z;
        }

        public static Vector3 RoundToInt(Vector3 a) {
            a.x = Mathf.RoundToInt(a.x);
            a.y = Mathf.RoundToInt(a.y);
            a.z = Mathf.RoundToInt(a.z);
            return a;
        }
    }

    public struct AxisDirection {
        public Vector3 up;
        public Vector3 right;
        public Vector3 forward;

        public AxisDirection(Vector3 up, Vector3 right, Vector3 forward) {
            this.up = up;
            this.right = right;
            this.forward = forward;
        }
    }
}

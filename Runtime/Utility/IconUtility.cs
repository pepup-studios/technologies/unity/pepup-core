﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Reflection;

namespace PepUp.Utility {
    public enum IconType {
        greyLong, blueLong, cyanLong, greenLong, yellowLong, orangeLong, redLong, magentaLong,
        greyRound, blueRound, cyanRound, greenRound, yellowRound, orangeRound, redRound, magentaRound,
        greyDiamond, blueDiamond, cyanDiamond, greenDiamond, yellowDiamond, orangeDiamond, redDiamond, magentaDiamond,
        none = -1
    }

    public static class IconUtility {

        private static readonly string[] prefixlabels = { "sv_label_", "sv_icon_dot",  };
        private static readonly string[] suffixlabels = { string.Empty, "_pix16_gizmo" };

        public static void DrawIcon(GameObject gameObject, IconType iconType) {
            if (iconType == IconType.none) return;
            int iconIndex = iconType.GetHashCode();
            int labelIndex = (iconIndex < 8) ? 0 : 1;
            var largeIcons = GetTextures(prefixlabels[labelIndex], suffixlabels[labelIndex], 0, 8);
            var icon = largeIcons[iconIndex - 8];
            var egu = typeof(EditorGUIUtility);
            var flags = BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.NonPublic;
            var args = new object[] { gameObject, icon.image };
            var setIcon = egu.GetMethod("SetIconForObject", flags, null, new System.Type[] { typeof(Object), typeof(Texture2D) }, null);
            setIcon.Invoke(null, args);
        }

        private static GUIContent[] GetTextures(string baseName, string postFix, int startIndex, int count) {
            GUIContent[] array = new GUIContent[count];
            for (int i = 0; i < count; i++) {
                array[i] = EditorGUIUtility.IconContent(baseName + (startIndex + i) + postFix);
            }
            return array;
        }
    }
}
#endif
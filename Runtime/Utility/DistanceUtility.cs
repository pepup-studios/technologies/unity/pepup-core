﻿using UnityEngine;

namespace PepUp.Utility {
    public static class DistanceUtility {

        public static T GetNearestPointByDistance<T>(T[] points, Vector3 targetPoint, Vector3 distanceDir)
            where T : IPointable {
            T nearestMarker = default;

            int min = 0;
            int max = points.Length - 1;

            while (min <= max) {
                int mid = (min + max) / 2;
                int prev = mid - 1;
                int next = mid + 1;

                if (prev < min) {
                    prev = min;
                }
                if (next > max) {
                    next = max;
                }

                int minDistanceIndex = -1;
                float minDistance = -1;

                for (int i = prev; i <= next; i++) {
                    float distance = Vector3Utility.Distance(points[i].GetPoint(), targetPoint, distanceDir);
                    if (distance < minDistance || minDistance < 0) {
                        minDistance = distance;
                        minDistanceIndex = i - prev;
                    }
                }

                if (minDistanceIndex == 1 && min != mid) { //1 equals mid Distance
                    nearestMarker = points[mid];
                    break;
                }
                if (max - min < 3) {
                    nearestMarker = (minDistanceIndex == 0) ? points[prev] : points[next];
                    break;
                }
                if (minDistanceIndex == 0) { //0 equals prev Distance
                    max = mid - 1;
                } else { //2 equals next Distance
                    min = mid + 1;
                }
            }
            return nearestMarker;
        }
    }
}
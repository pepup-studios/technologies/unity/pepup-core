﻿using PepUp.Events;
using UnityEngine;

namespace PepUp
{
    [RequireComponent(typeof(AudioSource))]
    public class SoundHandler : MonoBehaviour
    {
        public bool overrideSoundManager;
        public string awakeSoundName;
        public bool isMusicDefault;

        public RegisterEvent DoPlayAudioByName;
        public RegisterEvent DoStopAudioByName;

        [SerializeField, HideInInspector]
        private AudioSource _audioSource;
        public AudioSource audioSource { get { return _audioSource; } protected set { _audioSource = value; } }

        protected SoundManager soundManager;

        public void Awake()
        {
            Init();
        }

        protected void Init() {
            if (BaseGameManager.instance == null) {
                enabled = false;
                return;
            }
            soundManager = BaseGameManager.instance.soundManager; //temp
        }

        public void OnEnable()
        {
            if (soundManager == null) {
                enabled = false;
                return;
            }

            DoPlayAudioByName.Register(this, "PlayAudioByName");
            DoStopAudioByName.Register(this, "StopAudioByName");

            if (overrideSoundManager) return;

            if (!string.IsNullOrEmpty(awakeSoundName)) {
                PlayAudioByName(awakeSoundName);
            }

            if (!isMusicDefault) {
                soundManager.OnSoundOn += SoundOn;
                soundManager.OnSoundOff += SoundOff;
            } else {
                soundManager.OnMusicOn += SoundOn;
                soundManager.OnMusicOff += SoundOff;
            }
        }

        public void OnDisable()
        {
            DoPlayAudioByName.Unregister();
            DoStopAudioByName.Unregister();

            if (soundManager == null) return;

            soundManager.OnSoundOn -= SoundOn;
            soundManager.OnSoundOff -= SoundOff;

            soundManager.OnMusicOn -= SoundOn;
            soundManager.OnMusicOff -= SoundOff;
        }

        void SoundOn(params object[] objs)
        {
            if (overrideSoundManager)
            {
                OnDisable();
                return;
            }
            if(audioSource != null) audioSource.mute = false;
        }

        void SoundOff(params object[] objs)
        {
            if (overrideSoundManager)
            {
                OnDisable();
                return;
            }
            if (audioSource != null) audioSource.mute = true;
        }

        public void InitPlayAudioByName(string name) {
            Init();
            PlayAudioByName(name);
        }

        public void PlayAudioByName(params object[] objs)
        {
            PlayAudioByName((string)objs[0]);
        }

        public void PlayAudioByNameAnim(string name) {
            PlayAudioByName(name);
        }

        public void PlayAudioByName(string name)
        {
            SetSoundStatus(name);
            if (audioSource != null && soundManager != null)
            {
                audioSource.clip = soundManager.GetAudioClipByName(name);
                audioSource.Play();
            }
        }

        public void StopAudioByName(params object[] objs) {
            StopAudioByName((string) objs[0]);
        }

        public void StopAudioByName(string name)
        {
            if (audioSource != null && soundManager != null)
            {
                audioSource.clip = soundManager.GetAudioClipByName(name);
                audioSource.Stop();
            }
        }

        public void PlayDefault()
        {
            if (audioSource != null) audioSource.Play();
        }

        public void StopDefault()
        {
            if (audioSource != null) audioSource.Stop();
        }

        protected void SetSoundStatus(string clipName) {
            if (soundManager != null && soundManager.GetActiveStatusByClipName(clipName)) SoundOn(); else SoundOff();
        }

#if UNITY_EDITOR
        protected void OnValidate()
        {
            if (Application.isPlaying) return;
            _audioSource = GetComponent<AudioSource>();
        }
#endif
    }
}
﻿using UnityEngine;
using UnityEngine.UI;

namespace PepUp {
    [RequireComponent(typeof(Toggle))]
    public class SoundToggle : MonoBehaviour {

        [SerializeField]
        private bool isMusic;

        [SerializeField] [HideInInspector]
        protected Toggle toggle;

        protected SoundManager soundManager;

        protected void OnEnable() {
            soundManager = BaseGameManager.instance.soundManager;

            if (!isMusic) {
                if (soundManager.SoundActive) ToggleOn(); else ToggleOff();
                soundManager.OnSoundOn += ToggleOn;
                soundManager.OnSoundOff += ToggleOff;
            } else {
                if (soundManager.MusicActive) ToggleOn(); else ToggleOff();
                soundManager.OnMusicOn += ToggleOn;
                soundManager.OnMusicOff += ToggleOff;
            }
        }

        protected void OnDisable() {
            soundManager.OnSoundOn -= ToggleOn;
            soundManager.OnSoundOff -= ToggleOff;

            soundManager.OnMusicOn -= ToggleOn;
            soundManager.OnMusicOff -= ToggleOff;
        }

        public void ToggleOn(params object[] objs) {
            toggle.isOn = true;
        }

        public void ToggleOff(params object[] objs) {
            toggle.isOn = false;
        }

#if UNITY_EDITOR
        protected void OnValidate() {
            toggle = GetComponent<Toggle>();
        }
#endif
    }
}
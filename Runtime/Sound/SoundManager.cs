﻿using System.Collections.Generic;
using PepUp.Events;
using PepUp.Utility;
using UnityEngine;

namespace PepUp {
    [CreateAssetMenu(menuName = "PepUp/Sound/Manager")]
    public class SoundManager : ScriptableObject {

        [SerializeField]
        private bool _soundActive = true;
        [SerializeField]
        private bool _musicActive = true;

        public SoundClip[] audios;

        public bool SoundActive {
            get { return _soundActive; }
            set {
                SetSoundActive(value);
            }
        }

        public bool MusicActive {
            get { return _musicActive; }
            set {
                SetMusicActive(value);
            }
        }

        public event EventAction OnSoundOn;
        public event EventAction OnSoundOff;

        public event EventAction OnMusicOn;
        public event EventAction OnMusicOff;

        protected Dictionary<string, SoundClip> audioClip;

        private const string _soundFile = "soundInfo";
        private SoundData soundData;

        public void Init() {
            audioClip = new Dictionary<string, SoundClip>();
            for (int i = 0; i < audios.Length; i++) {
                audioClip.Add(audios[i].name, audios[i]);
            }
            LoadSoundState();
        }

        public AudioClip GetAudioClipByName(string name) {
            int len = audioClip[name].clips.Length;
            return audioClip[name].clips[Random.Range(0, len)];
        }

        public bool GetActiveStatusByClipName(string name) {
            return audioClip[name].isMusic ? _musicActive : _soundActive;
        }

        private void LoadSoundState() {
            if (SaveUtility.LoadFromBinaryFormatter(_soundFile, out soundData)) {
                SoundActive = soundData.soundActive;
                MusicActive = soundData.musicActive;
            } else {
                soundData = new SoundData();
                SoundActive = true;
                MusicActive = true;
            }
        }

        public void SetSoundActive(bool value) {
            SetSoundActiveWithoutSave(value);
            SaveSoundState(soundData);
        }

        public void SetSoundActiveWithoutSave(bool value) {
            soundData.soundActive = _soundActive = value;
            if (_soundActive) OnSoundOn?.Invoke(); else OnSoundOff?.Invoke();
        }

        public void SetMusicActive(bool value) {
            SetMusicActiveWithoutSave(value);
            SaveSoundState(soundData);
        }

        public void SetMusicActiveWithoutSave(bool value) {
            soundData.musicActive = _musicActive = value;
            if (_musicActive) OnMusicOn?.Invoke(); else OnMusicOff?.Invoke();
        }

        private void SaveSoundState(SoundData data) {
            SaveUtility.SaveAsBinaryFormatter(data, _soundFile);
        }
    }

    [System.Serializable]
    public class SoundData {
        public bool soundActive;
        public bool musicActive;
    }

    [System.Serializable]
    public class SoundClip {
        public string name;
        public bool isMusic;
        public AudioClip[] clips;
    }
}
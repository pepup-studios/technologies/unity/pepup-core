﻿using System;
using UnityEngine;
using System.Reflection;
using PepUp.Events;

namespace PepUp.Analytics {
    public abstract class AnalyticsBase<T> : MonoBehaviour where T : AnalyticalData {

        protected abstract void OnEnable();

        protected abstract void OnDisable();

        protected virtual void RegisterEvents(T[] data) {
            foreach (var value in data) {
                CreateDelegate(value, out EventInfo eventInfo, out Delegate d);
                MethodInfo eventMethod = eventInfo.GetAddMethod();
                eventMethod.Invoke(value.eventData.speaker, new[] { d });
            }
        }

        protected virtual void UnregisterEvents(T[] data) {
            foreach (var value in data) {
                CreateDelegate(value, out EventInfo eventInfo, out Delegate d);
                MethodInfo eventMethod = eventInfo.GetRemoveMethod();
                eventMethod.Invoke(value.eventData.speaker, new[] { d });
            }
        }

        protected virtual bool CreateDelegate(T data, out EventInfo eventInfo, out Delegate d) {
            string eventHandler = data.eventData.eventHandler;
            eventInfo = data.eventData.speaker.GetType().GetEvent(data.eventData.eventName);
            d = null;
            if (eventInfo != null) {
                if (data.type == AnalyticalDataType.Event) {
                    d = Delegate.CreateDelegate(eventInfo.EventHandlerType, data.eventData.listener.GetType(), eventHandler, true, true);
                }
                return true;
            }
            return false;
        }
    }

    [Serializable]
    public class AnalyticalData {
        public AnalyticalDataType type;
        public string description;
        public EventData eventData;
        //public LogData log;
    }

//    [Serializable]
//    public abstract class LogData {

//        public string name;
//        public LogParameter[] parameters;

//#if UNITY_EDITOR
//        public bool isFold = true;
//        public int parameterSize;
//#endif

//        public abstract void DoLogEvent(params object[] objs);
//        public abstract Delegate GetDelegate(EventData eventData, EventInfo eventInfo);
//    }

    [Serializable]
    public class LogParameter {
        public AnalyticalFieldType type;
        public UnityEngine.Object obj;
        public UnityEngine.Object component;

        public string value;
        public object valueObj;
        public string fieldName;
        public string displayName;

        public Type GetValueType() {
            return valueObj.GetType();
        }

        public object GetValue() {
            Type valueType = GetValueType();

            if (valueType == typeof(double)) {
                return (double) valueObj;
            }
            if (valueType == typeof(float)) {
                return (float) Convert.ToDecimal(valueObj);
            }
            if (valueType == typeof(long)) {
                return (long) valueObj;
            }
            if (valueType == typeof(int)) {
                return (int) Convert.ToInt64(valueObj);
            }
            return valueObj.ToString();
        }

        public static string GetDisplayFieldName(string name) {
            if (!string.IsNullOrEmpty(name) && name.Length > 1) {
                if (name[0] == '_') {
                    return name.Substring(1);
                }
                if (name[0] == '<') {
                    return name.Substring(1).Split('>')[0];
                }
            }
            return name;
        }
    }

    public enum AnalyticalDataType { Log, Event }
    public enum AnalyticalFieldType { Object, Manual }
}
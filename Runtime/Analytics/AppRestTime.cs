﻿using System;
using System.Collections;
using PepUp.Events;
using PepUp.Utility;
using UnityEngine;

public class AppRestTime : MonoBehaviour {

    [Range(0, 60)]
    public int calculateTime = 1;
    public string saveFile = "lastSession";
    public EventObject OnCalculated;

    [Space]
    public RegisterEvent DoCalculateRestTime;
    public RegisterEvent OnAppEnded;

    public double restTime { get; private set; }

    protected IEnumerator doStart;
    protected DateTime savedDateTime;
    protected TimeSpan restTimeSpan;
    protected bool didLoad;

    protected void Awake() {
        didLoad = SaveUtility.LoadFromBinaryFormatter(saveFile, out savedDateTime);
        restTimeSpan = DateTime.Now - savedDateTime;
    }

    protected void OnEnable() {
        DoCalculateRestTime.Register(this, "CalculateRestTime");
        OnAppEnded.Register(this, "AppEnded");
    }

    protected void OnDisable() {
        DoCalculateRestTime.Unregister();
        OnAppEnded.Unregister();

        if (doStart != null) StopCoroutine(doStart);
    }

    protected IEnumerator StartCalculating(float time) {
        yield return new WaitForSeconds(time);
        CalculateRestTime();
    }

    protected void CalculateRestTime(params object[] objs) {
        doStart = StartCalculating(calculateTime);
        StartCoroutine(doStart);
    }

    public void CalculateRestTime() {
        if (didLoad) {
            restTime = restTimeSpan.TotalSeconds;
        } else {
            restTime = 0;
        }

        OnCalculated?.Invoke(restTime);
    }

    public void AppEnded(params object[] objs) {
        SaveUtility.SaveAsBinaryFormatter(DateTime.Now, saveFile);
    }
}

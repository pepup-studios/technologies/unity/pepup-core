﻿using UnityEngine;
using PepUp.Collections;
using PepUp.Events;

namespace PepUp
{
    public class TapDetector : MonoBehaviour
    {
        public EventObject DoTapDown;
        public EventObject DoTapUp;
        private Timer timer;

        private Vector3 tapPosition;

        private float timeCount = 0;
        private float maxTapTime = 1f;
        private float maxDistance = 1f;
        private bool isTapDown = false;

        private void Start()
        {
            timer = new Timer(1f);
        }

        public void OnTapDown()
        {
            isTapDown = true;
            timeCount = 0;
            tapPosition = Input.mousePosition;

            DoTapDown?.Invoke(this, tapPosition);
        }

        public void OnTapUp()
        {
            isTapDown = false;
            Vector3 relative = GetSwipeDirection();

            float distance = relative.magnitude;
            if (timeCount <= maxTapTime && distance <= maxDistance)
            {
                DoTapUp?.Invoke(this, tapPosition);
            }
        }

        public Vector3 GetSwipeDirection()
        {
            Vector3 position = Input.mousePosition;
            return position - tapPosition;
        }

        private void Update()
        {
            timer.Update();
            if (isTapDown)
            {
                timeCount = timeCount + Time.deltaTime;
            }
        }
    }
}

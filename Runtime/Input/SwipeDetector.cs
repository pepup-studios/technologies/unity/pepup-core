﻿using UnityEngine;
using PepUp.Events;

namespace PepUp
{
    public class SwipeDetector : MonoBehaviour
    {
        public float minThershold;
        public EventObject OnSwipeStarted;
        public EventObject OnSwipping;
        public EventObject OnSwipeEnded;

        protected Vector3 pressPosition;
        //protected bool didInstantSwipe;

        public void OnDragBegin()
        {
            //didInstantSwipe = false;
            pressPosition = Input.mousePosition;
            OnSwipeStarted?.Invoke(this, pressPosition);
        }

        public void OnDragging()
        {
            OnSwipping?.Invoke(this, Input.mousePosition);
            //if (didInstantSwipe) return;
            //OnDragEnded();
        }

        public void OnDragEnded()
        {
            //if (didInstantSwipe) return;

            Vector3 relative = GetSwipeDirection();
            float distance = relative.magnitude;
            if (distance < minThershold) return;
            Vector3 swipeDir = relative.normalized;

            OnSwipeEnded?.Invoke(this, swipeDir, relative, distance);

            //didInstantSwipe = true;
        }

        public Vector3 GetSwipeDirection()
        {
            Vector3 position = Input.mousePosition;
            return position - pressPosition;
        }

        public static SwipeDirection GetSwipeEnum(Vector3 swipeDir)
        {
            float positiveX = Mathf.Abs(swipeDir.x);
            float positiveY = Mathf.Abs(swipeDir.y);
            if (positiveX > positiveY)
            {
                return (swipeDir.x > 0) ? SwipeDirection.Right : SwipeDirection.Left;
            }
            return (swipeDir.y > 0) ? SwipeDirection.Up : SwipeDirection.Down;
        }


        public static Vector3 GetFourSwipeDirection(Vector3 dragVector)
        {
            return GetFourSwipeDirection(dragVector.x, dragVector.y);
        }

        public static Vector3 GetFourSwipeDirection(float a, float b)
        {
            float positiveA = Mathf.Abs(a);
            float positiveB = Mathf.Abs(b);
            if (positiveA > positiveB)
            {
                return (a > 0) ? Vector3.right : Vector3.left;
            }
            return (b > 0) ? Vector3.forward : Vector3.back;
        }

        public static Vector3 GetFourSwipeDirection(Vector3 swipeDir, Transform target)
        {
            Transform source = Camera.main.transform;
            Vector3 forward = source.forward;
            Vector3 right = source.right;

            float dot = Vector3.Dot(source.up, target.forward);
            if (Mathf.Abs(dot) > 0.75f)
            {
                forward = source.up;
            }
            Vector3 desiredDir = forward.normalized * swipeDir.y + right.normalized * swipeDir.x;
            return GetFourSwipeDirection(desiredDir.x, desiredDir.z);
        }
    }

    public enum SwipeDirection { Up, Down, Right, Left }
}
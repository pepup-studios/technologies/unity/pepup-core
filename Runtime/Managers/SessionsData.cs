﻿using System;
using System.Collections.Generic;

namespace PepUp.Managers
{
    [Serializable]
    public class SessionsData
    {
        public List<SessionData> sessionData;
    }
}
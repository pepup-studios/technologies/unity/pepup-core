﻿using PepUp.Utility;

namespace PepUp.Managers
{
    public class IntCounterController : BaseCounter
    {
        public string saveFile;
        public int defaultCounter;
        public IntObject counter;

        protected override void Awake() {
            base.Awake();
            Load();
        }

        protected override void Reset(params object[] objs)
        {
            Reset();
        }

        public void Reset()
        {
#if UNITY_EDITOR
            if (counter == null) return;
#endif
            counter.value = 0;
            OnReset?.Invoke(this, counter);
        }

        protected override void Increment(params object[] objs)
        {
            int value;
            try {
                try {
                    value = (int) objs[1];
                }
                catch {
                    value = ((IntObject) objs[1]).value;
                }
            }
            catch {
                try {
                    value = (int) objs[0];
                }
                catch {
                    value = ((IntObject) objs[0]).value;
                }
            }
            counter.value += value;
            OnUpdate?.Invoke(this, counter.value);
        }

        protected override void Decrement(params object[] objs) {
            int value;
            try {
                try {
                    value = (int) objs[1];
                }
                catch {
                    value = ((IntObject) objs[1]).value;
                }
            }
            catch {
                try {
                    value = (int) objs[0];
                }
                catch {
                    value = ((IntObject) objs[0]).value;
                }
            }
            counter.value -= value;
            if (counter.value < 0) {
                counter.value = 0;
            }
            OnUpdate?.Invoke(this, counter.value);
        }

        protected override void Load(params object[] objs) {
            Load();
        }

        protected override void Save(params object[] objs) {
            Save();
        }

        public void Load() {
            if (!SaveUtility.LoadFromBinaryFormatter(saveFile, out counter.value)) {
                counter.value = defaultCounter;
            }
        }

        public void Save() {
            SaveUtility.SaveAsBinaryFormatter(counter.value, saveFile);
        }
    }
}


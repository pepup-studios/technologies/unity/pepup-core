﻿using System;

namespace PepUp.Managers
{
    [Serializable]
    public class SessionData
    {
        public UserSegementData userSegementData;
        public TimeSpan sessionDuration;
        public TimeSpan restTime;
        public DateTime startTime;
        public DateTime endTime;
    }
}
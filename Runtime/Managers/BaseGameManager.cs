using PepUp.Collections;
using PepUp.Map;
using UnityEngine;

namespace PepUp
{
    public class BaseGameManager : Singleton<BaseGameManager>
    {
        public MapLoader mapLoader;
        public SoundManager soundManager;

        protected override void Awake()
        {
            if (!InitSingleton(this)) return;

            Application.targetFrameRate = 60;

            mapLoader?.Init();
            soundManager?.Init();
        }
    }
}
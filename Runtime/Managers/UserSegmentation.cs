﻿using PepUp.Events;
using System;
using UnityEngine;

namespace PepUp.Managers
{
    public class UserSegmentation : MonoBehaviour
    {
        [SerializeField] protected EventObject OnUserIdChanged;
        [SerializeField] protected RegisterEvent DoRewardAdShown;
        [SerializeField] protected RegisterEvent DoRewardAdRewarded;
        [SerializeField] protected RegisterEvent DoReset;
        [SerializeField] protected RegisterEvent OnUserPaid;
        [SerializeField] protected int resetThreshold;
        [SerializeField]
        protected string[] userIds = {"DefaultUser",
    "PaidUser",
    "AdWhaleUser",
    "AdDolphinUser",
    "AdMinnowUser"};

        protected SessionManager sessionManager;
        protected int rewardAdShown;
        protected int rewardAdRewarded;
        protected string userId;

        protected void Awake()
        {
            sessionManager = SessionManager.instance;
            DoRewardAdShown.Register(this, "RewardAdShown");
            DoRewardAdRewarded.Register(this, "RewardAdRewarded");
            DoReset.Register(this, "Reset");
            OnUserPaid.Register(this, "UserPaid");
        }

        private void Start()
        {
            SessionData preSessionData = SessionManager.GetPreviousSessionData();
            if (preSessionData.userSegementData.userId == null)
            {
                userId = userIds[0];
            }
            else
            {
                userId = preSessionData.userSegementData.userId;
            }
            OnUserIdChanged?.Invoke(this, userId);
        }

        protected void OnDestroy()
        {
            DoRewardAdShown.Unregister();
            DoRewardAdRewarded.Unregister();
            DoReset.Unregister();
            OnUserPaid.Unregister();
        }

        protected void UserPaid(params object[] objs)
        {
            userId = userIds[1];
            enabled = false;
        }

        protected void Reset(params object[] objs)
        {
            Reset();
        }

        protected void RewardAdShown(params object[] objs)
        {
            RewardAdShown();
        }

        protected void RewardAdShown()
        {
            rewardAdShown++;
            CalculateUserId();
        }

        protected void RewardAdRewarded(params object[] objs)
        {
            RewardAdRewarded();
        }

        protected void RewardAdRewarded()
        {
            rewardAdRewarded++;
        }

        protected void CalculateUserId()
        {
            float clickPercent = (rewardAdRewarded / rewardAdShown) * 100;

            if (clickPercent >= 70 && clickPercent <= 100) { userId = userIds[2]; }
            else if (clickPercent >= 40 && clickPercent < 70) { userId = userIds[3]; }
            else if (clickPercent >= 0 && clickPercent < 40) { userId = userIds[4]; }
            else { userId = userIds[0]; }

            OnUserIdChanged?.Invoke(this, userId);
            FillUserSegmentData();

            if (rewardAdShown >= resetThreshold)
            {
                Reset();
            }
        }

        protected void FillUserSegmentData()
        {
            SessionData sessionData = SessionManager.GetCurrentSessionData();
            sessionData.userSegementData.userId = userId;
        }

        protected void Reset()
        {
            rewardAdRewarded = 0;
            rewardAdShown = 0;
        }

        protected TimeSpan GetDuration(DateTime endDate, DateTime startDate)
        {
            return endDate - startDate;
        }

    }
}
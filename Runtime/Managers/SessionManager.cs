﻿using PepUp.Collections;
using PepUp.Events;
using PepUp.Utility;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PepUp.Managers
{
    public class SessionManager : Singleton<SessionManager>
    {
        [SerializeField] protected string dataFileName = "All_Sessions_Data";
        [SerializeField] protected float sessionRestMinThreshold;
        [SerializeField] protected EventObject OnNewSessionCreated;
        [SerializeField] protected RegisterEvent DoLoadSessionData;
        [SerializeField] protected RegisterEvent DoSaveSessionData;
        [SerializeField] protected RegisterEvent DoStartRest;
        [SerializeField] protected RegisterEvent DoStopRest;

        [Space]
        protected static SessionsData sessionsData;
        protected static int currentSessionIndex;

        protected DateTime sessionStartDateTime;
        protected DateTime sessionEndDateTime;
        protected DateTime restTimeStart;
        protected DateTime restTimeStop;

        protected override void Awake()
        {
            if (!InitSingleton(this)) return;

            DoLoadSessionData.Register(this, "LoadSessionData");
            DoSaveSessionData.Register(this, "SaveSessionData");
            DoStartRest.Register(this, "StartRest");
            DoStopRest.Register(this, "StopRest");
        }

        private void OnDestroy()
        {
            DoLoadSessionData.Unregister();
            DoSaveSessionData.Unregister();
            DoStartRest.Unregister();
            DoStopRest.Unregister();
        }

        protected void StartRest(params object[] objs)
        {
            StartRest();
        }

        protected void StartRest()
        {
            restTimeStart = DateTime.Now;
        }

        protected void StopRest(params object[] objs)
        {
            StopRest();
        }

        protected void StopRest()
        {
            restTimeStop = DateTime.Now;
            SessionRestDuration();
        }

        protected void SessionRestDuration()
        {
            TimeSpan sessionRestDuration = GetDuration(restTimeStop, restTimeStart);
            if (sessionRestDuration.TotalMinutes < sessionRestMinThreshold) return;

            CreateNewSession();
        }

        protected void CreateNewSession()
        {
            SaveSessionData();
            LoadSessionData();

            OnNewSessionCreated?.Invoke(this);
        }

        protected void LoadSessionData(params object[] objs)
        {
            LoadSessionData();
        }

        protected void LoadSessionData()
        {
            if (!SaveUtility.LoadFromBinaryFormatter(dataFileName, out sessionsData))
            {
                sessionsData = new SessionsData();
                sessionsData.sessionData = new List<SessionData>();
            }
            sessionStartDateTime = DateTime.Now;

            sessionsData.sessionData.Add(new SessionData());
            currentSessionIndex = sessionsData.sessionData.Count - 1;
        }

        protected void SaveSessionData(params object[] objs)
        {
            SaveSessionData();
        }

        protected void SaveSessionData()
        {
            sessionEndDateTime = DateTime.Now;
            FillSessionData();

            SaveUtility.SaveAsBinaryFormatter(sessionsData, dataFileName);
        }

        protected void FillSessionData()
        {
            TimeSpan currentSessionDuration = GetDuration(sessionEndDateTime, sessionStartDateTime);
            sessionsData.sessionData[currentSessionIndex].sessionDuration = currentSessionDuration;
            sessionsData.sessionData[currentSessionIndex].startTime = sessionStartDateTime;
            sessionsData.sessionData[currentSessionIndex].endTime = sessionEndDateTime;
            sessionsData.sessionData[currentSessionIndex].restTime = GetRestTime();
        }

        protected TimeSpan GetRestTime()
        {
            if (sessionsData.sessionData.Count < 2)
            {
                return new TimeSpan(0, 0, 0);
            }
            else
            {
                return GetDuration(sessionStartDateTime, sessionsData.sessionData[currentSessionIndex - 1].endTime);
            }
        }

        public static SessionData GetCurrentSessionData()
        {
            return sessionsData.sessionData[currentSessionIndex];
        }

        public static SessionData GetPreviousSessionData()
        {
            return sessionsData.sessionData[currentSessionIndex - 1];
        }

        protected TimeSpan GetDuration(DateTime endDate, DateTime startDate)
        {
            return endDate - startDate;
        }
    }
}
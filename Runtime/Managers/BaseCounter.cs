﻿using PepUp.Events;
using UnityEngine;

namespace PepUp.Managers
{
    public abstract class BaseCounter : MonoBehaviour
    {
        public EventObject OnUpdate;
        public EventObject OnReset;

        [Space]
        public RegisterEvent DoLoad;
        public RegisterEvent DoSave;

        public RegisterEvent DoReset;
        public RegisterEvent DoActive;
        public RegisterEvent DoDeactive;

        public RegisterEvent DoIncrement;
        public RegisterEvent DoDecrement;

        protected virtual void Awake()
        {
            DoActive.Register(this, "Active");
            DoDeactive.Register(this, "Deactive");
        }

        protected virtual void OnDestroy()
        {
            DoActive.Unregister();
            DoDeactive.Unregister();
        }

        protected virtual void Active(params object[] objs)
        {
            enabled = true;
        }

        protected virtual void Deactive(params object[] objs)
        {
            enabled = false;
        }

        protected virtual void OnEnable()
        {
            DoLoad.Register(this, "Load");
            DoSave.Register(this, "Save");
            DoIncrement.Register(this, "Increment");
            DoDecrement.Register(this, "Decrement");
            DoReset.Register(this, "Reset");
            //Reset();//Remove from base include in IntCounter.
        }

        protected virtual void OnDisable()
        {
            DoLoad.Unregister();
            DoSave.Unregister();
            DoIncrement.Unregister();
            DoDecrement.Unregister();
            DoReset.Unregister();
        }

        protected abstract void Reset(params object[] objs);

        protected abstract void Increment(params object[] objs);

        protected abstract void Decrement(params object[] objs);

        protected abstract void Load(params object[] objs);

        protected abstract void Save(params object[] objs);
    }
}
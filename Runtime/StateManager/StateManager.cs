using UnityEngine;
using System.Collections.Generic;

namespace PepUp.State {
    public class StateManager : MonoBehaviour {

        public Utility.LogType logType;

        public bool resetOnDisable;
        public string nameSpace;

        public List<State> stateList;

        public Dictionary<string, IState> stateMap;
        public Dictionary<string, IState> currentStateMap;
        public Dictionary<string, IState> initStateMap;
        public Dictionary<string, Dictionary<string, IState>> stateTransitionMap;

        void Start() {
            stateMap = new Dictionary<string, IState>();
            currentStateMap = new Dictionary<string, IState>();
            initStateMap = new Dictionary<string, IState>();
            stateTransitionMap = new Dictionary<string, Dictionary<string, IState>>();
            foreach (State state in stateList) {
                if (!stateTransitionMap.ContainsKey(state.name)) {
                    stateTransitionMap.Add(state.name, new Dictionary<string, IState>());
                }
                List<StateTransition> transitionList = state.transitionList;
                IState stateReference = GetIStateComponent(state.name);
                stateReference.Init(); //lifecycle method
                if (state.initial) { //if marked as initial state
                    initStateMap.Add(stateReference.GetName(), stateReference);
                    if (!currentStateMap.ContainsKey(state.name)) {
                        currentStateMap.Add(state.name, stateReference);
                    }
                }
                foreach (StateTransition stateTransition in transitionList) {
                    IState stateTransitionReference = null;
                    if (stateTransition.state != "EXIT") {
                        stateTransitionReference = GetIStateComponent(stateTransition.state);
                    }
                    stateTransitionMap[state.name].Add(stateTransition.transition, stateTransitionReference);
                }
            }
            foreach (IState state in initStateMap.Values) {
                state.EnableState(true);
            }
        }

        protected void OnDisable() {
            if (!resetOnDisable || currentStateMap == null) return;
            ResetState();
        }

        public void ResetState() {
            var values = new List<IState>(currentStateMap.Values);
            int len = values.Count;
            for (int i = 0; i < len; i++)
            {
                DisableCurrentState(values[i]);
            }
            foreach (var initState in initStateMap.Values)
            {
                AddCurrentState(initState);
            }
        }

        public bool DoMultipleTransition(IState transition, params object[] objs) {
            return DoMultipleTransition(transition.GetName(), objs);
        }

        public bool DoMultipleTransition(string transition, params object[] objs) {
            bool flag = false;
            var keys = new List<string>(currentStateMap.Keys);
            foreach (string stateName in keys) {
                IState currentState = currentStateMap[stateName];
                bool result = DoTransition(currentState, transition, objs);
                if (result == true) flag = true;
            }
            return flag;
        }

        public bool DoTransition(IState currentState, IState targetState, params object[] objs) {
            return DoTransition(currentState, targetState.GetName(), objs);
        }

        public bool DoTransition(IState currentState, string transition, params object[] objs) {
            Dictionary<string, IState> currStateTransitionMap = null;
            string currentStateName = currentState.GetName();
            if (currentStateMap.ContainsKey(currentStateName) &&
                stateTransitionMap.ContainsKey(currentStateName)) {
                currStateTransitionMap = stateTransitionMap[currentStateName];
                if (currStateTransitionMap.ContainsKey(transition)) {
                    IState targetState = currStateTransitionMap[transition];
                    if (targetState != null && targetState.IsTransitionAllowed(currentState)) {
                        CycleMethods(currentState, targetState, objs);
                    } else { //EXIT SCENARIO - If a state wants to close(exit) another unrelated State
                        DisableCurrentState(currentState);
                    }
                    return true;
                }
                #if UNITY_EDITOR
                else {
                    DebugLog(currentState.GetName(), transition);
                }
                #endif
            }
            return false;
        }

        protected void CycleMethods(IState currentState, IState targetState, params object[] objs) {
            targetState.PreTransition(currentState, objs); //lifecycle method
            currentState.PreEntryNewTransition(targetState); //lifecycle method
            AddCurrentState(targetState);
            currentState.PostEntryNewTransition(targetState); //lifecycle method
            targetState.PostTransition(currentState, objs); //lifecycle method
        }

        public void AddCurrentState(IState state) {
            if (!currentStateMap.ContainsKey(state.GetName())) {
                currentStateMap.Add(state.GetName(), state);
            }
            state.EnableState(true);
        }

        public void DisableCurrentState(IState state) {
            state.EnableState(false);
            currentStateMap.Remove(state.GetName());
        }

        protected void DebugLog(string currentState, string targetState) {
            if (logType == Utility.LogType.debug) {
                Debug.Log(currentState + " - " + targetState + " - NOT ALLOWED");
            }
        }

        public T GetIStateComponent<T>()
        {
            return (T)GetIStateComponent(typeof(T).Name);
        }

        public IState GetIStateComponent(string stateName) {
            if (stateMap != null && stateMap.TryGetValue(stateName, out IState state))
            {
                return state;
            }
            if (string.IsNullOrEmpty(nameSpace))
            {
                state = gameObject.GetComponent(stateName) as IState;
            }
            else
            {
                state = gameObject.GetComponent(nameSpace + "." + stateName) as IState;
            }
            stateMap?.Add(stateName, state);
            return state;
        }

        public IState GetActiveIState(string stateName)
        {
            return currentStateMap != null && currentStateMap.TryGetValue(stateName, out IState state) ? state : null;
        }

        public bool IsStateActive(string stateName)
        {
            return GetActiveIState(stateName) != null;
        }
    }
}
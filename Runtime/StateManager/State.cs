﻿using System.Collections.Generic;

namespace PepUp.State {
    [System.Serializable]
    public class State {

        public string name;
        public bool initial;
        public List<StateTransition> transitionList;
    }
}
﻿using UnityEngine;
using PepUp.Events;

namespace PepUp.State
{
    public abstract class BaseState : MonoBehaviour, IState
    {

        [Header("State Event")]
        public EventObject OnStateEnter;
        public EventObject OnStateExit;

        public event EventAction OnStateEnterEvent;
        public event EventAction OnStateExitEvent;

        protected new string name;

        public virtual void EnableState(bool flag)
        {
            enabled = flag;
        }

        public virtual string GetName()
        {
            return name;
        }

        protected virtual void OnEnable()
        {
            InvokeStateEnter();
        }

        protected virtual void OnDisable()
        {
            InvokeStateExit();
        }

        public abstract void Init();

        public bool IsTransitionAllowed(IState previousState)
        {
            return true;
        }

        public virtual void PostEntryNewTransition(IState newState) { }

        public virtual void PostTransition(IState previousState, params object[] objs) { }

        public virtual void PreEntryNewTransition(IState newState) { }

        public virtual void PreTransition(IState previousState, params object[] objs) { }

        protected virtual void InvokeStateEnter(params object[] objs)
        {
            OnStateEnter?.Invoke(objs);
            OnStateEnterEvent?.Invoke(objs);
        }

        protected virtual void InvokeStateExit(params object[] objs)
        {
            OnStateExit?.Invoke(objs);
            OnStateExitEvent?.Invoke(objs);
        }
    }
}
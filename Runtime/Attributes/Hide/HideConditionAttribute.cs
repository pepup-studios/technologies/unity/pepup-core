﻿using UnityEngine;

public class HideConditionAttribute : PropertyAttribute {

    public string conditionName;
    public string conditionValue;
    public int propertyHeight = 0;

    public HideConditionAttribute(string conditionName, string conditionValue) {
        this.conditionName = conditionName;
        this.conditionValue = conditionValue;
    }

    public HideConditionAttribute(string conditionName, string conditionValue, int propertyHeight) {
        this.conditionName = conditionName;
        this.conditionValue = conditionValue;
        this.propertyHeight = propertyHeight;
    }
}

﻿namespace PepUp {
    [System.Serializable]
    public class StringsOption {

        public bool isValue;
        public string[] stringValues;
        public StringsObject stringsObject;

        public string[] values {
            get { return isValue ? stringValues : stringsObject.values; }
            set { if (isValue) stringValues = value; else stringsObject.values = value; }
        }

        public void SetValues(StringsOption stringsOption) {
            if (stringsOption.isValue) stringValues = stringsOption.stringValues;
            else stringsObject.values = stringsOption.stringsObject.values;
        }
    }
}

﻿using UnityEngine;

namespace PepUp {
    [CreateAssetMenu(menuName = "PepUp/Attribute/Strings Object")]
    public class StringsObject : ScriptableObject {

        public string[] values;
    }
}

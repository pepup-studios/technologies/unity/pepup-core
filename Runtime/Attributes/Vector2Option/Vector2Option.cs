﻿using UnityEngine;

namespace PepUp {
    [System.Serializable]
    public class Vector2Option {

        public bool isValue;
        public Vector2 vector2Value;
        public Vector2Object vector2Object;

        public Vector2 value {
            get { return isValue ? vector2Value : vector2Object.value; }
            set { if (isValue) vector2Value = value; else vector2Object.value = value; }
        }

        public void SetValue(Vector2Option vector2Option) {
            if (vector2Option.isValue) vector2Value = vector2Option.vector2Value;
            else vector2Object.value = vector2Option.vector2Object.value;
        }
    }
}

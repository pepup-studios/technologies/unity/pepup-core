﻿using UnityEngine;

namespace PepUp {
    [CreateAssetMenu(menuName = "PepUp/Attribute/Vector2 Object")]
    public class Vector2Object : ScriptableObject {

        public Vector2 value;
    }
}

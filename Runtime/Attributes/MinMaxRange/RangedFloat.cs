namespace PepUp {
    [System.Serializable]
    public class RangedFloat {

        public float minValue;
        public float maxValue;

        public RangedFloat(float min, float max) {
            minValue = min;
            maxValue = max;
        }

        public RangedFloat(RangedFloat rangeObj) {
            minValue = rangeObj.minValue;
            maxValue = rangeObj.maxValue;
        }
    }
}
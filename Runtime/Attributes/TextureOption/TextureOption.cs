﻿using UnityEngine;

namespace PepUp {
    [System.Serializable]
    public class TextureOption {

        public bool isValue;
        public Texture textureValue;
        public TextureObject textureObject;

        public Texture value {
            get { return isValue ? textureValue : (textureObject != null) ? textureObject.value : null; }
            set { if (isValue) textureValue = value; else textureObject.value = value; }
        }

        public void SetValues(TextureOption textureOption) {
            if (textureOption.isValue) textureValue = textureOption.textureValue;
            else textureObject.value = textureOption.textureObject.value;
        }
    }
}

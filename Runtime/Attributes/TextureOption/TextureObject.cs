﻿using UnityEngine;

namespace PepUp {
    [CreateAssetMenu(menuName = "PepUp/Attribute/Texture Object")]
    public class TextureObject : ScriptableObject {

        public Texture value;
    }
}

﻿namespace PepUp {
    [System.Serializable]
    public class FloatOption {

        public bool isValue;
        public float floatValue;
        public FloatObject floatObject;

        public float value {
            get { return isValue ? floatValue : floatObject.value; }
            set { if (isValue) floatValue = value; else floatObject.value = value; }
        }

        public void SetValues(FloatOption floatOption) {
            if (floatOption.isValue) floatValue = floatOption.floatValue;
            else floatObject.value = floatOption.floatObject.value;
        }
    }
}

﻿using UnityEngine;

namespace PepUp {
    [CreateAssetMenu(menuName = "PepUp/Attribute/Float Object")]
    public class FloatObject : ScriptableObject {

        public float value;
        public float SetValue{
            set {
                this.value = value;
            }
        }
    }
}

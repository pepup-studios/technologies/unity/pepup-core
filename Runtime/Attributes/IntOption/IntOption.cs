﻿namespace PepUp {
    [System. Serializable]
    public class IntOption {

        public bool isValue;
        public int intValue;
        public IntObject intObject;

        public int value {
            get { return isValue ? intValue : intObject.value; }
            set { if (isValue) intValue = value; else intObject.value = value; }
        }

        public void SetValues(IntOption intOption) {
            if (intOption.isValue) intValue = intOption.intValue;
            else intObject.value = intOption.intObject.value;
        }
    }
}

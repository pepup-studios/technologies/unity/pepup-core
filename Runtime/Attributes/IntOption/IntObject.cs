﻿using UnityEngine;

namespace PepUp {
    [CreateAssetMenu(menuName = "PepUp/Attribute/Int Object")]
    public class IntObject : ScriptableObject {

        public int value;
    }
}

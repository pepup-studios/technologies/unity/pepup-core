﻿namespace PepUp {
    [System.Serializable]
    public class RangedFloatOption {

        public bool isValue;
        [MinMaxRange(0, 1000)]
        public RangedFloat rangedValue;
        public RangedFloatObject rangedObject;

        public RangedFloat value {
            get { return isValue ? rangedValue : rangedObject.value; }
            set { if (isValue) rangedValue = value; else rangedObject.value = value; }
        }

        public void SetValues(RangedFloatOption rangedOption) {
            isValue = rangedOption.isValue;
            rangedValue = new RangedFloat(rangedOption.rangedValue);
            rangedObject = rangedOption.rangedObject;
        }
    }
}
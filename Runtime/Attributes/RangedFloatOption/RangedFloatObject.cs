﻿using UnityEngine;

namespace PepUp {
    [CreateAssetMenu(menuName = "PepUp/Attribute/Ranged Float Object")]
    public class RangedFloatObject : ScriptableObject {

        [MinMaxRange(0, 1000)]
        public RangedFloat value;
    }
}
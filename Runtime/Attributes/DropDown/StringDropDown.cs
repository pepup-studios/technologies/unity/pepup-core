﻿using UnityEngine;

namespace PepUp {
    [CreateAssetMenu(menuName = "PepUp/Attribute/Drop Down/String Drop Down")]
    public class StringDropDown : BaseDropDown {

        public StringsOption stringOption;

        public override string[] GetStringValues() {
            return stringOption.values;
        }
    }
}
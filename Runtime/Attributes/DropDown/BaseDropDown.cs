﻿using UnityEngine;

namespace PepUp {
    public abstract class BaseDropDown : ScriptableObject {

        public abstract string[] GetStringValues();
    }
}
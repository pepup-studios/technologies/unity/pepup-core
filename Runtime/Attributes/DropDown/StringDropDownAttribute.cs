﻿using PepUp.Editor;

namespace PepUp {
    public class StringDropDownAttribute : BaseDropDownAttribute {

        public StringDropDownAttribute(string pathName, bool displaySeperator = true, bool resultSeperator = false) :
            base(pathName, displaySeperator, resultSeperator) {
        }
    }
}
﻿using UnityEngine;

namespace PepUp.Editor {
    public abstract class BaseDropDownAttribute : PropertyAttribute {

        public string pathName;
        public bool displaySeperator;
        public bool resultSeperator;

        public BaseDropDownAttribute(string pathName, bool displaySeperator = true, bool resultSeperator = false) {
            this.pathName = "Drop Downs/" + pathName;
            this.displaySeperator = displaySeperator;
            this.resultSeperator = resultSeperator;
        }
    }
}
﻿using UnityEngine;
using System.Linq;

namespace PepUp.Editor {
    [CreateAssetMenu(menuName = "PepUp/Attribute/Drop Down/Int Drop Down")]
    public class IntDropDown : BaseDropDown {

        public int[] values;

        public override string[] GetStringValues() {
            return values.Select(x => x.ToString()).ToArray();
        }
    }
}
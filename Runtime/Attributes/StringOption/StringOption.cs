﻿namespace PepUp {
    [System.Serializable]
    public class StringOption {

        public bool isValue;
        public string stringValue;
        public StringObject stringObject;

        public string value {
            get { return isValue ? stringValue : (stringObject != null) ? stringObject.value : null; }
            set { if (isValue) stringValue = value; else stringObject.value = value; }
        }

        public void SetValues(StringOption stringOption) {
            if (stringOption.isValue) stringValue = stringOption.stringValue;
            else stringObject.value = stringOption.stringObject.value;
        }
    }
}

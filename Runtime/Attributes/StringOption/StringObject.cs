﻿using UnityEngine;

namespace PepUp {
    [CreateAssetMenu(menuName = "PepUp/Attribute/String Object")]
    public class StringObject : ScriptableObject {

        public string value;

        public string Value { get { return value; } set { this.value = value; } }
    }
}

﻿using UnityEngine;

namespace PepUp {
    public abstract class Wallet : MonoBehaviour {

        public abstract void Init();

        public abstract T GetCurrency<T>(string key);
    }
}
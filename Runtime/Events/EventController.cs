﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace PepUp.Events {
    public static class EventController {

        public static BindingFlags deafaultBindingFlags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static;
        public static Type[] defaultHandlerType = { typeof(object[]) };

        public static bool EventLinker(EventData eventData) {
            return EventLinker(eventData.listener, eventData.speaker, eventData.eventName, eventData.eventHandler);
        }

        public static bool EventLinker<T, Y>(T listener, Y speaker, string eventName, string eventHandler) {
            EventInfo eInfo = speaker.GetType().GetEvent(eventName);
            Type eventType = eInfo.EventHandlerType;

            //Temporary change move code to Editor
            //ParameterInfo[] parameters = handlerType.GetMethod("Invoke").GetParameters();
            //Type[] handlerTypes = parameters.Select(type => type.ParameterType).ToList().ToArray();

            MethodInfo methodInfo = listener.GetType().GetMethod(eventHandler, deafaultBindingFlags, Type.DefaultBinder, defaultHandlerType, null);

            if (eInfo != null && eventType != null && methodInfo != null) {
                Delegate d = Delegate.CreateDelegate(eventType, listener, eventHandler, false, true);
                MethodInfo eventMethod = eInfo.GetAddMethod();
                eventMethod.Invoke(speaker, new[] { d });
                return true;
            }
#if UNITY_EDITOR
            UnityEngine.Debug.LogError("Cannot bind event: " + speaker + " -> " + eventName + " on event handler: " + listener + " -> " + eventHandler);
#endif
            return false;
        }

        public static bool EventUnlinker(EventData eventData) {
            return EventUnlinker(eventData.listener, eventData.speaker, eventData.eventName, eventData.eventHandler);
        }

        public static bool EventUnlinker<T, Y>(T listener, Y speaker, string eventName, string eventHandler) {
            EventInfo eInfo = speaker.GetType().GetEvent(eventName);
            Type eventType = eInfo.EventHandlerType;

            //Temporary change move code to Editor
            //ParameterInfo[] parameters = handlerType.GetMethod("Invoke").GetParameters();
            //Type[] handlerTypes = parameters.Select(type => type.ParameterType).ToList().ToArray();

            MethodInfo methodInfo = listener.GetType().GetMethod(eventHandler, deafaultBindingFlags, Type.DefaultBinder, defaultHandlerType, null);

            if (eInfo != null && eventType != null && methodInfo != null) {
                Delegate d = Delegate.CreateDelegate(eventType, listener, eventHandler, false, true);
                MethodInfo eventMethod = eInfo.GetRemoveMethod();
                eventMethod.Invoke(speaker, new[] { d });
                return true;
            }
//#if UNITY_EDITOR
//            UnityEngine.Debug.LogError("Cannot unbind event: " + speaker + " -> " + eventName + " on event handler: " + listener + " -> " + eventHandler);
//#endif
            return false;
        }
    }

	public delegate void EventAction(params object[] objs);

	[Serializable]
    public class EventMap {

        public List<EventData> list = new List<EventData>();

        public virtual void Register() {
            int len = list.Count;
            for (int i = 0; i < len; i++) {
                var eventData = list[i];

                if (eventData.dataType == DataType.RuntimeData) {
                    EventController.EventLinker(eventData);
                } else {
                    EventController.EventLinker(eventData, eventData.speaker, eventData.eventName, "CallWithData");
                }
            }
        }

        public virtual void Unregister() {
            int len = list.Count;
            for (int i = 0; i < len; i++) {
                var eventData = list[i];

                if (eventData.dataType == DataType.RuntimeData) {
                    EventController.EventUnlinker(eventData);
                } else {
                    EventController.EventUnlinker(eventData, eventData.speaker, eventData.eventName, "CallWithData");
                }
            }
        }
    }

    [Serializable]
    public class RegisterEvent {

        public List<EventData> list = new List<EventData>();

        private readonly Dictionary<string, EventData> runtimeEventData = new Dictionary<string, EventData>();

        public virtual void Register(UnityEngine.Object listener, string eventHandler) {
            int len = list.Count;
            for (int i = 0; i < len; i++) {
                var eventData = list[i];
                eventData.listenerParent = eventData.listener = listener;
                eventData.eventHandler = eventHandler;

                if (eventData.dataType == DataType.RuntimeData) {
                    EventController.EventLinker(eventData);
                } else {
                    EventController.EventLinker(eventData, eventData.speaker, eventData.eventName, "CallWithData");
                }
            }
        }

        public virtual void Unregister() {
            int len = list.Count;
            for (int i = 0; i < len; i++) {
                var eventData = list[i];

                if (eventData.dataType == DataType.RuntimeData) {
                    EventController.EventUnlinker(eventData);
                } else {
                    EventController.EventUnlinker(eventData, eventData.speaker, eventData.eventName, "CallWithData");
                }
            }
            foreach (var eventData in runtimeEventData.Values) {
                EventController.EventUnlinker(eventData);
            }
        }

        public void AddSpeaker(UnityEngine.Object speaker, string eventName, UnityEngine.Object listener, string eventHandler) {
            EventData newEventData = new EventData {
                speakerParent = speaker,
                speaker = speaker,
                eventName = eventName,
                listenerParent = listener,
                listener = listener,
                eventHandler = eventHandler
            };
            runtimeEventData.Add(GenerateKey(listener, eventHandler), newEventData);
            EventController.EventLinker(newEventData);
        }

        public bool RemoveSpeaker(UnityEngine.Object speaker, string eventName) {
            string key = GenerateKey(speaker, eventName);
            if (runtimeEventData.TryGetValue(key, out EventData eventData)) {
                EventController.EventUnlinker(eventData);
                return runtimeEventData.Remove(key);
            }
            return false;
        }

        protected string GenerateKey(UnityEngine.Object speaker, string eventName) {
            return speaker.GetInstanceID() + " " + eventName;
        }
    }

    //[Serializable]
    //public class PepUpEvent {//Temporary commented; need to code functionalty.

    //    public List<EventData> list = new List<EventData>();

    //    public EventAction OnEvent;

    //    private readonly Dictionary<string, EventData> runtimeEventData = new Dictionary<string, EventData>();

    //    public virtual void OnEnable(UnityEngine.Object speaker) {
    //        int len = list.Count;
    //        for (int i = 0; i < len; i++) {
    //            var eventData = list[i];
    //            eventData.speakerParent = speaker;
    //            eventData.speaker = speaker;
    //            eventData.eventName = "OnEvent";

    //            EventController.EventLinker(eventData);
    //        }
    //    }

    //    public virtual void OnDisable() {
    //        int len = list.Count;
    //        for (int i = 0; i < len; i++) {
    //            var eventData = list[i];
    //            EventController.EventUnlinker(eventData);
    //        }
    //        foreach (var eventData in runtimeEventData.Values) {
    //            EventController.EventUnlinker(eventData);
    //        }
    //    }

    //    public void Invoke(params object[] objs) {
    //        OnEvent.Invoke(objs);
    //    }

    //    public void AddListener(UnityEngine.Object speaker, UnityEngine.Object listener, string eventHandler) {
    //        EventData newEventData = new EventData {
    //            speakerParent = speaker, speaker = speaker, eventName = "OnEvent",
    //            listenerParent = listener, listener = listener, eventHandler = eventHandler
    //        };
    //        runtimeEventData.Add(GenerateKey(listener, eventHandler), newEventData);
    //        EventController.EventLinker(newEventData);
    //    }

    //    public bool RemoveListener(UnityEngine.Object listener, string eventHandler) {
    //        string key = GenerateKey(listener, eventHandler);
    //        if (runtimeEventData.TryGetValue(key, out EventData eventData)) {
    //            EventController.EventUnlinker(eventData);
    //            return runtimeEventData.Remove(key);
    //        }
    //        return false;
    //    }

    //    protected string GenerateKey(UnityEngine.Object listener, string eventHandler) {
    //        return listener.GetInstanceID() + " " + eventHandler;
    //    }
    //}

    [Serializable]
    public class EventData {
        public UnityEngine.Object speakerParent;
        public UnityEngine.Object speaker;
        public string eventName;
        public int selectionIndex;

        public UnityEngine.Object listenerParent;
        public UnityEngine.Object listener;
        public string eventHandler;

        public DataType dataType = DataType.RuntimeData;
        public string stringValue;
        public bool boolValue;
        public int intValue;
        public float floatValue;
        public UnityEngine.Object objValue;

        public void CallWithData(params object[] objs) {
            MethodInfo methodInfo = listener.GetType().GetMethod(eventHandler, EventController.deafaultBindingFlags, Type.DefaultBinder, EventController.defaultHandlerType, null);
            object[] types = { new object[0] };
            switch (dataType) {
                case DataType.String:
                    types = new object[] { new object[] { stringValue } };
                    break;
                case DataType.Int:
                    types = new object[] { new object[] { intValue } };
                    break;
                case DataType.Float:
                    types = new object[] { new object[] { floatValue } };
                    break;
                case DataType.Bool:
                    types = new object[] { new object[] { boolValue } };
                    break;
                case DataType.Object:
                    types = new object[] { new object[] { objValue } };
                    break;
            }
            methodInfo.Invoke(listener, types);
        }

#if UNITY_EDITOR
        public bool isFold = true;
#endif
    }

    public enum DataType { RuntimeData, None, String, Bool, Int, Float, Object };
}

﻿namespace PepUp.Events {
    [System.Serializable]
	public class EventActionObject {

        public EventObject eventObject;

        public event EventAction OnResponce;

        public void Invoke(params object[] objs) {
            eventObject?.Invoke(objs);
            OnResponce?.Invoke(objs);
        }
	}
}
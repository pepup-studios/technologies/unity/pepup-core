﻿using UnityEngine;

namespace PepUp.Events {
    [CreateAssetMenu(menuName = "PepUp/Events/Event Object")]
    public class EventObject : ScriptableObject {

        public event EventAction OnResponse;

        public void Invoke(params object[] objs) {
            OnResponse?.Invoke(objs);
        }

        public void Invoke() {
            OnResponse?.Invoke();
        }

        public void Invoke(string value) {
            OnResponse?.Invoke(value);
        }

        public void BoolInvoke(bool value) {
            OnResponse?.Invoke(value);
        }

        public void Invoke(int value) {
            OnResponse?.Invoke(value);
        }

        public void Invoke(float value) {
            OnResponse?.Invoke(value);
        }

        public void Invoke(Object value) {
            OnResponse?.Invoke(value);
        }
    }
}
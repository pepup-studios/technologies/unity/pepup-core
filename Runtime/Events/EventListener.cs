﻿using UnityEngine;

namespace PepUp.Events {
    [AddComponentMenu("PepUp/Events/Event Listener")]
    public class EventListener : MonoBehaviour {

        public EventMap events;

        protected void OnEnable() {
            events.Register();
        }

        protected void OnDisable() {
            events.Unregister();
        }
    }
}

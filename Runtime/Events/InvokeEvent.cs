﻿using System.Collections;
using UnityEngine;

namespace PepUp.Events
{
    public class InvokeEvent : MonoBehaviour
    {
        public KeyCode keyCode = KeyCode.I;
        public EventObject EventToInvoke;
        public bool enableOnStart = true;
        public float delayStart;

        private void Start()
        {
            if (enableOnStart)
            {
                if (delayStart > 0)
                {
                    StartCoroutine(Wait());
                }
                else
                {
                    EventToInvoke?.Invoke();
                }
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(keyCode))
            {
                EventToInvoke?.Invoke();
            }
        }

        IEnumerator Wait()
        {
            yield return new WaitForSeconds(delayStart);
            EventToInvoke?.Invoke();
        }
    }
}
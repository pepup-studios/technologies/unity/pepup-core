﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PepUp.SceneManagement {
    [CreateAssetMenu(menuName = "PepUp/Scene Management/Scene Object")]
    public class SceneObject : ScriptableObject {
        public SceneData[] data;
    }

    [System.Serializable]
    public class SceneData {
        public string name;
        public LoadSceneMode mode;
        public SceneType sceneType;
        public bool doTransitionAnim;

        [HideInInspector]
        public bool isAsync;

        public bool isLoaded { get; private set; }
        public bool isRestarting { get; private set; }
        public SceneData parent { get; private set; }

        protected Dictionary<string, SceneData> children;

        public void Init() {
            if (children == null) {
                children = new Dictionary<string, SceneData>();
            } else {
                children.Clear();
            }
            isLoaded = false;
            isRestarting = false;
            parent = null;
        }

        public SceneData(string name, LoadSceneMode mode, bool isAsync = false, SceneData parent = null, SceneType sceneType = SceneType.Default) {
            Init();
            this.name = name;
            this.mode = mode;
            this.isAsync = isAsync;
            this.sceneType = sceneType;
            SetParent(parent);
        }

        public void SetParent(SceneData parent) {
            if (this.parent != null) this.parent.RemoveChild(name);
            if (parent != null) parent.AddChild(this);
            this.parent = parent;
        }

        public virtual void SceneLoaded() {
            isLoaded = true;
            isRestarting = false;

            //if (parent != null) Debug.Log("Loaded: " + name + " parent: " + parent.name);
            //else Debug.Log("Loaded: " + name + " parent: null");

            LoadChildren();
        }

        public virtual void SceneUnloaded() {
            isLoaded = false;

            //if (parent != null) Debug.Log("Unloaded: " + name + " parent: " + parent.name);
            //else Debug.Log("Unloaded: " + name + " parent: null");

            UnloadChildren();
        }

        public void RegisterLoad() {
            isRestarting = isLoaded;
        }

        public bool Load(int limit = -1) {
            if (parent == null || (parent.isLoaded && !parent.isRestarting)) {
                //if (parent != null) Debug.Log("Loading: " + name + " parent: " + parent.name);
                //else Debug.Log("Loading: " + name + " parent: null");
                LoadScene(name, mode, limit);
                return true;
            }
            //Debug.Log("Will Load When Parent is Loaded: " + name + " parent: " + parent.name + " isRestarting: " + parent.isRestarting);
            isRestarting = parent.isRestarting;
            SetParent(parent);
            return false;
        }

        public bool Unload() {
            if (isRestarting || (parent != null && parent.isRestarting)) {
                //Debug.Log("Restarting: " + name + " isRestarting: " + isRestarting);
                return false;
            }
            SetParent(null);

            switch (sceneType) {
                case SceneType.Swap:
                    SceneSwap.UnloadSceneAsync();
                    //Debug.Log("Unloading: " + name);
                    return true;
                case SceneType.Queue:
                    SceneQueue.UnloadSceneAsync();
                    //Debug.Log("Unloading: " + name);
                    return false;
            }
            if (isAsync) {
                SceneManager.UnloadSceneAsync(name);
                //Debug.Log("Unloading: " + name);
                return true;
            }
            isLoaded = false;
            return false;
        }

        public void LoadChildren() {
            var localChildren = new List<SceneData>(children.Values);
            foreach (var child in localChildren) {
                child.Load();
            }
        }

        public void UnloadChildren() {
            var localChildren = new List<SceneData>(children.Values);
            foreach (var child in localChildren) {
                child.Unload();
            }
        }

        protected void LoadScene(string name, LoadSceneMode mode, int limit = -1) {
            switch (sceneType) {
                case SceneType.Swap:
                    SceneSwap.LoadSceneAsync(this, mode);
                    return;
                case SceneType.Queue:
                    SceneQueue.LoadSceneAsync(this, mode, limit);
                    return;
            }
            if (isAsync) {
                SceneManager.LoadSceneAsync(name, mode);
            } else {
                SceneManager.LoadScene(name, mode);
            }
        }

        public SceneData[] GetChildren() {
            return new List<SceneData>(children.Values).ToArray();
        }

        public SceneData GetChild(string name) {
            children.TryGetValue(name, out SceneData sceneData);
            return sceneData;
        }

        public void AddChild(SceneData child) {
            if (children.ContainsKey(child.name)) {
                children[child.name] = child;
                return;
            }
            children.Add(child.name, child);
        }

        public bool RemoveChild(string name) {
            return children.Remove(name);
        }
    }

    public enum SceneType { Default, Queue, Swap }
}
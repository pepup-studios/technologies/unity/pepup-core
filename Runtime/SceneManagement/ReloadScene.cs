﻿using PepUp.Events;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReloadScene : MonoBehaviour {

    public RegisterEvent DoReloadScene;

    protected void Start() {
        DoReloadScene.Register(this, "ReloadActiveScene");
    }

    protected void nDestroy() {
        DoReloadScene.Unregister();
    }

    protected void ReloadActiveScene(params object[] objs) {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}

﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using PepUp.Events;

namespace PepUp.SceneManagement {
    [RequireComponent(typeof(Animator))]
	[AddComponentMenu("PepUp/Scene Management/Scene Controller")]
    public class SceneController : MonoBehaviour {

        public SceneObject scenes;

        [Space]
        public EventObject OnAnimationComplete;

        [Space]
        public RegisterEvent OnLoadScene;
        public RegisterEvent OnLoadSceneAsync;
        public RegisterEvent OnUnloadSceneAsync;

        protected Dictionary<string, SceneData> sceneMap = new Dictionary<string, SceneData>();

        protected List<SceneData> transitionScenes = new List<SceneData>();

        [SerializeField] [HideInInspector]
        private Animator _animator;

        protected virtual void Awake() {
            foreach (var scene in scenes.data) {
                scene.Init();
                sceneMap.Add(scene.name, scene);
            }
            SceneManager.sceneLoaded += SceneLoaded;
            SceneManager.sceneUnloaded += SceneUnloaded;
            OnLoadScene.Register(this, "LoadScene");
            OnLoadSceneAsync.Register(this, "LoadSceneAsync");
            OnUnloadSceneAsync.Register(this, "UnloadSceneAsync");
        }

        protected virtual void OnDisable() {
            SceneManager.sceneLoaded -= SceneLoaded;
            SceneManager.sceneUnloaded -= SceneUnloaded;

            OnLoadScene.Unregister();
            OnUnloadSceneAsync.Unregister();
            OnUnloadSceneAsync.Unregister();
        }

        public virtual void LoadSceneByAnimator() {
            foreach (var transitionScene in transitionScenes) {
                transitionScene.Load();
            }
        }

        public virtual void DoAnimationComplete() {
            OnAnimationComplete?.Invoke(this);
        }

        protected virtual void LoadScene(params object[] objs) {
            string sceneName = (string) objs[0];
            LoadSceneMode mode = (LoadSceneMode) objs[1];
            string parent = (string) objs[2];
            SceneType sceneType = (SceneType) objs[3];
            int sceneLimit;
            try {
                sceneLimit = (int) objs[4];
            }
            catch {
                sceneLimit = -1;
            }

            LoadScene(sceneName, mode, parent, sceneType, sceneLimit);
        }

        public virtual void LoadScene(string name, LoadSceneMode mode, string parent = null, SceneType sceneType = SceneType.Default, int sceneLimit = -1) {
            SceneData sceneData = CreateScene(name, mode, false, parent, sceneType);
            LoadScene(sceneData, sceneLimit);
        }

        protected virtual void LoadSceneAsync(params object[] objs) {
            string sceneName = (string) objs[0];
            LoadSceneMode mode = (LoadSceneMode) objs[1];
            string parent = (string) objs[2];
            SceneType sceneType = (SceneType) objs[3];
            int sceneLimit;
            try {
                sceneLimit = (int) objs[4];
            }
            catch {
                sceneLimit = -1;
            }

            LoadSceneAsync(sceneName, mode, parent, sceneType, sceneLimit);
        }

        public virtual void LoadSceneAsync(string name, LoadSceneMode mode, string parent = null, SceneType sceneType = SceneType.Default, int sceneLimit = -1) {
            SceneData sceneData = CreateScene(name, mode, true, parent, sceneType);
            LoadScene(sceneData, sceneLimit);
        }

        public virtual void LoadScene(SceneData sceneData, int sceneLimit = -1) {
            if (sceneData.doTransitionAnim) {
                sceneData.RegisterLoad();
                StartTransition(sceneData);
                return;
            }
            sceneData.Load(sceneLimit);
        }

        protected virtual void UnloadSceneAsync(params object[] objs) {
            string sceneName = (string) objs[0];
            UnloadSceneAsync(sceneName);
        }

        public virtual bool UnloadSceneAsync(string name) {
            SceneData sceneData = GetScene(name);
            if (sceneData != null) {
                sceneData.Unload();
                return true;
            }
            return false;
        }

        public virtual void SceneLoaded(Scene scene, LoadSceneMode mode) {
            SceneData sceneData = GetScene(scene);
            if (sceneData != null) {
                sceneData.SceneLoaded();
                if (sceneData.doTransitionAnim) StopTransition(sceneData);
            }
        }

        public virtual void SceneUnloaded(Scene scene) {
            SceneData sceneData = GetScene(scene);
            if (sceneData != null) {
                sceneData.SceneUnloaded();
            }
        }

        public SceneData CreateScene(string name, LoadSceneMode mode, bool isAsync, string parent = null, SceneType sceneType = SceneType.Default) {
            //string sceneName = GetSceneName(name);
            SceneData sceneData = GetScene(name);
            SceneData parentScene = null; 
            if (parent != null) parentScene = GetScene(parent);

            if (sceneData == null) {
                sceneData = new SceneData(name, mode, isAsync, parentScene, sceneType);
                sceneMap.Add(name, sceneData);
            } else {
                sceneData.mode = mode;
                sceneData.isAsync = isAsync;
                sceneData.SetParent(parentScene);
                sceneData.sceneType = sceneType;
            }
            return sceneData;
        }

        public SceneData GetScene(Scene scene) {
            SceneData value = GetScene(scene.name);
            if (value != null) return value;
            return GetScene(GetScenePath(scene.path));
        }

        public SceneData GetScene(string name) {
            sceneMap.TryGetValue(name, out SceneData value);
            return value;
        }

        public string GetSceneName(string name) {
            if (name == null) return null;
            string[] names = name.Split('/');
            if (names.Length > 1) {
                return names[names.Length - 1];
            }
            return names[0];
        }

        public string GetScenePath(string path) {
            if (path == null) return null;
            string[] newPath = path.Remove(0, 7).Split('.');
            return newPath[0];
        }

        protected void StartTransition(SceneData sceneData) {
            transitionScenes.Add(sceneData);
            _animator.SetBool("Open", true);
        }

        protected void StopTransition(SceneData sceneData) {
            bool didRemove = transitionScenes.Remove(sceneData);
            if (didRemove) _animator.SetBool("Open", false);
        }

#if UNITY_EDITOR
        protected void OnValidate() {
            _animator = GetComponent<Animator>();
        }
#endif
    }
}
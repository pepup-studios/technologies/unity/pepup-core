﻿using UnityEngine.SceneManagement;

namespace PepUp.SceneManagement {
    public static class SceneSwap {

        private static SceneData _sceneData;

        public static void LoadSceneAsync(SceneData sceneData, LoadSceneMode mode) {
            if (_sceneData != null && _sceneData.name != sceneData.name) {
                UnloadSceneAsync();
            }
            if (_sceneData != null && _sceneData.name == sceneData.name) return;

            SceneManager.LoadSceneAsync(sceneData.name, mode);
            _sceneData = sceneData;
        }

        public static void UnloadSceneAsync() {
            _sceneData.SetParent(null);
            SceneManager.UnloadSceneAsync(_sceneData.name);
            _sceneData = null;
        }
    }
}
﻿using UnityEngine.SceneManagement;
using System.Collections.Generic;

namespace PepUp.SceneManagement {
    public static class SceneQueue {

        private static Queue<SceneData> scenes = new Queue<SceneData>();

        public static void LoadSceneAsync(SceneData sceneData, LoadSceneMode mode, int limit = -1) {
            SceneManager.LoadSceneAsync(sceneData.name, mode);
            if (scenes.Count >= limit && limit != -1) {
                UnloadSceneAsync();
            }
            AddScene(sceneData);
        }

        public static void UnloadSceneAsync() {
            if (scenes.Count == 0) return;
            SceneData sceneData = RemoveScene();
            sceneData.SetParent(null);
            SceneManager.UnloadSceneAsync(sceneData.name);
        }

        public static void UnloadAllScenesAsync() {
            for (int i = 0; i < scenes.Count; i = 0) {
                UnloadSceneAsync();
            }
        }

        public static void ClearScenes() {
            scenes.Clear();
        }

        private static void AddScene(SceneData sceneData) {
            scenes.Enqueue(sceneData);
        }

        private static SceneData RemoveScene() {
            return scenes.Dequeue();
        }

        public static bool IsSceneLoaded(int index) {
            return SceneManager.GetSceneByBuildIndex(index).IsValid();
        }

        public static bool IsSceneLoaded(string name) {
            return SceneManager.GetSceneByName(name).IsValid();
        }
    }
}
﻿using UnityEngine;

namespace PepUp.Pathfinding {
    public class Node : BaseNode<NodeData> {

        [SerializeField, HideInInspector]
        private NodeData _nodeData;

        public override NodeData data { get => _nodeData; set => _nodeData = value; }
    }
}

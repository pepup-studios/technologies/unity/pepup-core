﻿using UnityEngine;

namespace PepUp.Pathfinding {
    [System.Serializable]
    public class NodeLink {

        public int target;
        public Vector3 direction;

        public NodeLink() { }

        public NodeLink(int target) {
            this.target = target;
        }

        public NodeLink(int target, Vector3 direction) {
            this.target = target;
            this.direction = direction;
        }
    }
}
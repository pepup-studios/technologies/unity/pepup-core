﻿using UnityEngine;

namespace PepUp.Pathfinding {
    [CreateAssetMenu(menuName = "PepUp/Path/Path Data")]
    public class PathData : ScriptableObject {

        public Path map;

#if UNITY_EDITOR
        protected void OnValidate() {
            if (Application.isPlaying) return;
            map.Init();
        }
#endif
    }
}
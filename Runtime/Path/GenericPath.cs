﻿using UnityEngine;
using System.Collections.Generic;
using PepUp.Utility;

namespace PepUp.Pathfinding {
    [System.Serializable]
    public abstract class GenericPath<T> where T : NodeData {

        public string tag;

        public NodeSettings globalNodeSettings;

        public abstract List<T> nodes { get; protected set; }

        public void Init() {
            int len = nodes.Count;
            for (int i = 0; i < len; i++) {
                nodes[i].index = i;
            }
        }

        public void UpdateNode<J>(int index, J node) where J : BaseNode<T> {
            nodes[index].UpdateNode(node);
        }

        public T GetNode(int index) {
            return nodes[index];
        }

        public void AddNode(T nodeData) {
            nodes.Add(nodeData);
        }

        public void RemoveNode(int index) {
            nodes.RemoveAt(index);
        }

        public void RemoveNode(T nodeData) {
            nodes.Remove(nodeData);
        }

        public void Clear() {
            nodes.Clear();
        }

        public T GetNearestNode(Vector3 targetPoint, Vector3 distanceDir) {
            return DistanceUtility.GetNearestPointByDistance(nodes.ToArray(), targetPoint, distanceDir);
        }
    }
}
﻿using UnityEngine;

[CreateAssetMenu(menuName = "PepUp/Path/Settings")]
public class NodeSettings : ScriptableObject {

    public Color wireSphereColor = Color.white;
    public Color activeWireSphereColor = Color.blue;
    public float wireSphereRadius = 1;

    [Space]
    public Color linkColor = Color.white;
    public Color activeLinkColor = Color.blue;
}

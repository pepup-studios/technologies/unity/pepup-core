﻿using System.Collections.Generic;
using UnityEngine;

namespace PepUp.Pathfinding {
    [System.Serializable]
    public class Path : GenericPath<NodeData> {

        [SerializeField]
        private List<NodeData> _nodes = new List<NodeData>();

        public override List<NodeData> nodes { get => _nodes; protected set => _nodes = value; }
    }
}
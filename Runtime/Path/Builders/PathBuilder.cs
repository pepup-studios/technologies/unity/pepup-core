﻿using UnityEngine;

namespace PepUp.Pathfinding {
    public class PathBuilder : BasePathBuilder<Path, Node, NodeData> {

        public PathData pathData;

        [Header("Node Settings")]
        public bool useNodePrefab;
        public Node nodePrefab;

        public override Path path { get => pathData?.map; set { if (pathData != null) pathData.map = value; } }

        public override Node GetNewNode() {
            if (useNodePrefab) {
                return Instantiate(nodePrefab);
            }
            GameObject newGO = new GameObject("New Node");
            Node newNode = newGO.AddComponent<Node>();
            return newNode;
        }

        protected override NodeData GetNewNodeData(int index, Node node, NodeSettings nodeSettings) {
            var newNodeData = new NodeData(index, nodeSettings);
            newNodeData.UpdateNode(node);
            return newNodeData;
        }

#if UNITY_EDITOR
        protected override void SetSODirty() {
            UnityEditor.EditorUtility.SetDirty(pathData);
        }
#endif
    }
}
﻿using UnityEngine;
using System.Collections.Generic;

namespace PepUp.Pathfinding {
    public abstract class BasePathBuilder<J, F, T> : MonoBehaviour
        where J : GenericPath<T> where T : NodeData where F : BaseNode<T> {

        protected List<F> childNodes = new List<F>();
        protected Dictionary<F, J> pathMap = new Dictionary<F, J>();

        public abstract J path { get; set; }

        #region WriteToPath

        public F CreateNode(Vector3 position) {
            F newNode = GetNewNode();
            newNode.transform.position = position;
            newNode.transform.SetParent(transform);

            path.AddNode(newNode.data);
            return newNode;
        }

        public virtual void LoadNode(F node, T nodeData, string tag) {
            if (node.pathTag != tag) return;
            LoadNode(node, nodeData);
        }

        public virtual void LoadNode(F node, T nodeData) {
            childNodes.Add(node);
            node.Init(nodeData);
            path.AddNode(node.data);
            //pathMap.Add(node, path);
            AddToPath(node);
        }

        #endregion

        #region ReadFromPath

        public virtual void ConnectNode(F node, T nodeData, string tag) {
            if (node.pathTag != tag) return;
            ConnectNode(node, nodeData);
        }

        public virtual void ConnectNode(F node, T nodeData) {
            node.Init(nodeData);
            childNodes.Add(node);
        }

        #endregion

        public virtual void DisconnectNode(F node) {
            node.data = null;
            node.pathTag = "";
        }

        public virtual void UnloadNode(F node) {
            node.data = null;
        }

        public virtual void LinkNode(F node, F target) {
#if UNITY_EDITOR
            SetSODirty();
#endif
            T nodeData = node.data;
            T targetNodeData = target.data;
            nodeData.AddLink(targetNodeData);
        }

        public virtual void LinkNode(F node, Vector3 position) {
#if UNITY_EDITOR
            SetSODirty();
#endif
            T nodeData = node.data;
            F targetNode = CreateNode(position);
            nodeData.AddLink(targetNode.data);
        }

        public virtual void DelinkNode(F node, NodeLink nodeLink) {
#if UNITY_EDITOR
            SetSODirty();
#endif
            T nodeData = node.data;
            nodeData.RemoveLink(nodeLink);
        }

        protected void AddToPath(F node) {
            if (pathMap == null) {
                pathMap = new Dictionary<F, J>();
            }
            pathMap.Add(node, path);
        }

        public abstract F GetNewNode();

        protected abstract T GetNewNodeData(int index, F node, NodeSettings nodeSettings);

#if UNITY_EDITOR

        //[HideInInspector]
        //public int prevPathID;

        protected virtual void OnValidate() {
            //if (path != null) {
            //    if (prevPathID != path.GetHashCode()) {
            //        pathMap.Clear();
            //    }
            //    prevPathID = path.GetHashCode();
            //} else {
            //    pathMap = new Dictionary<F, J>();
            //}
            foreach (var mapPath in pathMap.Values) {
                if (path != mapPath) {
                    DisconnectChildNodes();
                }
                ConnectChildNodes();
                break;
            }
        }

        protected abstract void SetSODirty();

        public virtual void UnloadChildNodes() {
            foreach (var node in childNodes) {
                UnloadNode(node);
            }
            childNodes.Clear();
            path.Clear();
            pathMap.Clear();
        }

        public virtual void LoadChildNodes() {
            childNodes.Clear();
            path.Clear();
            pathMap.Clear();

            F[] nodes = transform.GetComponentsInChildren<F>();
            int len = nodes.Length;
            for (int i = 0; i < len; i++) {
                F node = nodes[i];
                T nodeData = GetNewNodeData(i, node, path.globalNodeSettings);
                LoadNode(node, nodeData, path.tag);
            }
        }

        public virtual void ConnectChildNodes() {
            childNodes.Clear();
            pathMap.Clear();
            List<F> nodes = new List<F>(transform.GetComponentsInChildren<F>());
            int nodeLen = path.nodes.Count;

            for (int i = 0; i < nodeLen; i++) {
                T nodeData = path.GetNode(i);

                int childNodeLen = nodes.Count;
                bool foundNode = false;
                for (int j = 0; j < childNodeLen; j++) {
                    F node = nodes[j];
                    if (node.transform.position == nodeData.position) { //nodes must match position
                        ConnectNode(node, nodeData, path.tag);
                        foundNode = true;
                        break;
                    }
                }
                if (!foundNode) {
                    ConnectNode(CreateNode(nodeData.position), nodeData, path.tag);
                }
            }
        }

        public virtual void DisconnectChildNodes() {
            foreach (var node in childNodes) {
                DisconnectNode(node);
            }
            childNodes.Clear();
            pathMap.Clear();
        }

        protected virtual void OnDrawGizmos() {
            if (path == null) return;
            foreach (var node in childNodes) {
                node.OnCustomDrawGizmos(path.nodes.ToArray());
            }
        }
#endif
    }
}
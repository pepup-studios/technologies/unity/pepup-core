﻿using System.Collections.Generic;
using UnityEngine;

namespace PepUp.Pathfinding {
    [System.Serializable]
    public class NodeData : IPointable {

        [Disable]
        public int index;
        [Disable]
        public int siblingIndex;

        public NodeSettings settings;

        [Header("Transform")]
        public Vector3 position;
        public Vector3 localPosition;
        public Quaternion rotation;
        public Vector3 localScale;
        public Vector3 up;
        public Vector3 right;
        public Vector3 forward;

        [Header("Node")]
        [SerializeField]
        private List<NodeLink> _links;
        public List<NodeLink> links { get { return _links; } }

        public NodeData(int index) {
            this.index = index;
            _links = new List<NodeLink>();
        }

        public NodeData(int index, NodeSettings settings) {
            this.index = index;
            this.settings = settings;
            _links = new List<NodeLink>();
        }

        public virtual void UpdateNode<T>(BaseNode<T> node) where T : NodeData {
            //Gameobject
            siblingIndex = node.transform.GetSiblingIndex();

            //Transform
            Transform transform = node.transform;
            position = transform.position;
            localPosition = transform.localPosition;
            rotation = transform.rotation;
            localScale = transform.localScale;
            up = transform.up;
            right = transform.right;
            forward = transform.forward;
        }

        public virtual void UpdateNode<T>(BaseNode<T> node, NodeData[] nodeDataList) where T : NodeData {
            UpdateNode(node);
            foreach (var link in _links) {
                link.direction = GetDirection(nodeDataList[link.target].position);
            }
        }

        public Vector3 GetPoint() {
            return position;
        }

        public void AddLink<T>(T noteData) where T : NodeData {
            var nodeLink = GetNewNodeLink(noteData);
            _links.Add(nodeLink);
        }

        protected virtual NodeLink GetNewNodeLink<T>(T noteData) where T : NodeData {
            return new NodeLink(noteData.index, GetDirection(noteData.position));
        }

        public void RemoveLink(NodeLink noteLink) {
            _links.Remove(noteLink);
        }

        public void RemoveIndex(int index) {
            _links.RemoveAt(index);
        }

        public void ClearLink() {
            _links.Clear();
        }

        protected Vector3 GetDirection(Vector3 target) {
            Vector3 relative = target - position;
            return relative;
        }
    }
}
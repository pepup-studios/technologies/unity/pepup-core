﻿using UnityEngine;
using PepUp.Utility;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace PepUp.Pathfinding {
    public abstract class BaseNode<T> : MonoBehaviour where T : NodeData {

        public string pathTag = "";

        public abstract T data { get; set; }

        public virtual void Init(T data) {
            this.data = data;
            UpdateNodeData();
        }

        public virtual void UpdateNodeData() {
            data.UpdateNode(this);
        }

        public virtual void UpdateNodeData(NodeData[] nodeDataList) {
            data.UpdateNode(this, nodeDataList);
        }

#if UNITY_EDITOR
        public bool activeNode { get; set; }

        public virtual void OnCustomDrawGizmos(NodeData[] nodeDataList) {
            int nodeDataLen = nodeDataList.Length;
            if (data == null || nodeDataLen == 0) return;
            if (data.settings == null) {
                Debug.LogError("Cannot render node missing settings in path data");
                return;
            }

            UpdateNodeData(nodeDataList); data.UpdateNode(this, nodeDataList);

            bool activeNodeFlag = false;
            GameObject newGO = Selection.activeGameObject;
            if (newGO != null) {
                BaseNode<T> selectNode = newGO.GetComponent<BaseNode<T>>();
                if (selectNode != null && selectNode.GetInstanceID() == GetInstanceID()) {
                    activeNodeFlag = true;
                }
            }
            activeNode = activeNodeFlag;

            Gizmos.color = activeNode ? data.settings.activeWireSphereColor : data.settings.wireSphereColor;

            DrawNode();

            Gizmos.color = activeNode ? data.settings.activeLinkColor : data.settings.linkColor;
            foreach (var nodeLink in data.links) {
                DrawUtility.GizmoArrow(data.position, nodeLink.direction);
            }

            Gizmos.color = Color.white;
        }

        protected virtual void DrawNode() {
            Gizmos.DrawSphere(transform.position, data.settings.wireSphereRadius);
        }
#endif
    }
}
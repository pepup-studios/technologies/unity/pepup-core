﻿namespace PepUp.Grid {
    public interface IGridable {
        void OnEnter(GridObject gridObject);
        void OnStay(GridObject gridObject);
        void OnExit(GridObject gridObject);
    }
}
﻿using UnityEngine;

namespace PepUp.Grid {
    [AddComponentMenu("PepUp/Grid/Grid Object")]
    public class GridObject : MonoBehaviour {

        [HideInInspector]
        public GridCell cell;

        [HideInInspector]
        public GridManager gridManager;

#if UNITY_EDITOR
        [Header("Editor")]
        public bool gridSnap = true;

        public bool GridBlock { get; set; }
#endif
    }
}
﻿using UnityEngine;

namespace PepUp.Grid {
    [CreateAssetMenu(menuName = "PepUp/Grid/Grid Data")]
    public class GridData : ScriptableObject {

        public Vector3 cellSize;
        public Vector3 cellGap;
        public GridAxis axis;
        public int row, column;
    }
}
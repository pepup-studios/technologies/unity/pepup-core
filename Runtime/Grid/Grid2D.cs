﻿using UnityEngine;
using System.Collections.Generic;
using PepUp.Utility;

namespace PepUp.Grid {
    [System.Serializable]
    public class Grid2D {

        [SerializeField]
        public Vector3 Center;

        [SerializeField]
        public Vector3 CellSize;

        [SerializeField]
        public Vector3 CellGap;

        [SerializeField]
        public int RowCount;

        [SerializeField]
        public int ColCount;

        [SerializeField]
        private GridAxis _axis;
        public GridAxis Axis { get { return _axis; } set { _axis = value; SetGridDirection(_axis); } }

        [SerializeField]
        public int Count { get; private set; }

        [SerializeField] [HideInInspector]
        protected List<GridCell> cells;

        private AxisDirection gridDirections;

        private Vector3 _cellResized { get { return CellSize / 10; } }
        private Vector3 _cellGapResized { get { return CellGap / 10; } }
        private Vector3 _cellCalSize { get { return _cellResized + _cellGapResized; } }

        public Grid2D(Vector3 center, Vector3 cellSize, Vector3 cellGap, int row, int col, GridAxis axis) {
            Center = center;
            CellSize = cellSize;
            CellGap = cellGap;
            RowCount = row; 
            ColCount = col;
            Axis = axis;
            Count = row * col;

            cells = new List<GridCell>(Count);
            Create();
        }

        public Grid2D(Vector3 center, GridData data) {
            Center = center;
            CellSize = data.cellSize;
            CellGap = data.cellGap;
            RowCount = data.row;
            ColCount = data.column;
            Axis = data.axis;
            Count = data.row * data.column;

            cells = new List<GridCell>(Count);
            Create();
        }

        public void Create() {
            cells.Clear();

            Vector3 gridSize = Vector3Utility.MultiplyByAxis(_cellCalSize, gridDirections, new Vector3(ColCount, RowCount, 0));
            Vector3 cellOffset = (gridSize - _cellCalSize) / -2;

            int counter = 0;
            for (int i = 0; i < RowCount; i++) {
                for (int j = 0; j < ColCount; j++) {
                    GridCell newCell = CreateCell(counter, i, j, cellOffset);
                    cells.Add(newCell);
                    counter++;
                }
            }
        }

        protected GridCell CreateCell(int index, int rowIndex, int colIndex, Vector3 cellOffset) {
            return new GridCell {
                index = index,
                coordinate = new Vector2(colIndex, rowIndex),
                position = Vector3Utility.MultiplyByAxis(_cellCalSize, gridDirections, new Vector3(colIndex, rowIndex, 0)) + Center + cellOffset,
                size = CellSize,
                gap = CellGap
            };
        }

        public GridCell GetCell(int index) {
            return cells[index];
        }

        public GridCell[] GetCells() {
            return cells.ToArray();
        }

        public void OnDrawGizmos(Color gridColor) {
            foreach (var cell in cells) {
                Gizmos.color = gridColor;
                Gizmos.DrawWireCube(cell.position, _cellResized);
            }
        }

        protected void SetGridDirection(GridAxis axis) {
            switch (axis) {
                case GridAxis.XZY:
                    gridDirections = new AxisDirection(new Vector3(0, 1, 0), new Vector3(1, 0, 0), new Vector3(0, 0, 1));
                    break;
                case GridAxis.XYZ:
                    gridDirections = new AxisDirection(new Vector3(0, 0, 1), new Vector3(1, 0, 0), new Vector3(0, 1, 0));
                    break;
                default:
                    gridDirections = new AxisDirection(Vector3.up, Vector3.right, Vector3.forward);
                    break;
            }
        }
    }

    public enum GridAxis { XZY, XYZ }
}
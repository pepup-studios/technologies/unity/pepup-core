﻿using UnityEngine;
using System.Collections.Generic;

namespace PepUp.Grid {
#if UNITY_EDITOR
    [ExecuteInEditMode]
#endif
    [AddComponentMenu("PepUp/Grid/Grid Manager")]
    public class GridManager : MonoBehaviour {

        public GridData gridData;

        [SerializeField] [HideInInspector]
        protected Grid2D grid;

        [SerializeField] [HideInInspector]
        protected List<GridObject> gridObjects;

        public void ResetGrid() {
            grid.Center = transform.position;
            grid.CellSize = gridData.cellSize;
            grid.CellGap = gridData.cellGap;
            grid.Axis = gridData.axis;
            grid.RowCount = gridData.row;
            grid.ColCount = gridData.column;

            grid.Create();
        }

        public GridCell GetGridCell(int index) {
            return grid.GetCell(index);
        }

        public void Register(params object[] objs) {
            GridObject gridObject = (GridObject) objs[0];
            gridObject.gridManager = this;
            gridObjects.Add(gridObject);
        }

        public bool Unregister(params object[] objs) {
            GridObject gridObject = (GridObject) objs[0];
            gridObject.gridManager = null;
            return gridObjects.Remove(gridObject);
        }

#if UNITY_EDITOR

        protected void Awake() {
            if (Application.isPlaying || gridData == null) return;
            grid = new Grid2D(transform.position, gridData);
            gridObjects = new List<GridObject>();
        }

        [Header("Editor")]
        public bool drawGrid;
        public Color gridColor = Color.green;

        private Vector3 _prevPos;

        public void OnDrawGizmos() {
            if (!drawGrid || gridData == null) return;
            grid = new Grid2D(transform.position, gridData);
            grid.OnDrawGizmos(gridColor);
        }

        protected void Update() {
            if (Application.isPlaying || gridData == null) return;
            LoadChildGridObjects();
            _prevPos = transform.position;
        }

        protected void LoadChildGridObjects() {
            gridObjects.Clear();
            foreach (var child in transform.GetComponentsInChildren<GridObject>()) {
                LoadChildGridObject(child);
            }
        }

        protected void LoadChildGridObject(GridObject marker) {
            Register(marker);
            if (transform.position == _prevPos) {
                marker.GridBlock = false;
                UpdateGridObjectCell(marker);
            } else {
                marker.GridBlock = true;
            }
        }

        public void UpdateGridObjectCell(GridObject gridObject) {
            float minDistance = -1;
            GridCell gridCell = default;
            foreach (var cell in grid.GetCells()) {
                float distance = Vector3.Distance(gridObject.transform.position, cell.position);
                if (distance <= minDistance || minDistance < 0) {
                    minDistance = distance;
                    gridCell = cell;
                }
            }
            if (minDistance >= 0) {
                gridObject.cell = gridCell;
            }
        }
#endif
    }
}
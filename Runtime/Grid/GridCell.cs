﻿using UnityEngine;

namespace PepUp.Grid {
    [System.Serializable]
    public struct GridCell {

        public int index;
        public Vector2 coordinate;
        public Vector3 position;
        public Vector3 size;
        public Vector3 gap;
    }
}
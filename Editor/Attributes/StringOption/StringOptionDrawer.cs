﻿using UnityEditor;
using UnityEngine;

namespace PepUp {
    [CustomPropertyDrawer(typeof(StringOption))]
    public class StringOptionDrawer : PropertyDrawer {

        /// <summary>
        /// Options to display in the popup to select constant or variable.
        /// </summary>
        private readonly string[] popupOptions = { "Value", "Object" };

        /// <summary> Cached style to use to draw the popup button. </summary>
        private GUIStyle popupStyle;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            if (popupStyle == null) {
                popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"));
                popupStyle.imagePosition = ImagePosition.ImageOnly;
            }

            label = EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, label);

            EditorGUI.BeginChangeCheck();

            // Get properties
            SerializedProperty isValue = property.FindPropertyRelative("isValue");
            SerializedProperty value = property.FindPropertyRelative("stringValue");
            SerializedProperty obj = property.FindPropertyRelative("stringObject");

            // Calculate rect for configuration button
            Rect buttonRect = new Rect(position);
            buttonRect.yMin += popupStyle.margin.top;
            buttonRect.width = popupStyle.fixedWidth + popupStyle.margin.right;
            position.xMin = buttonRect.xMax;

            // Store old indent level and set it to 0, the PrefixLabel takes care of it
            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            int result = EditorGUI.Popup(buttonRect, isValue.boolValue ? 0 : 1, popupOptions, popupStyle);

            isValue.boolValue = result == 0;

            EditorGUI.PropertyField(position,
                isValue.boolValue ? value : obj,
                GUIContent.none);

            if (EditorGUI.EndChangeCheck())
                property.serializedObject.ApplyModifiedProperties();

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            SerializedProperty isValue = property.FindPropertyRelative("isValue");
            if (isValue.boolValue) {
                return base.GetPropertyHeight(property, label) + 16;
            }
            return base.GetPropertyHeight(property, label);
        }
    }
}
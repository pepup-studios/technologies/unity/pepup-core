﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace PepUp {
    [CustomPropertyDrawer(typeof(StringsOption))]
    public class StringsOptionDrawer : PropertyDrawer {

        /// <summary>
        /// Options to display in the popup to select constant or variable.
        /// </summary>
        private readonly string[] popupOptions = { "Value", "Object" };

        protected int length;
        protected bool doShow = false;

        /// <summary> Cached style to use to draw the popup button. </summary>
        private GUIStyle popupStyle;
        private ReorderableList list;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            if (popupStyle == null) {
                popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"));
                popupStyle.imagePosition = ImagePosition.ImageOnly;
            }

            Rect originalRect = new Rect(position);
            label = EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, label);

            EditorGUI.BeginChangeCheck();

            // Get properties
            SerializedProperty isValue = property.FindPropertyRelative("isValue");
            SerializedProperty value = property.FindPropertyRelative("stringValues");
            SerializedProperty obj = property.FindPropertyRelative("stringsObject");

            // Calculate rect for configuration button
            Rect buttonRect = new Rect(position);
            buttonRect.height = 18;
            buttonRect.yMin += popupStyle.margin.top;
            buttonRect.width = popupStyle.fixedWidth + popupStyle.margin.right;
            position.xMin = buttonRect.xMax;

            // Store old indent level and set it to 0, the PrefixLabel takes care of it
            int indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            int result = EditorGUI.Popup(buttonRect, isValue.boolValue ? 0 : 1, popupOptions, popupStyle);

            isValue.boolValue = result == 0;

            if (isValue.boolValue) {
                EditorGUI.BeginProperty(position, label, property);
                float height = 0;// EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
                Rect listRect = new Rect(position.x, position.y + height, position.width, EditorGUIUtility.singleLineHeight);

                if (list == null) {
                    SerializedProperty stringValues = property.FindPropertyRelative("stringValues");
                    list = new ReorderableList(property.serializedObject, stringValues, false, true, true, true);

                    list.drawHeaderCallback = (Rect rect) => {
                        EditorGUI.LabelField(rect, "Size: " + length);
                    };

                    list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
                        EditorGUI.PropertyField(rect, stringValues.GetArrayElementAtIndex(index), GUIContent.none);
                    };
                }
                list.DoList(listRect);
                length = list.count;
                EditorGUI.EndProperty();
            } else {
                EditorGUI.PropertyField(position, obj, GUIContent.none);
            }

            if (EditorGUI.EndChangeCheck())
                property.serializedObject.ApplyModifiedProperties();

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            SerializedProperty isValue = property.FindPropertyRelative("isValue");
            if (isValue.boolValue) {
                return (list != null) ? list.GetHeight() : base.GetPropertyHeight(property, label);
            }
            return base.GetPropertyHeight(property, label);
        }
    }
}
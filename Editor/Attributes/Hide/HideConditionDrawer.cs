﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(HideConditionAttribute))]
public class HideConditionDrawer : PropertyDrawer {

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        if (!GetCondition(property)) {
            return base.GetPropertyHeight(property, label) - 18f;
        }
        HideConditionAttribute hideAttrib = attribute as HideConditionAttribute;
        return base.GetPropertyHeight(property, label) + hideAttrib.propertyHeight;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        EditorGUI.BeginProperty(position, label, property);

        if (GetCondition(property)) {
            EditorGUI.PropertyField(position, property, true);
        }            
        EditorGUI.EndProperty();
    }

    //void SetLabelPosition(Rect position, GUIContent label) {
    //    EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
    //}

    bool GetCondition(SerializedProperty property) {
        HideConditionAttribute hideAttrib = attribute as HideConditionAttribute;

        string propertyPath = property.propertyPath;
        string conditionPath = hideAttrib.conditionName;
        string keyToFind = ".";
        if (propertyPath.Contains(keyToFind)) {
            int i = propertyPath.LastIndexOf(keyToFind);
            conditionPath = i < 0 ? "" : propertyPath.Substring(0, i + keyToFind.Length) + hideAttrib.conditionName;
        }
        SerializedProperty conditionProperty = property.serializedObject.FindProperty(conditionPath);

        if (conditionProperty.propertyType == SerializedPropertyType.Boolean) {
            if (conditionProperty.boolValue.ToString().ToLower().Equals(hideAttrib.conditionValue.ToLower())) {
                return true;
            }
        } else if (conditionProperty.propertyType == SerializedPropertyType.Integer) {
            if (conditionProperty.intValue.ToString().Equals(hideAttrib.conditionValue)) {
                return true;
            }
        } else if (conditionProperty.propertyType == SerializedPropertyType.Float) {
            if (conditionProperty.floatValue.ToString().Equals(hideAttrib.conditionValue)) {
                return true;
            }
        } else if (conditionProperty.propertyType == SerializedPropertyType.String) {
            if (conditionProperty.stringValue.Equals(hideAttrib.conditionValue)) {
                return true;
            }
        } else if (conditionProperty.propertyType == SerializedPropertyType.Enum) {
            if (int.TryParse(hideAttrib.conditionValue, out int value)) { //If value passed is an integer
                if (conditionProperty.enumValueIndex.Equals(value)) {
                    return true;
                }
            } else { //if value passed is string
                string[] names = conditionProperty.enumNames;
                if (names[conditionProperty.enumValueIndex].Equals(hideAttrib.conditionValue)) {
                    return true;
                }
            }
        }
        return false;
    }
}
﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace PepUp.Editor {
    public class DropDownWindow : EditorWindow {

        public static string prefixPath = "Drop Downs/";

        public static List<DropDownObject> ddObjects = new List<DropDownObject>();

        //protected int length;

        //[MenuItem("PepUp/Drop Down Manager")]
        //public static void ShowWindow() {
        //    GetWindow<DropDownWindow>("Drop Down Manager");
        //}

        protected void OnEnable() {
            var data = EditorPrefs.GetString("dropDownWindow", JsonUtility.ToJson(this, false));
            JsonUtility.FromJsonOverwrite(data, this);
        }

        protected void OnDisable() {
            var data = JsonUtility.ToJson(this, false);
            EditorPrefs.SetString("dropDownWindow", data);
        }

        private void OnGUI() {
            prefixPath = EditorGUILayout.TextField("Resources Path", prefixPath);

            //GUILayout.Space(10);

            //foreach (DropDownObject ddObject in ddObjects) {
            //    EditorGUILayout.ObjectField(ddObject.obj, typeof(DropDownObject), true);
            //    foreach (DropDownCaller ddCaller in ddObject.callers) {
            //        EditorGUILayout.ObjectField(ddCaller.caller, typeof(DropDownCaller), true);
            //        foreach (DropDownProperty ddProperty in ddCaller.properties) {
            //            EditorGUILayout.ObjectField(ddProperty.property, typeof(DropDownProperty), true);
            //        }
            //    }
            //}
        }

        public static void SetDropDown(Object obj, Object caller, Object property, int selectedIndex, BaseDropDown[] targets) {
            DropDownObject newddObject;
            foreach (DropDownObject ddObject in ddObjects) {
                if (ddObject.obj == obj) {
                    newddObject = ddObject;
                    DropDownCaller newddCaller;
                    foreach (DropDownCaller ddCaller in ddObject.callers) {
                        if (ddCaller.caller == caller) {
                            newddCaller = ddCaller;
                            foreach (DropDownProperty ddProperty in ddCaller.properties) {
                                if (ddProperty.property == property) {
                                    ddProperty.selectedIndex = selectedIndex;
                                    ddProperty.targets = targets;
                                    return;
                                }
                            }
                            newddCaller.properties.Add(new DropDownProperty(property, selectedIndex, targets));
                            return;
                        }
                    }
                    newddCaller = new DropDownCaller(caller);
                    newddCaller.properties.Add(new DropDownProperty(property, selectedIndex, targets));
                    return;
                }
            }
            newddObject = new DropDownObject(obj);
            newddObject.callers.Add(new DropDownCaller(caller));
            newddObject.callers[0].properties.Add(new DropDownProperty(property, selectedIndex, targets));

            ddObjects.Add(newddObject);
        }

        public static int GetDropDownSelectedIndex(Object obj, Object caller, Object property, BaseDropDown[] targets) {
            foreach (DropDownObject ddObject in ddObjects) {
                if (ddObject.obj == obj) {
                    foreach (DropDownCaller ddCaller in ddObject.callers) {
                        if (ddCaller.caller == caller) {
                            foreach (DropDownProperty ddProperty in ddCaller.properties) {
                                if (ddProperty.property == property && ddProperty.targets == targets) {
                                    return ddProperty.selectedIndex;
                                }
                            }
                        }
                    }
                }
            }
            return 0;
        }

        public static string GetPrefixPath() {
            return prefixPath[prefixPath.Length - 1] != '/' ? prefixPath + "/" : prefixPath;
        }
    }

    [System.Serializable]
    public class DropDownObject : Object {
        public string objectName;
        public Object obj;

        public List<DropDownCaller> callers;

        public DropDownObject(Object obj) {
            objectName = obj.name;
            this.obj = obj;

            callers = new List<DropDownCaller>();
        }
    }

    [System.Serializable]
    public class DropDownCaller : Object {
        public string callerName;
        public Object caller;

        public List<DropDownProperty> properties;

        public DropDownCaller(Object caller) {
            callerName = caller.name;
            this.caller = caller;

            properties = new List<DropDownProperty>();
        }
    }

    [System.Serializable]
    public class DropDownProperty : Object {
        public string propertyName;
        public Object property;

        public int selectedIndex;
        public BaseDropDown[] targets;

        public DropDownProperty(Object property, int selectedIndex, BaseDropDown[] targets) {
            propertyName = property.name;
            this.property = property;
            this.selectedIndex = selectedIndex;
            this.targets = targets;
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace PepUp.Editor {
    public abstract class BaseDropDownDrawer<T, J> : PropertyDrawer where T : BaseDropDownAttribute where J : BaseDropDown {

        protected T ddAttribute;
        protected J[] ddObjects;

        protected string[] displayValues;
        protected string[] values;
        protected string[] fullValues;

        protected string ddIndexManager;
        protected int selectedIndex = 0;

        protected virtual void Init() {
            ddAttribute = (T) attribute;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            Init();

            LoadDropDownData();
            LoadSelectedIndex(property);

            DisplayPopUp(position, property);
        }

        protected virtual void DisplayPopUp(Rect position, SerializedProperty property) {
            if (displayValues.Length == 0) {
                EditorGUI.HelpBox(position, "Cannot find list or it's empty.", MessageType.Warning);
                return;
            }

            //if (HasValueChanged(property)) {
            //    EditorGUI.HelpBox(position, "Cannot find serialized name on the drop down index map. Please resolve the issue in \"Drop Down Index Manager\"", MessageType.Error);
            //    return;
            //}
            //
            selectedIndex = EditorGUI.Popup(position, property.displayName, selectedIndex, displayValues);

            SetPropertyValue(position, property);
        }

        protected abstract void SetPropertyValue(Rect position, SerializedProperty property);

        protected virtual void LoadDropDownData() {
            ddObjects = LoadResource();

            List<string> newValue = new List<string>();
            List<string> newFullValue = new List<string>();
            List<string> newDisplayValue = new List<string>();

            foreach (J ddObject in ddObjects) {
                string[] values = ddObject.GetStringValues(); ;
                if (ddAttribute.displaySeperator) {
                    newDisplayValue.AddRange(values.Select(x => ddObject.name + "/" + x));
                } else {
                    newDisplayValue.AddRange(values);
                }
                newFullValue.AddRange(values.Select(x => ddObject.name + "/" + x));
                newValue.AddRange(values);
            }
            values = newValue.ToArray();
            fullValues = newFullValue.ToArray();
            displayValues = newDisplayValue.ToArray();
        }

        protected virtual J[] LoadResource() {
            Object[] objs = Resources.LoadAll(ddAttribute.pathName, typeof(J));
            return objs.OfType<J>().ToArray();
        }

        protected virtual void LoadSelectedIndex(SerializedProperty property) {
            //selectedIndex = DropDownWindow.GetDropDownSelectedIndex(property.serializedObject.targetObject, property.serializedObject.context, property.objectReferenceValue, ddObjects);

            string propertyString = GetPropertyString(property);
            ValueType type = GetValueType(propertyString);
            string[] values;
            if (type == ValueType.fullValue) {
                values = fullValues;
            } else { //if (type == ValueType.value
                values = this.values;
            }

            int len = values.Length;
            for (int i = 0; i < len; i++) {
                if (values[i] == propertyString) {
                    selectedIndex = i;
                    return;
                }
            }
        }

        protected virtual bool HasValueChanged(SerializedProperty property) {
            return HasValueChanged(GetPropertyString(property));
        }

        protected virtual bool HasValueChanged(string value) {
            ValueType valueType = GetValueType(value);
            return (valueType == ValueType.fullValue && fullValues[selectedIndex].Equals(value)) ||
                (valueType == ValueType.value && values[selectedIndex].Equals(value));
        }

        protected ValueType GetValueType(string value) {
            if (value.Contains("/")) {
                return ValueType.fullValue;
            }
            return ValueType.value;
        }

        protected abstract string GetPropertyString(SerializedProperty property);
    }

    public enum ValueType { value, fullValue }
}
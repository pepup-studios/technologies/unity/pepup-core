﻿using UnityEditor;
using UnityEngine;

namespace PepUp.Editor {
    [CustomPropertyDrawer(typeof(StringDropDownAttribute))]
    public class StringDropDownDrawer : BaseDropDownDrawer<StringDropDownAttribute, StringDropDown> {

        protected override void SetPropertyValue(Rect position, SerializedProperty property) {
            if (ddAttribute.resultSeperator) {
                property.stringValue = values[selectedIndex];
            } else {
                string[] splitValues = values[selectedIndex].Split('/');
                property.stringValue = splitValues[splitValues.Length - 1];
            }
        }

        protected override string GetPropertyString(SerializedProperty property) {
            return property.stringValue;
        }
    }
}
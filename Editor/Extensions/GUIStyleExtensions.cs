﻿using UnityEngine;

namespace PepUp.Editor {
    public static class GUIStyleExtensions {

        private static float _lineSpace;
        //private static float _fieldNumber;

        //public static void SetFieldNumber(this GUIStyle style, float fieldNumber) {
        //    _fieldNumber = fieldNumber;
        //}

        //public static float GetFieldNumber(this GUIStyle style) {
        //    return _fieldNumber;
        //}

        public static float GetElementHeight(this GUIStyle style) {
            return style.fixedHeight;
        }

        public static float GetLineSpace(this GUIStyle style) {
            return _lineSpace;
        }

        public static void SetLineHeight(this GUIStyle style, float height) {
            style.fixedHeight = height + _lineSpace;
        }

        public static float GetLineHeight(this GUIStyle style) {
            return style.lineHeight - _lineSpace;
        }

        public static void SetLineSpace(this GUIStyle style, float lineSpace) {
            style.fixedHeight = style.lineHeight + (_lineSpace = lineSpace);
        }
    }
}
﻿using UnityEngine;
using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

namespace PepUp.Editor {
    public static class UnityObjectExtensions {

        public static GenericObject<FieldInfo>[] GetGenericObjectFields(this UnityEngine.Object obj, bool includeAllBases = false, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic) {
            try {
                ScriptableObject scriptableObject = (ScriptableObject) obj;
                FieldInfo[] fields = ((object)obj).GetFields(includeAllBases, bindings);
                PropertyInfo[] properties = obj.GetType().GetProperties(bindings);
                string[] fieldNames = fields.Select((x) => x.Name).ToArray();
                string[] propertyNames = properties.Select((x) => x.Name).ToArray();
                var newList = new List<string>(fieldNames);
                newList.AddRange(propertyNames);
                string[] names = newList.ToArray();
                if (names.Length > 0) {
                    return new GenericObject<FieldInfo>[] { new GenericObject<FieldInfo>(scriptableObject, scriptableObject.name, names, fields, names.Length) };
                }
                return null;
            }
            catch { }

            try {
                GameObject gameObject = (GameObject) obj;
                return gameObject.GetGenericComponents(includeAllBases, bindings);
            }
            catch { }

            try {
                Component component = (Component) obj;
                FieldInfo[] fields = ((object) obj).GetFields(includeAllBases, bindings);
                PropertyInfo[] properties = obj.GetType().GetProperties(bindings);
                string[] fieldNames = fields.Select((x) => x.Name).ToArray();
                string[] propertyNames = properties.Select((x) => x.Name).ToArray();
                var newList = new List<string>(fieldNames);
                newList.AddRange(propertyNames);
                string[] names = newList.ToArray();
                if (names.Length > 0) {
                    return new GenericObject<FieldInfo>[] { new GenericObject<FieldInfo>(component, component.name, names, fields, names.Length) };
                }
                return null;
            }
            catch { }

            Type objType = obj.GetType();
            Debug.LogError("Object Type: " + objType.Name + " not supported");
            return null;
        }

        public static GenericObject<MemberInfo>[] GetGenericObjectEvents(this UnityEngine.Object obj, bool includeAllBases = false, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic) {
            try {
                ScriptableObject scriptableObject = (ScriptableObject) obj;
                MemberInfo[] events = ((object) obj).GetEvents(includeAllBases, bindings);
                string[] names = events.Select((x) => x.Name).ToArray();
                if (names.Length > 0) {
                    return new GenericObject<MemberInfo>[] { new GenericObject<MemberInfo>(scriptableObject, scriptableObject.name, names, events, events.Length) };
                }
                return null;
            }
            catch { }

            try {
                GameObject gameObject = (GameObject) obj;
                return gameObject.GetEventGenericComponents(includeAllBases, bindings);
            }
            catch { }

            try {
                Component component = (Component) obj;
                MemberInfo[] events = ((object) obj).GetEvents(includeAllBases, bindings);
                string[] names = events.Select((x) => x.Name).ToArray();
                if (names.Length > 0) {
                    return new GenericObject<MemberInfo>[] { new GenericObject<MemberInfo>(component, component.name, names, events, events.Length) };
                }
                return null;
            }
            catch { }

            Type objType = obj.GetType();
            Debug.LogError("Object Type: " + objType.Name + " not supported");
            return null;
        }

        public static GenericObject<MethodInfo>[] GetGenericObjectMethods(this UnityEngine.Object obj, Type type = null, string namePrefix = null, bool includeAllBases = false, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.DeclaredOnly) {
            try {
                ScriptableObject scriptableObject = (ScriptableObject) obj;
                MethodInfo[] methods = ((object) obj).GetMethods(type, namePrefix, includeAllBases, bindings);
                string[] names = methods.Select((x) => x.Name).ToArray();
                if (names.Length > 0) {
                    return new GenericObject<MethodInfo>[] { new GenericObject<MethodInfo>(scriptableObject, scriptableObject.name, names, methods, methods.Length) };
                }
                return null;
            }
            catch { }

            try {
                GameObject gameObject = (GameObject) obj;
                return gameObject.GetMethodGenericComponents(type, namePrefix, includeAllBases, bindings);
            }
            catch { }

            try {
                Component component = (Component) obj;
                MethodInfo[] methods = ((object) obj).GetMethods(type, namePrefix, includeAllBases, bindings);
                string[] names = methods.Select((x) => x.Name).ToArray();
                if (names.Length > 0) {
                    return new GenericObject<MethodInfo>[] { new GenericObject<MethodInfo>(component, component.name, names, methods, methods.Length) };
                }
                return null;
            }
            catch { }

            Type objType = obj.GetType();
            Debug.LogError("Object Type: " + objType.Name + " not supported");
            return null;
        }

        public static GenericObject<FieldInfo>[] GetGenericComponents(this GameObject gameObject, bool includeAllBases = false, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic) {
            List<Component> components = new List<Component>(gameObject.GetComponents<Component>());
            List<GenericObject<FieldInfo>> genericObjects = new List<GenericObject<FieldInfo>>();
            List<string> tempNames = new List<string>();
            for (int i = 0; i < components.Count; i++) {
                Component component = components[i];
                string objName = component.GetType().Name;
                string suffix = tempNames.Contains(objName) ? " (" + component.GetInstanceID() + ")" : "";
                FieldInfo[] fields = ((object) component).GetFields(includeAllBases, bindings);
                PropertyInfo[] properties = component.GetType().GetProperties(bindings);
                string[] fieldNames = fields.Select((x) => objName + suffix + "/" + x.Name).ToArray();
                string[] propertyNames = properties.Select((x) => objName + suffix + "/" + x.Name).ToArray();
                var newList = new List<string>(fieldNames);
                newList.AddRange(propertyNames);
                string[] names = newList.ToArray();
                //string[] names = fields.Select((x) => objName + suffix + "/" + x.Name).ToArray();
                if (fields.Length > 0) {
                    genericObjects.Add(new GenericObject<FieldInfo>(component, component.name, names, fields, names.Length));
                }
                tempNames.Add(objName);
            }
            return genericObjects.ToArray();
        }

        public static GenericObject<MemberInfo>[] GetEventGenericComponents(this GameObject gameObject, bool includeAllBases = false, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic) {
            List<Component> components = new List<Component>(gameObject.GetComponents<Component>());
            List<GenericObject<MemberInfo>> genericObjects = new List<GenericObject<MemberInfo>>();
            List<string> tempNames = new List<string>();
            for (int i = 0; i < components.Count; i++) {
                Component component = components[i];
                string objName = component.GetType().Name;
                string suffix = tempNames.Contains(objName) ? " (" + component.GetInstanceID() + ")" : "";
                MemberInfo[] events = ((object) component).GetEvents(includeAllBases, bindings);
                string[] names = events.Select((x) => objName + suffix + "/" + x.Name).ToArray();
                if (events.Length > 0) {
                    genericObjects.Add(new GenericObject<MemberInfo>(component, component.name, names, events, events.Length));
                }
                tempNames.Add(objName);
            }
            return genericObjects.ToArray();
        }

        public static GenericObject<MethodInfo>[] GetMethodGenericComponents(this GameObject gameObject, Type type, string namePrefix, bool includeAllBases = false, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly) {
            List<Component> components = new List<Component>(gameObject.GetComponents<Component>());
            List<GenericObject<MethodInfo>> genericObjects = new List<GenericObject<MethodInfo>>();
            List<string> tempNames = new List<string>();
            for (int i = 0; i < components.Count; i++) {
                Component component = components[i];
                string objName = component.GetType().Name;
                string suffix = tempNames.Contains(objName) ? " (" + component.GetInstanceID() + ")" : "";
                MethodInfo[] methods = ((object) component).GetMethods(type, namePrefix, includeAllBases, bindings);
                string[] names = methods.Select((x) => objName + suffix + "/" + x.Name).ToArray();
                if (methods.Length > 0) {
                    genericObjects.Add(new GenericObject<MethodInfo>(component, component.name, names, methods, methods.Length));
                }
                tempNames.Add(objName);
            }
            return genericObjects.ToArray();
        }
    }

    [Serializable]
    public class GenericObject<T> {
        public UnityEngine.Object obj;
        public string name;
        public string[] paths;
        public int length;
        public T[] fields;

        public GenericObject(UnityEngine.Object obj, string name, string[] paths, T[] fields, int length) {
            this.obj = obj;
            this.name = name;
            this.paths = paths;
            this.fields = fields;
            this.length = length;
        }
    }
}

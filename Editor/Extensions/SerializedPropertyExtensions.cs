﻿using System.Collections.Generic;
using System.Linq;
 
namespace PepUp.Editor {
    using System;
    using UnityEngine;
    using UnityEditor;
    using System.Reflection;

    public static class SerializedPropertyExtensions {

        public static T GetValue<T>(this SerializedProperty property) {
            return GetNestedObject<T>(property.propertyPath, GetSerializedPropertyRootComponent(property));
        }

        public static bool SetValue<T>(this SerializedProperty property, T value) {
            object obj = null;
            try {
                obj = GetSerializedPropertyRootComponent(property);
            }
            catch {
                try {
                    obj = GetSerializedPropertyRootSO(property);
                }
                catch { }
            }
            //Iterate to parent object of the value, necessary if it is a nested object
            string[] fieldStructure = property.propertyPath.Split('.');
            for (int i = 0; i < fieldStructure.Length - 1; i++) {
                obj = GetFieldOrPropertyValue<object>(fieldStructure[i], obj);
            }
            string fieldName = fieldStructure.Last();

            return SetFieldOrPropertyValue(fieldName, obj, value);
        }

        public static Component GetSerializedPropertyRootComponent(SerializedProperty property) {
            return (Component) property.serializedObject.targetObject;
        }

        public static ScriptableObject GetSerializedPropertyRootSO(SerializedProperty property) {
            return (ScriptableObject) property.serializedObject.targetObject;
        }

        public static T GetNestedObject<T>(string path, object obj, bool includeAllBases = false) {
            foreach (string part in path.Split('.')) {
                obj = GetFieldOrPropertyValue<object>(part, obj, includeAllBases);
            }
            return (T) obj;
        }

        public static T GetFieldOrPropertyValue<T>(string fieldName, object obj, bool includeAllBases = false, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic) {
            FieldInfo field = obj.GetType().GetField(fieldName, bindings);
            if (field != null) return (T) field.GetValue(obj);

            PropertyInfo property = obj.GetType().GetProperty(fieldName, bindings);
            if (property != null) return (T) property.GetValue(obj, null);

            if (includeAllBases) {

                foreach (Type type in GetBaseClassesAndInterfaces(obj.GetType())) {
                    field = type.GetField(fieldName, bindings);
                    if (field != null) return (T) field.GetValue(obj);

                    property = type.GetProperty(fieldName, bindings);
                    if (property != null) return (T) property.GetValue(obj, null);
                }
            }

            return default(T);
        }

        public static bool SetFieldOrPropertyValue(string fieldName, object obj, object value, bool includeAllBases = false, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic) {
            FieldInfo field = obj.GetType().GetField(fieldName, bindings);
            if (field != null) {
                field.SetValue(obj, value);
                return true;
            }

            PropertyInfo property = obj.GetType().GetProperty(fieldName, bindings);
            if (property != null) {
                property.SetValue(obj, value, null);
                return true;
            }

            if (includeAllBases) {
                foreach (Type type in GetBaseClassesAndInterfaces(obj.GetType())) {
                    field = type.GetField(fieldName, bindings);
                    if (field != null) {
                        field.SetValue(obj, value);
                        return true;
                    }

                    property = type.GetProperty(fieldName, bindings);
                    if (property != null) {
                        property.SetValue(obj, value, null);
                        return true;
                    }
                }
            }
            return false;
        }

        public static IEnumerable<Type> GetBaseClassesAndInterfaces(this Type type, bool includeSelf = false) {
            List<Type> allTypes = new List<Type>();

            if (includeSelf) allTypes.Add(type);

            if (type.BaseType == typeof(object)) {
                allTypes.AddRange(type.GetInterfaces());
            } else {
                allTypes.AddRange(
                        Enumerable
                        .Repeat(type.BaseType, 1)
                        .Concat(type.GetInterfaces())
                        .Concat(type.BaseType.GetBaseClassesAndInterfaces())
                        .Distinct());
            }

            return allTypes;
        }

        public static FieldInfo[] GetFields(this object obj, bool includeAllBases = false, BindingFlags bindings = BindingFlags.GetProperty | BindingFlags.GetField | BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static) {
            List<FieldInfo> fields = new List<FieldInfo>(obj.GetType().GetFields(bindings));

            if (includeAllBases) {
                foreach (Type type in GetBaseClassesAndInterfaces(obj.GetType())) {
                    fields.AddRange(type.GetFields(bindings));
                }
            }
            return fields.ToArray();
        }

        public static FieldInfo[] GetFields(this object obj, Type type, bool includeAllBases = false, BindingFlags bindings = BindingFlags.GetProperty | BindingFlags.GetField | BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static) {
            FieldInfo[] fields = GetFields(obj, includeAllBases, bindings);

            List<FieldInfo> filterFields = new List<FieldInfo>(fields.Length);

            foreach (var field in filterFields) {
                if (field.GetType() == type) {
                    filterFields.Add(field);
                }
            }
            return filterFields.ToArray();
        }

        public static EventInfo[] GetEvents(this object obj, bool includeAllBases = false, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly) {
            List<EventInfo> events = new List<EventInfo>(obj.GetType().GetEvents(bindings));

            if (includeAllBases) {
                foreach (Type type in GetBaseClassesAndInterfaces(obj.GetType())) {
                    events.AddRange(type.GetEvents(bindings));
                }
            }
            return events.ToArray();
        }

        public static MethodInfo[] GetMethods(this object obj, bool includeAllBases = false, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly) {
            List<MethodInfo> methods = new List<MethodInfo>(obj.GetType().GetMethods(bindings));

            if (includeAllBases) {
                foreach (Type type in GetBaseClassesAndInterfaces(obj.GetType())) {
                    methods.AddRange(type.GetMethods(bindings));
                }
            }
            return methods.ToArray();
        }

        public static MethodInfo[] GetMethods(this object obj, Type type, bool includeAllBases = false, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly) {
            MethodInfo[] methods = GetMethods(obj, includeAllBases, bindings);

            List<MethodInfo> filterMethods = new List<MethodInfo>(methods.Length);

            foreach (var method in methods) {
                ParameterInfo[] parameters = method.GetParameters();
                if (parameters.Length > 0 && parameters[0].ParameterType == type) {
                    filterMethods.Add(method);
                }
            }
            return filterMethods.ToArray();
        }

        public static MethodInfo[] GetMethods(this object obj, Type type, string namePrefix, bool includeAllBases = false, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly) {
            MethodInfo[] methods = GetMethods(obj, includeAllBases, bindings);

            List<MethodInfo> filterMethods = new List<MethodInfo>(methods.Length);

            foreach (var method in methods) {
                bool samePrefix = true;
                if (namePrefix != null) {
                    int len = namePrefix.Length;
                    string methodName = method.Name;
                    for (int i = 0; i < len; i++) {
                        if (methodName[i] != namePrefix[i]) {
                            samePrefix = false;
                            break;
                        }
                    }
                }
                ParameterInfo[] parameters = method.GetParameters();
                if ((type == null || (parameters.Length > 0 && parameters[0].ParameterType == type)) && samePrefix) {
                    filterMethods.Add(method);
                }
            }
            return filterMethods.ToArray();
        }
    }
}
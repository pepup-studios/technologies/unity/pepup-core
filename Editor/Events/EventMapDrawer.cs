﻿using System.Linq;
using System.Reflection;
using PepUp.Editor;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace PepUp.Events {
    [CustomPropertyDrawer(typeof(EventMap), true)]
    public class EventMapDrawer : PropertyDrawer {

        protected GUIStyle mainStyle;
        protected SerializedProperty eventList;
        protected ReorderableList reorderableList;

        private int _elementCount = 4;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            //property.serializedObject.Update();

            if (reorderableList == null) {
                Init(position, property);
            }
            position.y += 4;
            reorderableList.DoList(position);

            property.serializedObject.ApplyModifiedProperties();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            float height = 69;
            if (reorderableList != null && eventList.arraySize > 0) {
                height = 48;
                height += mainStyle.GetElementHeight() * _elementCount * eventList.arraySize;
            }
            return height;
        }

        protected void Init(Rect position, SerializedProperty property) {
            eventList = property.FindPropertyRelative("list");
            mainStyle = new GUIStyle {
                fontSize = 15
            };
            mainStyle.SetLineSpace(2);
            mainStyle.SetLineHeight(18);

            CreateReorderableList(eventList);
        }

        protected void CreateReorderableList(SerializedProperty property) {
            reorderableList = new ReorderableList(property.serializedObject, property,
                true, true, true, true);

            reorderableList.drawHeaderCallback += DrawHeader;
            reorderableList.drawElementCallback += DrawElement;
            reorderableList.elementHeightCallback += SetElementHeight;
        }

        protected void DrawHeader(Rect rect) {
            EditorGUI.LabelField(rect, "Events Map");
        }

        protected void DrawElement(Rect rect, int index, bool isActive, bool isFocused) {
            SerializedProperty property = eventList.GetArrayElementAtIndex(index);
            SerializedProperty speakerParent = property.FindPropertyRelative("speakerParent");
            SerializedProperty speaker = property.FindPropertyRelative("speaker");
            SerializedProperty eventName = property.FindPropertyRelative("eventName");

            float lineHeight = mainStyle.GetLineHeight();
            EditorPropertyLayout.BeginProperty(new Rect(rect.x, rect.y, rect.width, lineHeight), mainStyle);

            GUIStyle labelStyle = new GUIStyle(mainStyle) { fontSize = 11 };
            labelStyle.SetLineHeight(15);
            labelStyle.fontStyle = FontStyle.Bold;

            float lineSpace = 2;
            EditorPropertyLayout.Label(new Rect(rect.x, rect.y + lineSpace, rect.width, lineHeight), "Event", labelStyle);
            float speakerWidth = rect.width / 2.65f;
            float yPos = rect.y + mainStyle.GetElementHeight() + lineSpace;
            speakerParent.ShowField(0, new Rect(rect.x, yPos, speakerWidth, lineHeight), new GUIContent("", "Event Object"));

            float width = rect.width / 1.66f;
            float xPos = rect.x + speakerWidth + 7;
            Rect newRect = new Rect(xPos, yPos, width, lineHeight);

            if (speakerParent.objectReferenceValue != null) {
                GenericObject<MemberInfo>[] genericObjects = speakerParent.objectReferenceValue.GetGenericObjectEvents();

                if (genericObjects == null || genericObjects.Length == 0) {
                    eventName.stringValue = null;
                    EditorPropertyLayout.HelpBox(newRect, "Cannot find events in current object", MessageType.Warning);
                } else {
                    string[] names = genericObjects.SelectMany((x) => x.paths).ToArray();
                    int eventIndex = GetIndexByValue(names, eventName.stringValue);
                    eventIndex = EditorPropertyLayout.Popup(eventIndex, newRect, names);
                    string[] subNames = names[eventIndex].Split('/');
                    int actualIndex = GetIndexFromSubArrayByIndex(eventIndex, genericObjects, names);
                    speaker.objectReferenceValue = genericObjects[actualIndex].obj;
                    if (subNames.Length > 1) {
                        eventName.stringValue = subNames[1];
                    } else {
                        eventName.stringValue = subNames[0];
                    }
                }
            } else {
                speaker.objectReferenceValue = null;
                eventName.stringValue = null;
                string[] names = { "Empty" };
                EditorPropertyLayout.Popup(0, newRect, names);
            }

            SerializedProperty listenerParent = property.FindPropertyRelative("listenerParent");
            SerializedProperty listener = property.FindPropertyRelative("listener");
            SerializedProperty eventHandler = property.FindPropertyRelative("eventHandler");

            yPos += mainStyle.GetElementHeight();
            EditorPropertyLayout.Label(new Rect(rect.x, yPos, rect.width, lineHeight), "Event Handler", labelStyle);
            yPos += mainStyle.GetElementHeight();
            listenerParent.ShowField(0, new Rect(rect.x, yPos, speakerWidth, lineHeight), new GUIContent("", "Target Object"));

            newRect = new Rect(xPos, yPos, width, lineHeight);

            if (listenerParent.objectReferenceValue != null) {
                GenericObject<MethodInfo>[] genericObjects = listenerParent.objectReferenceValue.GetGenericObjectMethods(typeof(object[]));

                if (genericObjects == null || genericObjects.Length == 0) {
                    eventHandler.stringValue = null;
                    EditorPropertyLayout.HelpBox(newRect, "Cannot find method in current object", MessageType.Warning);
                } else {
                    string[] names = genericObjects.SelectMany((x) => x.paths).ToArray();
                    int methodIndex = GetIndexByValue(names, eventHandler.stringValue);
                    methodIndex = EditorPropertyLayout.Popup(methodIndex, newRect, names);
                    string[] subNames = names[methodIndex].Split('/');
                    int actualIndex = GetIndexFromSubArrayByIndex(methodIndex, genericObjects, names);
                    listener.objectReferenceValue = genericObjects[actualIndex].obj;
                    if (subNames.Length > 1) {
                        eventHandler.stringValue = subNames[1];
                    } else {
                        eventHandler.stringValue = subNames[0];
                    }
                }
            } else {
                listener.objectReferenceValue = null;
                eventHandler.stringValue = null;
                string[] names = { "Empty" };
                EditorPropertyLayout.Popup(0, newRect, names);
            }

            SerializedProperty dataType = property.FindPropertyRelative("dataType");

            float dataTypeWidth = rect.width / 2.65f;
            yPos += mainStyle.GetElementHeight();
            dataType.ShowField(0, new Rect(rect.x, yPos, dataTypeWidth, lineHeight), new GUIContent("", "Data Type"));

            xPos = rect.x + dataTypeWidth + 7;
            newRect = new Rect(xPos, yPos, width, lineHeight);

            int dataTypeIndex = dataType.enumValueIndex;
            if (dataTypeIndex != DataType.RuntimeData.GetHashCode()) {
                GUIContent valueContent = new GUIContent("", "Paramter");
                switch ((DataType) dataTypeIndex) {
                    case DataType.String:
                        SerializedProperty value = property.FindPropertyRelative("stringValue");
                        value.ShowField(lineHeight, newRect, valueContent);
                        break;
                    case DataType.Int:
                        value = property.FindPropertyRelative("intValue");
                        value.ShowField(lineHeight, newRect, valueContent);
                        break;
                    case DataType.Float:
                        value = property.FindPropertyRelative("floatValue");
                        value.ShowField(lineHeight, newRect, valueContent);
                        break;
                    case DataType.Bool:
                        value = property.FindPropertyRelative("boolValue");
                        value.ShowField(lineHeight, newRect, valueContent);
                        break;
                    case DataType.Object:
                        value = property.FindPropertyRelative("objValue");
                        value.ShowField(lineHeight, newRect, valueContent);
                        break;
                }
            } else {
                EditorPropertyLayout.HelpBox(newRect, "Cannot override parameter", MessageType.Info);
            }

            _elementCount = 5;

            //if (index < eventList.arraySize - 1) {
            //    EditorPropertyLayout.GuideLine(Color.grey, 1, new Rect(-13, 5, 13, -15));
            //}

            EditorPropertyLayout.EndProperty();
        }

        protected float SetElementHeight(int index) {
            return mainStyle.GetElementHeight() * _elementCount;
        }

        protected virtual int GetIndexFromSubArrayByIndex<T>(int index, GenericObject<T>[] genericObjects, string[] names) {
            int fieldCount = 0;
            for (int i = 0; i < genericObjects.Length; i++) {
                fieldCount += genericObjects[i].fields.Length;
                if (index < fieldCount) {
                    return i;
                }
            }
            return 0;
        }

        protected virtual int GetIndexByValue(string[] names, string value) {
            for (int i = 0; i < names.Length; i++) {
                string name = names[i];
                if (value == null || name.Contains(value)) {
                    return i;
                }
            }
            return 0;
        }
    }
}

﻿using UnityEditor;
using UnityEngine;

namespace PepUp.Events {
    [CustomPropertyDrawer(typeof(EventActionObject), true)]
    public class EventActionObjectDrawer : PropertyDrawer {

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            SerializedProperty eventObject = property.FindPropertyRelative("eventObject");
            EditorGUI.PropertyField(position, eventObject, new GUIContent(property.displayName));
        }
    }
}
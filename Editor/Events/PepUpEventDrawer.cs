﻿//using UnityEngine;
//using UnityEditor;
//using UnityEditorInternal;
//using PepUp.Editor;
//using System.Reflection;
//using System.Linq;

//namespace PepUp.Events {
//    [CustomPropertyDrawer(typeof(PepUpEvent), true)]
//    public class PepUpEventDrawer : PropertyDrawer {

//        protected GUIStyle mainStyle;
//        protected SerializedProperty eventList;
//        protected ReorderableList reorderableList;

//        private string _propertyName;
//        private int _elementCount = 1;

//        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
//            //property.serializedObject.Update();

//            _propertyName = property.displayName;

//            if (reorderableList == null) {
//                Init(position, property);
//            }

//            position.y += 4;
//            reorderableList.DoList(position);

//            property.serializedObject.ApplyModifiedProperties();
//        }

//        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
//            float height = 69;
//            if (reorderableList != null && eventList.arraySize > 0) {
//                height = 48;
//                height += mainStyle.GetElementHeight() * _elementCount * eventList.arraySize;
//            }
//            return height;
//        }

//        protected void Init(Rect position, SerializedProperty property) {
//            mainStyle = new GUIStyle {
//                fontSize = 15
//            };
//            mainStyle.SetLineSpace(2);
//            mainStyle.SetLineHeight(18);

//            eventList = property.FindPropertyRelative("list");
//            CreateReorderableList(eventList);
//        }

//        protected void CreateReorderableList(SerializedProperty property) {
//            reorderableList = new ReorderableList(property.serializedObject, property,
//                true, true, true, true);

//            reorderableList.drawHeaderCallback += DrawHeader;
//            reorderableList.drawElementCallback += DrawElement;
//            reorderableList.elementHeightCallback += SetElementHeight;
//        }

//        protected void DrawHeader(Rect rect) {
//            EditorGUI.LabelField(rect, _propertyName);
//        }

//        protected virtual void DrawElement(Rect rect, int index, bool isActive, bool isFocused) {
//            DrawElement(rect, eventList.GetArrayElementAtIndex(index));
//        }

//        protected virtual void DrawElement(Rect rect, SerializedProperty property) {
//            SerializedProperty listenerParent = property.FindPropertyRelative("listenerParent");
//            SerializedProperty listener = property.FindPropertyRelative("listener");
//            SerializedProperty eventHandler = property.FindPropertyRelative("eventHandler");

//            EditorPropertyLayout.BeginProperty(new Rect(rect.x, rect.y, rect.width,
//                mainStyle.GetLineHeight()), mainStyle);

//            GUIStyle labelStyle = new GUIStyle(mainStyle) { fontSize = 11 };
//            labelStyle.SetLineHeight(15);
//            labelStyle.fontStyle = FontStyle.Bold;

//            EditorPropertyLayout.Space(2);

//            listenerParent.ShowField(0, new GUIContent("", "Target Object"), false, new Rect(0, 0, -235, 0));

//            if (listenerParent.objectReferenceValue != null) {
//                GenericObject<MethodInfo>[] genericObjects = listenerParent.objectReferenceValue.GetGenericObjectMethods(typeof(object[]));

//                if (genericObjects == null || genericObjects.Length == 0) {
//                    eventHandler.stringValue = null;
//                    EditorPropertyLayout.HelpBox("Cannot find method in current object", MessageType.Warning, new Rect(150, 0, -150, 0));
//                } else {
//                    string[] names = genericObjects.SelectMany((x) => x.paths).ToArray();
//                    int methodIndex = GetIndexByValue(names, eventHandler.stringValue);
//                    methodIndex = EditorPropertyLayout.Popup(methodIndex, names, new Rect(150, 0, -150, 0));
//                    string[] subNames = names[methodIndex].Split('/');
//                    int actualIndex = GetIndexFromSubArrayByIndex(methodIndex, genericObjects, names);
//                    listener.objectReferenceValue = genericObjects[actualIndex].obj;
//                    if (subNames.Length > 1) {
//                        eventHandler.stringValue = subNames[1];
//                    } else {
//                        eventHandler.stringValue = subNames[0];
//                    }
//                }
//            } else {
//                listener.objectReferenceValue = null;
//                eventHandler.stringValue = null;
//                string[] names = { "Empty" };
//                EditorPropertyLayout.Popup(0, names, new Rect(150, 0, -150, 0));
//            }

//            EditorPropertyLayout.EndProperty();
//        }

//        protected float SetElementHeight(int index) {
//            return mainStyle.GetElementHeight() * _elementCount;
//        }

//        protected virtual int GetIndexFromSubArrayByIndex<T>(int index, GenericObject<T>[] genericObjects, string[] names) {
//            int fieldCount = 0;
//            for (int i = 0; i < genericObjects.Length; i++) {
//                fieldCount += genericObjects[i].fields.Length;
//                if (index < fieldCount) {
//                    return i;
//                }
//            }
//            return 0;
//        }

//        protected virtual int GetIndexByValue(string[] names, string value) {
//            for (int i = 0; i < names.Length; i++) {
//                string name = names[i];
//                if (value == null || name.Contains(value)) {
//                    return i;
//                }
//            }
//            return 0;
//        }
//    }
//}
﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using PepUp.Editor;
using System.Reflection;
using System.Linq;

namespace PepUp.Events {
    [CustomPropertyDrawer(typeof(RegisterEvent), true)]
    public class RegisterEventDrawer : PropertyDrawer {

        protected GUIStyle mainStyle;
        protected SerializedProperty eventList;
        protected ReorderableList reorderableList;

        private string _propertyName;
        private int _elementCount = 2;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            //property.serializedObject.Update();

            _propertyName = property.displayName;

            if (reorderableList == null) {
                Init(position, property);
            }

            position.y += 4;
            reorderableList.DoList(position);

            property.serializedObject.ApplyModifiedProperties();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            float height = 69;
            if (reorderableList != null && eventList.arraySize > 0) {
                height = 48;
                height += mainStyle.GetElementHeight() * _elementCount * eventList.arraySize;
            }
            return height;
        }

        protected void Init(Rect position, SerializedProperty property) {
            mainStyle = new GUIStyle {
                fontSize = 15
            };
            mainStyle.SetLineSpace(2);
            mainStyle.SetLineHeight(18);

            eventList = property.FindPropertyRelative("list");
            CreateReorderableList(eventList);
        }

        protected void CreateReorderableList(SerializedProperty property) {
            reorderableList = new ReorderableList(property.serializedObject, property,
                true, true, true, true);

            reorderableList.drawHeaderCallback += DrawHeader;
            reorderableList.drawElementCallback += DrawElement;
            reorderableList.elementHeightCallback += SetElementHeight;
        }

        protected void DrawHeader(Rect rect) {
            EditorGUI.LabelField(rect, _propertyName);
        }

        protected virtual void DrawElement(Rect rect, int index, bool isActive, bool isFocused) {
            DrawElement(rect, eventList.GetArrayElementAtIndex(index));
        }

        protected virtual void DrawElement(Rect rect, SerializedProperty property) {
            SerializedProperty speakerParent = property.FindPropertyRelative("speakerParent");
            SerializedProperty speaker = property.FindPropertyRelative("speaker");
            SerializedProperty eventName = property.FindPropertyRelative("eventName");
            SerializedProperty selectionIndex = property.FindPropertyRelative("selectionIndex");

            float lineHeight = mainStyle.GetLineHeight();
            EditorPropertyLayout.BeginProperty(new Rect(rect.x, rect.y, rect.width, lineHeight), mainStyle);

            GUIStyle labelStyle = new GUIStyle(mainStyle) { fontSize = 11 };
            labelStyle.SetLineHeight(15);
            labelStyle.fontStyle = FontStyle.Bold;

            float lineSpace = 2;
            float speakerWidth = rect.width / 2.65f;
            speakerParent.ShowField(0, new Rect(rect.x, rect.y + lineSpace, speakerWidth, lineHeight), new GUIContent("", "Event Object")); //w: -235

            float width = rect.width / 1.66f;
            float xPos = rect.x + speakerWidth + 7;
            Rect newRect = new Rect(xPos, rect.y + lineSpace, width, lineHeight);

            if (speakerParent.objectReferenceValue != null) {
                GenericObject<MemberInfo>[] genericObjects = speakerParent.objectReferenceValue.GetGenericObjectEvents();

                if (genericObjects == null || genericObjects.Length == 0) {
                    eventName.stringValue = null;
                    EditorPropertyLayout.HelpBox(newRect, "Cannot find events in current object", MessageType.Warning);
                } else {
                    string[] names = genericObjects.SelectMany((x) => x.paths).ToArray();
                    if (!HasSameNames(names, eventName.stringValue) || selectionIndex.intValue >= names.Length) {
                        selectionIndex.intValue = GetIndexByValue(names, eventName.stringValue);
                    }
                    selectionIndex.intValue = EditorPropertyLayout.Popup(selectionIndex.intValue, newRect, names);//w: -150
                    string[] subNames = names[selectionIndex.intValue].Split('/');
                    int actualIndex = GetIndexFromSubArrayByIndex(selectionIndex.intValue, genericObjects, names);
                    speaker.objectReferenceValue = genericObjects[actualIndex].obj;
                    if (subNames.Length > 1) {
                        eventName.stringValue = subNames[1];
                    } else {
                        eventName.stringValue = subNames[0];
                    }
                }
            } else {
                speaker.objectReferenceValue = null;
                eventName.stringValue = null;
                string[] names = { "Empty" };
                EditorPropertyLayout.Popup(0, newRect, names);//w: -150
            }

            SerializedProperty dataType = property.FindPropertyRelative("dataType");

            float dataTypeWidth = rect.width / 2.65f;
            dataType.ShowField(0, new Rect(rect.x, rect.y + mainStyle.GetElementHeight() + lineSpace, dataTypeWidth, lineHeight), new GUIContent("", "Data Type"));

            xPos = rect.x + dataTypeWidth + 7;
            newRect = new Rect(xPos, rect.y + mainStyle.GetElementHeight() + lineSpace, width, lineHeight);

            int dataTypeIndex = dataType.enumValueIndex;
            if (dataTypeIndex != DataType.RuntimeData.GetHashCode()) {
                GUIContent valueContent = new GUIContent("", "Paramter");
                switch ((DataType) dataTypeIndex) {
                    case DataType.String:
                        SerializedProperty value = property.FindPropertyRelative("stringValue");
                        value.ShowField(lineHeight, newRect, valueContent);
                        break;
                    case DataType.Int:
                        value = property.FindPropertyRelative("intValue");
                        value.ShowField(lineHeight, newRect, valueContent);
                        break;
                    case DataType.Float:
                        value = property.FindPropertyRelative("floatValue");
                        value.ShowField(lineHeight, newRect, valueContent);
                        break;
                    case DataType.Bool:
                        value = property.FindPropertyRelative("boolValue");
                        value.ShowField(lineHeight, newRect, valueContent);
                        break;
                    case DataType.Object:
                        value = property.FindPropertyRelative("objValue");
                        value.ShowField(lineHeight, newRect, valueContent);
                        break;
                }
            } else {
                EditorPropertyLayout.HelpBox(newRect, "Cannot override parameter", MessageType.Info);
            }

            EditorPropertyLayout.EndProperty();
        }

        protected float SetElementHeight(int index) {
            return mainStyle.GetElementHeight() * _elementCount;
        }

        protected virtual int GetIndexFromSubArrayByIndex<T>(int index, GenericObject<T>[] genericObjects, string[] names) {
            int fieldCount = 0;
            for (int i = 0; i < genericObjects.Length; i++) {
                fieldCount += genericObjects[i].fields.Length;
                if (index < fieldCount) {
                    return i;
                }
            }
            return 0;
        }

        protected virtual int GetIndexByValue(string[] names, string value) {
            for (int i = 0; i < names.Length; i++) {
                string name = names[i];
                if (value == null || name.Contains(value)) {
                    return i;
                }
            }
            return 0;
        }

        protected virtual bool HasSameNames(string[] names, string value) {
            int counter = 0;
            for (int i = 0; i < names.Length; i++) {
                string name = names[i];
                if (value == null || name.Contains(value)) {
                    if (counter >= 1) {
                        return true;
                    }
                    counter++;
                }
            }
            return false;
        }
    }
}

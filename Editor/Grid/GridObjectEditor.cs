﻿using UE = UnityEditor;

namespace PepUp.Grid {
    [UE.CustomEditor(typeof(GridObject), true)]
    public class GridObjectEditor : UE.Editor {

        public GridObject gridObject;

        void OnEnable() {
            gridObject = target as GridObject;
        }

        private void OnSceneGUI () {
            if (UnityEngine.Application.isPlaying) return;
            if (gridObject.gridSnap && !gridObject.GridBlock && gridObject.gridManager != null && gridObject.gridManager.gameObject.activeInHierarchy &&
                gridObject.gridManager.enabled)
                UpdatePosition();
        }

        private void UpdatePosition() {
            gridObject.transform.position = gridObject.cell.position;
        }
    }
}
﻿using UnityEngine;

namespace PepUp.Editor {
	public struct Layout {

        public int totalElements;
	    public GUIStyle style;

        public Layout(int totalElements, GUIStyle style) {
            this.totalElements = totalElements;
            this.style = style;
        }
    }
}

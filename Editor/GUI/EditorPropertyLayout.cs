﻿using UnityEditor;
using UnityEngine;

namespace PepUp.Editor {
    public static class EditorPropertyLayout {

        private static Rect _currRect;
        private static GUIStyle _style;

        #region BeginProperty

        public static void BeginProperty(float x, float y, float width, float height) {
            _currRect = new Rect(x, y, width, height);
            _style = new GUIStyle();
        }

        public static void BeginProperty(Rect rect) {
            _currRect = new Rect(rect);
            _style = new GUIStyle();
        }

        public static void BeginProperty(Rect rect, GUIStyle style) {
            _currRect = new Rect(rect);
            _style = style;
        }

        #endregion

        #region HeaderGroup

        public static bool BeginFoldoutHeaderGroup(GUIContent content, bool foldout = false, System.Action<Rect> menuAction = null) {
            bool flag = EditorGUI.BeginFoldoutHeaderGroup(_currRect, foldout, content, null, menuAction);
            _currRect.y += _style.GetElementHeight();
            return flag;
        }

        public static void EndFoldoutHeaderGroup() {
            EditorGUI.EndFoldoutHeaderGroup();
        }

        #endregion

        #region ShowField

        public static void ShowField(this SerializedProperty property, bool includeChild = false, Rect rectOffset = new Rect()) {
            ShowField(property, _style.GetElementHeight(), includeChild, rectOffset);
        }

        public static void ShowField(this SerializedProperty property, GUIContent content, Rect rectOffset = new Rect(), bool includeChild = false) {
            ShowField(property, _style.GetElementHeight(), content, includeChild, rectOffset);
        }

        public static void ShowField(this SerializedProperty property, GUIContent content, GUIStyle style, bool includeChild = false, Rect rectOffset = new Rect()) {
            ShowField(property, style.GetElementHeight(), content, includeChild, rectOffset);
        }

        public static void ShowField(this SerializedProperty property, GUIStyle style, bool includeChild = false, Rect rectOffset = new Rect()) {
            ShowField(property, style.GetElementHeight(), includeChild, rectOffset);
        }

        public static void ShowField(this SerializedProperty property, float lineHeight, Rect overrideRect, GUIContent content) {
            EditorGUI.PropertyField(overrideRect, property, content);
            _currRect.y = overrideRect.y += lineHeight;
        }

        public static void ShowField(this SerializedProperty property, float lineHeight, bool includeChild = false, Rect rectOffset = new Rect()) {
            Rect newRect = OffsetRect(_currRect, rectOffset);
            EditorGUI.PropertyField(newRect, property, includeChild);
            _currRect.y = newRect.y += lineHeight;
        }

        public static void ShowField(this SerializedProperty property, float lineHeight, GUIContent content, bool includeChild = false, Rect rectOffset = new Rect()) {
            Rect newRect = OffsetRect(_currRect, rectOffset);
            EditorGUI.PropertyField(newRect, property, content, includeChild);
            _currRect.y = newRect.y += lineHeight;
        }

        #endregion

        #region Popup

        public static int Popup(int index, Rect overrideRect, string[] displayOptions, float lineHeight = 18) {
            index = EditorGUI.Popup(overrideRect, index, displayOptions);
            _currRect.y = overrideRect.y += lineHeight;
            return index;
        }

        public static int Popup(int index, string[] displayOptions, Rect rectOffset = new Rect(), float lineHeight = 18) {
            Rect newRect = OffsetRect(_currRect, rectOffset);
            index = EditorGUI.Popup(newRect, index, displayOptions);
            _currRect.y = newRect.y += lineHeight;
            return index;
        }

        public static int Popup(int index, string[] displayOptions, GUIStyle style, Rect rectOffset = new Rect()) {
            Rect newRect = OffsetRect(_currRect, rectOffset);
            index = EditorGUI.Popup(newRect, index, displayOptions, style);
            _currRect.y = newRect.y += style.GetElementHeight();
            return index;
        }

        public static int Popup(GUIContent content, int index, GUIContent[] displayOptions, float lineHeight = 18, Rect rectOffset = new Rect()) {
            Rect newRect = OffsetRect(_currRect, rectOffset);
            index = EditorGUI.Popup(newRect, content, index, displayOptions);
            _currRect.y = newRect.y += lineHeight;
            return index;
        }

        #endregion

        #region HelpBox

        public static void HelpBox(Rect overrideRect, string message, MessageType messageType, float lineHeight = 18) {
            EditorGUI.HelpBox(overrideRect, message, messageType);
            _currRect.y = overrideRect.y += lineHeight;
        }

        public static void HelpBox(string message, MessageType messageType, Rect rectOffset = new Rect(), float lineHeight = 18) {
            Rect newRect = OffsetRect(_currRect, rectOffset);
            EditorGUI.HelpBox(newRect, message, messageType);
            _currRect.y = newRect.y += lineHeight;
        }

        #endregion

        #region Space

        public static void Space(float space = 10) {
            _currRect.y += space;
        }

        #endregion

        #region Label

        public static void Label(string label, float lineHeight = 18, Rect rectOffset = new Rect()) {
            Rect newRect = OffsetRect(_currRect, rectOffset);
            EditorGUI.LabelField(newRect, label);
            _currRect.y = newRect.y += lineHeight;
        }

        public static void Label(string label, GUIStyle style, Rect rectOffset = new Rect()) {
            Rect newRect = OffsetRect(_currRect, rectOffset);
            EditorGUI.LabelField(newRect, label, style);
            _currRect.y = newRect.y += style.GetElementHeight();
        }


        public static void Label(Rect overrideRect, string label, GUIStyle style) {
            EditorGUI.LabelField(overrideRect, label, style);
            _currRect.y = overrideRect.y += style.GetElementHeight();
        }

        public static void Label(GUIContent content, GUIStyle style, Rect rectOffset = new Rect()) {
            Rect newRect = OffsetRect(_currRect, rectOffset);
            EditorGUI.LabelField(newRect, content, style);
            _currRect.y = newRect.y += style.GetElementHeight();
        }

        #endregion

        #region Box

        public static void Box(GUIContent content, int lineHeight = 15, Rect rectOffset = new Rect()) {
            Rect newRect = OffsetRect(_currRect, rectOffset);
            GUI.Box(newRect, content);
            _currRect.y = newRect.y += lineHeight;
        }

        public static void Box(GUIContent content, GUIStyle style, Rect rectOffset = new Rect()) {
            Rect newRect = OffsetRect(_currRect, rectOffset);
            GUI.Box(newRect, content, style);
            _currRect.y = newRect.y += style.GetElementHeight();
        }

        #endregion

        public static void GuideLine(Color color, int lineHeight = 1, Rect rectOffset = new Rect()) {
            Rect newRect = OffsetRect(_currRect, rectOffset);
            EditorGUI.DrawRect(newRect, color);
            _currRect.y = newRect.y += lineHeight;
        }

        public static void EndProperty() {
            _currRect = new Rect();
        }

        private static Rect OffsetRect(Rect rect1, Rect rect2) {
            rect1.x += rect2.x;
            rect1.y += rect2.y;
            rect1.width += rect2.width;
            rect1.height += rect2.height;
            return rect1;
        }
    }
}
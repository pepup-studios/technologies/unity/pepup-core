﻿using UnityEditor;

namespace PepUp.Pathfinding {
    [CustomEditor(typeof(Node), true), CanEditMultipleObjects]
    public class NodeEditor : BaseNodeEditor<Node, NodeData> {

    }
}
﻿using UnityEngine;

namespace PepUp.Pathfinding {
    public abstract class BaseNodeEditor<T, J> : UnityEditor.Editor where T : BaseNode<J> where J : NodeData {

        //private static SerializedProperty _data;

        //protected void OnEnable() {
        //    _data = serializedObject.FindProperty("data");
        //}

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            T node = (T) target;

            GUILayout.Space(10);
            if (GUILayout.Button("Update Data")) {
                node.UpdateNodeData();
            }
        }
    }
}
﻿using UnityEditor;
using UnityEngine;

namespace PepUp.Pathfinding {
    [CustomEditor(typeof(BasePathBuilder<Path, Node, NodeData>), true), CanEditMultipleObjects]
    public class BasePathBuilderEditor<J, F, T> : UnityEditor.Editor
        where J : GenericPath<T> where T : NodeData where F : BaseNode<T> {

        protected BasePathBuilder<J, F, T> pathBuilder;

        protected GUIStyle btnStyleNormal;
        protected GUIStyle btnStylePressed;

        protected ButtonStates createNodeState;
        protected ButtonStates linkNodeState;

        protected bool buttonPressed;

        protected Vector3 startworldPosition;

        public enum ButtonStates { Normal, Pressed };

        private void InitStyle() {
            btnStyleNormal = "Button";
            btnStylePressed = new GUIStyle(btnStyleNormal);
            btnStylePressed.normal.background = btnStylePressed.active.background;
        }

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            if (pathBuilder == null) pathBuilder = (BasePathBuilder<J, F, T>) target;
            if (btnStyleNormal == null || btnStylePressed == null) InitStyle();

            GUILayout.Space(10);
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Load Path", btnStyleNormal)) {
                pathBuilder.LoadChildNodes();

                SceneView.RepaintAll();
                //SavePath();
            }
            if (GUILayout.Button("Unload Path", btnStyleNormal)) {
                pathBuilder.UnloadChildNodes();

                SceneView.RepaintAll();
                //SavePath();
            }

            GUILayout.EndHorizontal();
            GUILayout.Space(5);
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Connect Path Node(s)", btnStyleNormal)) {
                pathBuilder.ConnectChildNodes();

                SceneView.RepaintAll();
                //SavePath();
            }
            if (GUILayout.Button("Disconnect Path Node(s)", btnStyleNormal)) {
                pathBuilder.DisconnectChildNodes();

                SceneView.RepaintAll();
                //SavePath();
            }

            GUILayout.EndHorizontal();
            GUILayout.Space(5);
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Create Node", GetStyle(createNodeState))) {
                if (linkNodeState == ButtonStates.Pressed) {
                    linkNodeState = ButtonStates.Normal;
                    createNodeState = ButtonStates.Pressed;
                } else {
                    createNodeState = ToggleBtnState(createNodeState);
                }

                SceneView.RepaintAll();
                //SavePath();
            }
            if (GUILayout.Button("Link Node", GetStyle(linkNodeState))) {
                if (createNodeState == ButtonStates.Pressed) {
                    createNodeState = ButtonStates.Normal;
                    linkNodeState = ButtonStates.Pressed;
                } else {
                    linkNodeState = ToggleBtnState(linkNodeState);
                }

                Selection.activeGameObject = pathBuilder.gameObject;

                SceneView.RepaintAll();
                //SavePath();
            }

            GUILayout.EndHorizontal();
            GUILayout.Space(5);
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Reload Node")) {
                pathBuilder.path.Clear();

                SceneView.RepaintAll();
                //SavePath();
            }

            GUILayout.EndHorizontal();

            if (createNodeState == ButtonStates.Pressed || linkNodeState == ButtonStates.Pressed) {
                ActiveEditorTracker.sharedTracker.isLocked = buttonPressed = true;
            } else {
                ActiveEditorTracker.sharedTracker.isLocked = buttonPressed = false;
            }
        }

        //protected bool SavePath() {
        //    return SavePath(pathBuilder.path);
        //}

        //protected bool SavePath(Path path) {
        //    if (path != null) {
        //        EditorUtility.SetDirty(path);
        //        return true;
        //    }
        //    return false;
        //}

        protected GUIStyle GetStyle(ButtonStates state) {
            switch (state) {
                case ButtonStates.Pressed:
                    return btnStylePressed;
                default:
                    return btnStyleNormal;
            }
        }

        protected ButtonStates ToggleBtnState(ButtonStates state) {
            return state == ButtonStates.Normal ? ButtonStates.Pressed : ButtonStates.Normal;
        }

        void OnDisable() {            
            Tools.hidden = false;
            SceneView.RepaintAll();
        }

        protected void OnSceneGUI() {
            Event currEvent = Event.current;
            SceneView sceneView = SceneView.currentDrawingSceneView;

            Vector3 worldPosition = currEvent.mousePosition;
            worldPosition = CalculateMousePosition(sceneView, worldPosition);

            int activeButton = currEvent.button;
            switch (currEvent.type) {
                case EventType.MouseDown:
                    startworldPosition = worldPosition;
                    if (activeButton == 0) {
                        LeftMouseDown(currEvent, sceneView, worldPosition);
                    }
                    break;
                case EventType.MouseDrag:
                    if (activeButton == 0) {
                        LeftMouseDrag(currEvent, sceneView, worldPosition);
                    }
                    break;
                case EventType.MouseUp:
                    if (activeButton == 0) {
                        LeftMouseUp(currEvent, sceneView, worldPosition);
                    }
                    break;
                case EventType.Layout:
                    if (buttonPressed) {
                        Tools.hidden = true;
                        DefaultHandleControl();
                    }
                    break;
            }
        }

        protected int DefaultHandleControl() {
            int controlID = GUIUtility.GetControlID(GetHashCode(), FocusType.Passive);
            HandleUtility.AddDefaultControl(controlID);
            return controlID;
        }

        protected Vector3 CalculateMousePosition(SceneView sceneView, Vector3 mousePosition) {
            mousePosition.y = sceneView.camera.pixelHeight - mousePosition.y;
            mousePosition = sceneView.camera.ScreenToWorldPoint(mousePosition);
            mousePosition.y = -mousePosition.y;
            return mousePosition;
        }

        protected void LeftMouseDown(Event currEvent, SceneView sceneView, Vector3 worldPosition) {
            Tools.hidden = true;
            if (createNodeState == ButtonStates.Pressed) {
                CreateNode(worldPosition);
            }
            if (linkNodeState == ButtonStates.Pressed) {
                SelectObject(currEvent.mousePosition);
            }
        }

        protected void LeftMouseDrag(Event currEvent, SceneView sceneView, Vector3 worldPosition) {
            if (linkNodeState == ButtonStates.Pressed) {
                GameObject selectedObj = Selection.activeGameObject;
                if (selectedObj == null) return;
                DrawLine(selectedObj.transform.position, sceneView.camera.ScreenToWorldPoint(currEvent.mousePosition));
            }
        }

        protected void LeftMouseUp(Event currEvent, SceneView sceneView, Vector3 worldPosition) {
            Tools.hidden = false;
            if (linkNodeState == ButtonStates.Pressed) {
                GameObject selectedObj = Selection.activeGameObject;
                if (currEvent.control) {
                    DelinkNode(sceneView, currEvent.mousePosition, selectedObj);
                } else {
                    LinkNode(sceneView, currEvent.mousePosition, selectedObj);
                }
                SceneView.RepaintAll();
            }
        }

        protected void CreateNode(Vector3 position) {
            Debug.Log("CreateNode At: " + position);
        }

        protected void LinkNode(SceneView sceneView, Vector3 position, GameObject selectedObj) {
            //Debug.Log("LinkNode At: " + position + " selectedObj: " + selectedObj);
            if (selectedObj == null) return;
            F node = GetTargetNode(selectedObj);

            if (node != null) {
                GameObject selectedTarget = HandleUtility.PickGameObject(position, false);
                if (selectedTarget != null) {
                    F targetNode = GetTargetNode(selectedTarget);
                    if (targetNode != null) {
                        if (!IsLinkAdded(node, targetNode) && targetNode != node) {
                            pathBuilder.LinkNode(node, targetNode);
                            //Debug.Log("Linked " + node + " to " + targetNode);
                        }
                    } else {
                        Debug.LogError("Link cannot be connected because target gameobject: " + selectedTarget.name + " is not a node");
                    }
                }
                //else {
                //    Debug.LogWarning("Link needs to be attached to a target node");
                //}
            }
        }

        protected void DelinkNode(SceneView sceneView, Vector3 position, GameObject selectedObj) {
            if (selectedObj == null) return;
            F node = GetTargetNode(selectedObj);

            if (node != null) {
                GameObject selectedTarget = HandleUtility.PickGameObject(position, false);
                if (selectedTarget != null) {
                    F targetNode = GetTargetNode(selectedTarget);
                    if (targetNode != null) {
                        if (IsLinkAdded(node, targetNode, out NodeLink nodeLink) && targetNode != node) {
                            pathBuilder.DelinkNode(node, nodeLink);
                            //Debug.Log("Delinked " + node + " to " + targetNode);
                        }
                    } else {
                        Debug.LogError("Link cannot be disconnected because target gameobject: " + selectedTarget.name + " is not a node");
                    }
                }
                //else {
                //    Debug.LogWarning("Delink needs to be attached to a target node");
                //}
            }
        }

        protected F GetTargetNode(GameObject gameObject) {
            F targetNode = gameObject.GetComponent<F>();
            Transform parent = gameObject.transform.parent;
            if (targetNode == null && parent != null && parent.gameObject != pathBuilder.gameObject) {
                return GetTargetNode(parent.gameObject);
            }
            if (!IsNodeAddedToPath(targetNode)) {
                return null;
            }
            return targetNode;
        }

        protected bool IsLinkAdded(BaseNode<T> node, BaseNode<T> targetNode) {
            return IsLinkAdded(node, targetNode, out _);
        }

        protected bool IsLinkAdded(BaseNode<T> node, BaseNode<T> targetNode, out NodeLink nodeLink) {
            if (pathBuilder != null && pathBuilder.path != null) {
                foreach (var markerLink in node.data.links) {
                    NodeData currNode = pathBuilder.path.GetNode(markerLink.target);
                    if (currNode.position == targetNode.data.position) {
                        nodeLink = markerLink;
                        return true;
                    }
                }
            }
            nodeLink = null;
            return false;
        }

        protected bool IsNodeAddedToPath(BaseNode<T> targetNode) {
            if (pathBuilder != null && pathBuilder.path != null) {
                foreach (var node in pathBuilder.path.nodes) {
                    if (node == targetNode.data) {
                        return true;
                    }
                }
            }
            return false;
        }

        protected void DrawLine(Vector3 from, Vector3 to) {
            //Debug.Log("DrawLine At: " + to + " startPos: " + from);
            //Handles.color = Color.red;
            //Handles.DrawLine(from, from + Vector3.right * 10);
            //HandleUtility.Repaint();
            //SceneView.RepaintAll();
        }

        protected void SelectObject(Vector3 position) {
            Selection.activeGameObject = HandleUtility.PickGameObject(position, false);
            //Debug.Log("SelectObject: " + position + " activeGameObject: " + Selection.activeGameObject);
        }
    }
}
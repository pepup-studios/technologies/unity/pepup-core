﻿using UnityEditor;

namespace PepUp.Pathfinding {
    [CustomEditor(typeof(PathBuilder), true), CanEditMultipleObjects]
    public class PathBuilderEditor : BasePathBuilderEditor<Path, Node, NodeData> {
    }
}
﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using PepUp.Editor;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace PepUp.Analytics {
    public abstract class AnalyticalEventDrawerBase : PropertyDrawer {

        protected GUIStyle mainStyle;
        protected List<Layout> dataLayouts;
        protected SerializedProperty dataList;
        protected ReorderableList reorderableList;

        protected bool showHiddenEvents;
        protected bool showHiddenLogs;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            property.serializedObject.Update();

            if (reorderableList == null) {
                Init(position, property);
            }
            position.y += 5;
            reorderableList.DoList(position);

            property.serializedObject.ApplyModifiedProperties();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            float height = 65;
            if (dataLayouts != null && dataLayouts.Count > 0) {
                height = 48;
                foreach (var dataLayout in dataLayouts) {
                    height += dataLayout.style.GetElementHeight()* dataLayout.totalElements;
                }
            }
            return height;
        }

        protected virtual void Init(Rect position, SerializedProperty property) {
            dataList = property.FindPropertyRelative("data");
            mainStyle = new GUIStyle {
                fontSize = 15
            };
            mainStyle.SetLineSpace(2);
            mainStyle.SetLineHeight(18);

            dataLayouts = new List<Layout>();

            CreateReorderableList(dataList);
        }

        protected virtual void CreateReorderableList(SerializedProperty property) {
            reorderableList = new ReorderableList(property.serializedObject, property,
                true, true, true, true);

            reorderableList.drawHeaderCallback += DrawHeader;
            reorderableList.drawElementCallback += DrawElement;
            reorderableList.onAddDropdownCallback += OnAddDropDown;
            reorderableList.onRemoveCallback += OnRemoveElement;
            reorderableList.elementHeightCallback += SetElementHeight;
        }

        protected virtual float SetElementHeight(int index) {
            if (dataLayouts == null) return mainStyle.GetElementHeight();
            if (index > dataLayouts.Count - 1) {
                AddElementLayout(new Layout(5, new GUIStyle(mainStyle)));
            }
            Layout dataLayout = dataLayouts[index];
            return dataLayout.totalElements * (dataLayout.style.lineHeight + 2);
        }

        protected virtual void AddElementLayout(Layout newLayout) {
            dataLayouts.Add(newLayout);
        }

        protected virtual void DrawHeader(Rect rect) {
            EditorGUI.LabelField(rect, "Analytical Events");
        }

        protected virtual void OnAddDropDown(Rect rect, ReorderableList list) {
            GenericMenu menu = new GenericMenu();
            AddMenuItems(menu);
            menu.DropDown(rect);
        }

        protected abstract void OnRemoveElement(ReorderableList list);

        protected virtual void DrawElement(Rect rect, int index, bool isActive, bool isFocused) {
            SerializedProperty property = dataList.GetArrayElementAtIndex(index);

            EditorPropertyLayout.BeginProperty(new Rect(rect.x, rect.y, rect.width,
                mainStyle.GetLineHeight()), mainStyle);

            EditorPropertyLayout.Space(2);

            SerializedProperty description = property.FindPropertyRelative("description");
            description.ShowField(new GUIContent("#" + (index + 1) + " Description"));
            Layout dataLayout = dataLayouts[index];
            dataLayout.totalElements = 0;
            dataLayout.totalElements += 1;
            dataLayouts[index] = dataLayout;

            SerializedProperty type = property.FindPropertyRelative("type");
            int analyticalTypeIndex = type.enumValueIndex;
            if (analyticalTypeIndex == AnalyticalDataType.Log.GetHashCode()) {
                SerializedProperty eventData = property.FindPropertyRelative("eventData");
                DrawLogEventElement(rect, index, eventData);

                SerializedProperty log = property.FindPropertyRelative("log");
                DrawLogElement(rect, index, log);
            } else { //Event
                SerializedProperty eventData = property.FindPropertyRelative("eventData");
                DrawEventElement(rect, index, eventData);
            }

            if (index < dataList.arraySize - 1) {
                EditorPropertyLayout.GuideLine(Color.grey, 1, new Rect(-13, 8, 13, -15));
                dataLayout = dataLayouts[index];
                dataLayout.totalElements += 1;
                dataLayouts[index] = dataLayout;
            }
            EditorPropertyLayout.EndProperty();
        }

        protected virtual void DrawLogElement(Rect rect, int index, SerializedProperty property) {
            Layout dataLayout = dataLayouts[index];
            SerializedProperty isFold = property.FindPropertyRelative("isFold");
            SerializedProperty type = property.FindPropertyRelative("type");
            int typeIndex = type.enumValueIndex;

            if (typeIndex == AnalyticalFieldType.Object.GetHashCode()) {
                isFold.boolValue = EditorPropertyLayout.BeginFoldoutHeaderGroup(new GUIContent("Log Data"), isFold.boolValue, ShowLogHeaderContextMenu);
            } else {
                isFold.boolValue = EditorPropertyLayout.BeginFoldoutHeaderGroup(new GUIContent("Log Data"), isFold.boolValue);
            }
            dataLayout.totalElements += 1;
            if (isFold.boolValue) {

                SerializedProperty name = property.FindPropertyRelative("name");
                SerializedProperty value = property.FindPropertyRelative("value");

                type.ShowField();
                name.ShowField();
                dataLayout.totalElements += 2;

                if (typeIndex == AnalyticalFieldType.Object.GetHashCode()) {
                    SerializedProperty obj = property.FindPropertyRelative("obj");
                    SerializedProperty component = property.FindPropertyRelative("component");
                    SerializedProperty fieldName = property.FindPropertyRelative("fieldName");

                    obj.ShowField(new GUIContent("Object"));
                    dataLayout.totalElements += 1;

                    if (obj.objectReferenceValue != null) {
                        GenericObject<FieldInfo>[] genericObjects = obj.objectReferenceValue.GetGenericObjectFields();

                        if (genericObjects == null || genericObjects.Length == 0) {
                            fieldName.stringValue = null;
                            value.stringValue = null;
                            EditorPropertyLayout.HelpBox("Cannot find fields in current object", MessageType.Warning);
                        } else {
                            string[] names = genericObjects.SelectMany((x) => x.paths).ToArray();
                            int nameIndex = GetIndexByValue(names, fieldName.stringValue);
                            nameIndex = EditorPropertyLayout.Popup(new GUIContent("Properties/Fields"), nameIndex, GetGUIContents(names));
                            //fieldName.stringValue = GetObjectFromSubArrayByIndex(nameIndex, genericObjects, names, out object genericObject);
                            string[] subNames = names[nameIndex].Split('/');
                            int actualIndex = GetIndexFromSubArrayByIndex(nameIndex, genericObjects, names);
                            Object genericObject = genericObjects[actualIndex].obj;
                            component.objectReferenceValue = genericObject;
                            if (subNames.Length > 1) {
                                fieldName.stringValue = subNames[1];
                            } else {
                                fieldName.stringValue = subNames[0];
                            }
                            object objValue = SerializedPropertyExtensions.GetFieldOrPropertyValue<object>(fieldName.stringValue, genericObject);
                            value.stringValue = (objValue != null) ? objValue.ToString() : "null";
                        }
                        dataLayout.totalElements += 1;
                    } else {
                        fieldName.stringValue = null;
                        value.stringValue = null;
                    }

                    if (showHiddenLogs) {
                        GUI.enabled = false;
                        component.ShowField();
                        fieldName.ShowField();
                        value.ShowField(new GUIContent("Field Value"));
                        GUI.enabled = true;

                        dataLayout.totalElements += 3;
                    }
                } else {
                    value.ShowField(new GUIContent("Field Value"));
                    dataLayout.totalElements += 1;
                }
            }
            EditorPropertyLayout.EndFoldoutHeaderGroup();
            dataLayouts[index] = dataLayout;
        }

        protected virtual void DrawEventElement(Rect rect, int index, SerializedProperty property) {
            Layout dataLayout = dataLayouts[index];
            SerializedProperty isFold = property.FindPropertyRelative("isFold");
            isFold.boolValue = EditorPropertyLayout.BeginFoldoutHeaderGroup(new GUIContent("Event"), isFold.boolValue, ShowEventHeaderContextMenu);
            dataLayout.totalElements += 1;
            if (isFold.boolValue) {
                SerializedProperty speakerParent = property.FindPropertyRelative("speakerParent");
                SerializedProperty speaker = property.FindPropertyRelative("speaker");
                SerializedProperty eventName = property.FindPropertyRelative("eventName");

                speakerParent.ShowField(new GUIContent("Speaker"));
                dataLayout.totalElements += 1;

                if (speakerParent.objectReferenceValue != null) {
                    GenericObject<MemberInfo>[] genericObjects = speakerParent.objectReferenceValue.GetGenericObjectEvents();

                    if (genericObjects == null || genericObjects.Length == 0) {
                        eventName.stringValue = null;
                        EditorPropertyLayout.HelpBox("Cannot find events in current object", MessageType.Warning);
                    } else {
                        string[] names = genericObjects.SelectMany((x) => x.paths).ToArray();
                        int eventIndex = GetIndexByValue(names, eventName.stringValue);
                        eventIndex = EditorPropertyLayout.Popup(new GUIContent("Event"), eventIndex, GetGUIContents(names));
                        //sourceEvent.stringValue = GetObjectFromSubArrayByIndex(eventIndex, genericObjects, names, out object obj);
                        string[] subNames = names[eventIndex].Split('/');
                        int actualIndex = GetIndexFromSubArrayByIndex(eventIndex, genericObjects, names);
                        speaker.objectReferenceValue = genericObjects[actualIndex].obj;
                        if (subNames.Length > 1) {
                            eventName.stringValue = subNames[1];
                        } else {
                            eventName.stringValue = subNames[0];
                        }
                    }
                    dataLayout.totalElements += 1;
                } else {
                    speaker.objectReferenceValue = null;
                    eventName.stringValue = null;
                }

                SerializedProperty listener = property.FindPropertyRelative("listener");
                SerializedProperty eventHandler = property.FindPropertyRelative("eventHandler");

                GenericObject<MethodInfo>[] methodObjects = property.serializedObject.targetObject.GetGenericObjectMethods(null, "Do");

                if (methodObjects == null || methodObjects.Length == 0) {
                    eventHandler.stringValue = null;
                    listener.objectReferenceValue = null;
                    EditorPropertyLayout.HelpBox("Cannot find methods in current object", MessageType.Warning);
                } else {
                    string[] names = methodObjects.SelectMany((x) => x.paths).ToArray();
                    int eventIndex = GetIndexByValue(names, eventHandler.stringValue);
                    eventIndex = EditorPropertyLayout.Popup(new GUIContent("Event Handler"), eventIndex, GetGUIContents(names));
                    eventHandler.stringValue = names[eventIndex];
                    listener.objectReferenceValue = property.serializedObject.targetObject;
                }

                dataLayout.totalElements += 1;

                if (showHiddenEvents) {
                    GUI.enabled = false;
                    speaker.ShowField();
                    eventName.ShowField();
                    listener.ShowField();
                    eventHandler.ShowField();
                    GUI.enabled = true;

                    dataLayout.totalElements += 4;
                }
            }
            EditorPropertyLayout.EndFoldoutHeaderGroup();
            dataLayouts[index] = dataLayout;
        }

        protected virtual void DrawLogEventElement(Rect rect, int index, SerializedProperty property) {
            Layout dataLayout = dataLayouts[index];
            SerializedProperty isFold = property.FindPropertyRelative("isFold");
            isFold.boolValue = EditorPropertyLayout.BeginFoldoutHeaderGroup(new GUIContent("Event"), isFold.boolValue, ShowEventHeaderContextMenu);
            dataLayout.totalElements += 1;
            if (isFold.boolValue) {
                SerializedProperty speakerParent = property.FindPropertyRelative("speakerParent");
                SerializedProperty speaker = property.FindPropertyRelative("speaker");
                SerializedProperty eventName = property.FindPropertyRelative("eventName");

                speakerParent.ShowField(new GUIContent("Speaker"));
                dataLayout.totalElements += 1;

                if (speakerParent.objectReferenceValue != null) {
                    GenericObject<MemberInfo>[] genericObjects = speakerParent.objectReferenceValue.GetGenericObjectEvents();

                    if (genericObjects == null || genericObjects.Length == 0) {
                        eventName.stringValue = null;
                        EditorPropertyLayout.HelpBox("Cannot find events in current object", MessageType.Warning);
                    } else {
                        string[] names = genericObjects.SelectMany((x) => x.paths).ToArray();
                        int eventIndex = GetIndexByValue(names, eventName.stringValue);
                        eventIndex = EditorPropertyLayout.Popup(new GUIContent("Event"), eventIndex, GetGUIContents(names));
                        //sourceEvent.stringValue = GetObjectFromSubArrayByIndex(eventIndex, genericObjects, names, out object obj);
                        string[] subNames = names[eventIndex].Split('/');
                        int actualIndex = GetIndexFromSubArrayByIndex(eventIndex, genericObjects, names);
                        speaker.objectReferenceValue = genericObjects[actualIndex].obj;
                        if (subNames.Length > 1) {
                            eventName.stringValue = subNames[1];
                        } else {
                            eventName.stringValue = subNames[0];
                        }
                    }
                    dataLayout.totalElements += 1;
                } else {
                    speaker.objectReferenceValue = null;
                    eventName.stringValue = null;
                }

                SerializedProperty listener = property.FindPropertyRelative("listener");
                SerializedProperty eventHandler = property.FindPropertyRelative("eventHandler");

                GenericObject<MethodInfo>[] methodObjects = property.serializedObject.targetObject.GetGenericObjectMethods(null, "Do");

                if (methodObjects == null || methodObjects.Length == 0) {
                    eventHandler.stringValue = null;
                    listener.objectReferenceValue = null;
                    EditorPropertyLayout.HelpBox("Cannot find methods in current object", MessageType.Warning);
                } else {
                    string[] names = methodObjects.SelectMany((x) => x.paths).ToArray();
                    int eventIndex = GetIndexByValue(names, eventHandler.stringValue);
                    eventHandler.stringValue = "DoLogEvent";
                    listener.objectReferenceValue = property.serializedObject.targetObject;
                }

                if (showHiddenEvents) {
                    GUI.enabled = false;
                    speaker.ShowField();
                    eventName.ShowField();
                    GUI.enabled = true;

                    dataLayout.totalElements += 2;
                }
            }
            EditorPropertyLayout.EndFoldoutHeaderGroup();
            dataLayouts[index] = dataLayout;
        }

        protected virtual void AddMenuItems(GenericMenu menu) {
            menu.AddItem(new GUIContent("Log Data"), false, AddLogCallback);
            menu.AddItem(new GUIContent("Only Event"), false, AddEventCallback);
        }

        protected abstract void AddLogCallback();

        protected abstract void AddEventCallback();

        protected virtual void ShowEventHeaderContextMenu(Rect position) {
            var menu = new GenericMenu();
            if (!showHiddenEvents) {
                menu.AddItem(new GUIContent("Show Hidden Fields"), false, ShowHiddenEvents);
            } else {
                menu.AddItem(new GUIContent("Hide Hidden Fields"), false, HideHiddenEvents);
            }
            menu.DropDown(position);
        }

        protected virtual void ShowHiddenEvents() {
            showHiddenEvents = true;
        }

        protected virtual void HideHiddenEvents() {
            showHiddenEvents = false;
        }

        protected virtual void ShowLogHeaderContextMenu(Rect position) {
            var menu = new GenericMenu();
            if (!showHiddenLogs) {
                menu.AddItem(new GUIContent("Show Hidden Fields"), false, ShowHiddenLogs);
            } else {
                menu.AddItem(new GUIContent("Hide Hidden Fields"), false, HideHiddenLogs);
            }
            menu.DropDown(position);
        }

        protected virtual void ShowHiddenLogs() {
            showHiddenLogs = true;
        }

        protected virtual void HideHiddenLogs() {
            showHiddenLogs = false;
        }

        protected virtual string GetNameFromSubArrayByIndex<T>(int index, GenericObject<T>[] genericObjects, string[] names) {
            int fieldCount = 0;
            for (int i = 0; i < genericObjects.Length; i++) {
                fieldCount += genericObjects[i].length;
                if (index < fieldCount) {
                    int indexStart = names[index].LastIndexOf('/') + 1;
                    string subName = names[index].Substring(indexStart);
                    return subName;
                }
            }
            return null;
        }

        protected virtual string GetObjectFromSubArrayByIndex<T>(int index, GenericObject<T>[] genericObjects, string[] names, out object obj) {
            int fieldCount = 0;
            obj = null;
            for (int i = 0; i < genericObjects.Length; i++) {
                fieldCount += genericObjects[i].length;
                if (index < fieldCount) {
                    int indexStart = names[index].LastIndexOf('/') + 1;
                    string subName = names[index].Substring(indexStart);
                    obj = genericObjects[i].obj;
                    return subName;
                }
            }
            return null;
        }

        protected virtual int GetIndexFromSubArrayByIndex<T>(int index, GenericObject<T>[] genericObjects, string[] names) {
            int fieldCount = 0;
            for (int i = 0; i < genericObjects.Length; i++) {
                fieldCount += genericObjects[i].length;
                if (index < fieldCount) {
                    return i;
                }
            }
            return 0;
        }

        protected virtual GUIContent[] GetGUIContents(string[] labels) {
            List<GUIContent> contents = new List<GUIContent>();
            for (int i = 0; i < labels.Length; i++) {
                contents.Add(new GUIContent(labels[i]));
            }
            return contents.ToArray();
        }

        protected virtual int GetIndexByValue(string[] names, string value) {
            for (int i = 0; i < names.Length; i++) {
                string name = names[i];
                if (value == null || name.Contains(value)) {
                    return i;
                }
            }
            return 0;
        }
    }
}